\graphicspath{{"module1mobile/OSsimulator/figs/"}}
%\renewcommand{\chaptername}{Lab}
%\renewcommand{\thechapter}{7}
\chapter{Job and Semaphore Processing Simulation Project}
\label{CHAP:SchedulingProject}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Author: Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

\begin{itemize}

\item The ability to think through all of the processing that must take
place when an operating system processes (internal
and external) events

\item The construction of a large software system making practical use
of (some of the) data structures studied a course
on data structures.

\item A tool that can be used to evaluate how a variety of
parameters (e.g., scheduling algorithms, quantums, memory sizes)
affect the performance measures of an operating system.

\item
An appreciation for the complexity involved in building a scheduler and 
the event processing loop of an operating system.

\end{itemize}

\section{Resources and Getting Help} 

\begin{itemize}
    \item System Libraries and I/O Notes:
\url{https://perugini.cps.udayton.edu/teaching/books/SPUC/www/lecture_notes/librariesIO.html}

    \item Linux Manpages (e.g., \texttt{\$ man ps})
\end{itemize}

%%%%%% END PREFIX %%%%%%%

%%%%%% BEGIN MAIN CONTENT OF LAB %%%%%%%

\section{Introduction}

\section{Project Specification}

\subsection{Problem}

Design and implement a program (in any language) that simulates some of the job
and \textsc{cpu} scheduling, and semaphore processing, of a time-shared
operating system.

\subsection{Detailed Description and Requirements}

\begin{figure}
\centering
\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{p2.eps}}
\caption{Architectural view of the simulator.}
\label{fig:arch}
\end{figure}

When jobs initially arrive in the system, they are put on the job scheduling
queue which is maintained in \textsc{fifo} order. The job scheduling algorithm
is run when a job arrives or terminates. Job scheduling allows as many jobs to
enter the ready state as possible given the following restriction: a job cannot
enter the ready state if there is not enough free memory to accommodate that
job's memory requirement. Do not start a job unless it is the first job on the
job scheduling queue. When a job terminates, its memory is released, which may
allow one or more waiting jobs to enter the ready state. 

A job can only run if it requires less than or equal to the system's main
memory capacity. The system has a total of 512 blocks of usable memory. If a
new job arrives needing more than 512 blocks, it is rejected by the system with
an appropriate error message. Rejected jobs do not factor into the final
statistics (described below). 

Note that all jobs in the ready state must fit into available main memory.

Process scheduling is managed as a multilevel feedback queue. The queue has two
levels, each queue is organized as a \textsc{fifo}, and both use a round robin
scheduling technique. New jobs are put on the first level when arriving in the
ready state. When a job from the first level is given access to the
\textsc{cpu}, it is allowed a quantum of 100 time units. If it exceeds that
time quantum, it is preempted and moves to the second level. 

The jobs on the second level may only be allocated the \textsc{cpu} if there
are no jobs on the first level. When a job on the second level is given access
to the \textsc{cpu}, it is allowed a quantum of 300 time units. If it exceeds
that, it is preempted and put back on the second level of the ready queue. 

Process scheduling decisions are made whenever any process leaves the
\textsc{cpu} for any reason (e.g., expiration of a quantum or job termination).
When a job terminates, do job scheduling first, then process scheduling. Also,
give preference to first level jobs (i.e., if a job from the second level of
the ready queue is running, and a new job enters the first level, the running
job is preempted to the second level in favor of the first level job). 

While executing on the \textsc{cpu}, a job may require \textsc{i/o}, which
preempts it to the \textsc{i/o} wait queue for the duration of its \textsc{i/o}
burst. 

While executing on the \textsc{cpu}, a job may perform a semaphore operation.
Assume there are five semaphores shared among all jobs running in the system,
numbered 0 through 4, each initialized to 1. If a job must wait because of a
semaphore, it goes onto the appropriate wait queue until it is signaled. There
is a separate wait queue for each semaphore. 

When a job completes, put it on a finished list for later processing.

The simulator is driven by the events read from standard input. Examples of
possible events are given below. The first field will be the first character of
the line, and subsequent fields will be separated by one of more spaces or
tabs. The header of each field in the following examples does not appear in the
input stream.

\noindent
\textbf{A new job arrives:}
\begin{verbatim}
Event Time Job Memory Run Time
A     140  12  24     2720
\end{verbatim}

\noindent \textit{Interpretation}: job 12 arrives at time 140, requires 24
blocks of memory and uses the \textsc{cpu} for a total of 2720 time units.

\smallskip
\noindent
\textbf{A job needs to perform \textsc{i/o}:}

\begin{verbatim}
Event Time I/O Burst Time
I     214  85
\end{verbatim}

\noindent \textit{Interpretation}: the job currently running on the
\textsc{cpu} will not finish its quantum because at time 214 it needs to
perform \textsc{i/o} for a duration of 85 time units.

\smallskip
\noindent
\textbf{A job performs a wait on a semaphore:}

\begin{verbatim}
Event Time Semaphore
W     550  2
\end{verbatim}

\noindent \textbf{Interpretation}: the job currently running on the CPU
performs a wait on semaphore number 2 which may or may not cause it to be
preempted. Initialize each semaphore to 1.

\smallskip
\noindent
\textbf{A job performs a signal on a semaphore:}

\begin{verbatim}
Event Time Semaphore
S     622  2
\end{verbatim}

\noindent
\textbf{Interpretation}: the job currently running on the CPU
performs a performs a signal on semaphore number 2 which may allow a job to
re-enter the ready state.

\smallskip
\noindent
\textbf{Display the status of the simulator:}

\begin{verbatim}
Event Time
D     214
\end{verbatim}

\noindent \textit{Interpretation}: display the status of the simulator at time
214.  Display events are helpful for debugging the simulator during development.

You may assume that events appear on the input stream in ascending time order.
However, realize that the events given in the input stream are not only events
which your simulator must handle. For instance, a time quantum expiration is
not an event given in the input stream, but it is an event which your simulator
must handle. Furthermore, an internal event, such as a time quantum expiration,
not in the input stream, may occur at the same time as an event in the input
stream (e.g., a new job arrival). Events in the input stream are external
events.

The following is a list of internal events (i.e., not given on the input
stream) which your simulator must handle:

\begin{itemize}
\item job termination (T)
\item time quantum expiration (E)
\item \textsc{i/o} completion (C)
\end{itemize}

Assume that context switching, semaphore operations, and displays take no simulator time (an
unrealistic assumption in a real operating system).

When a display is requested, print the contents of all queues as well as the
job currently running on the \textsc{cpu} to standard output using only the
format used in the sample output given below.

After processing all jobs, write the following to standard output (in this
order, as shown on the sample output given below):

\begin{enumerate}

\item completion time for each job (in order of completion),

\item average turnaround time (where turnaround time is defined as completion
time minus arrival time), and

\item average job scheduling wait time (where wait time is defined as the
number of time units spent in the job scheduling queue).

\end{enumerate}

\subsection{Event Collisions}

Often more than one event happen at the same time. Use the following rules to
determine which events to process first:

\begin{itemize}

\item If an internal event (e.g., an event not on the input stream such as time
slice expiration, \textsc{i/o} completion, or job termination) and an external event
(i.e., an event given explicitly on the input stream) happen at the same time,
process the internal event first.

\item If a job is scheduled to come off the \textsc{i/o} wait queue at the same
time a job is scheduled to come off the semaphore wait queue, take the job off
the \textsc{i/o} wait queue first.

\end{itemize}

An architectural view of the simulator is shown in Figure~\ref{fig:arch}.

\subsection{Additional Requirements}

\begin{enumerate}

%\item Your system must be developed and run on the cpssuse systems. Mutliple
%programming languages are available on these systems, including C
%(/usr/bin/gcc), C++ (/usr/bin/g++), Java (/usr/bin/javac, /usr/bin/java), Go
%(/usr/local/go/bin/go), Perl (/usr/bin/perl), and Python (/usr/bin/python). If
%the language you want to develop your simulator is absent from the system, let
%us know and we will try to get it installed.

\item Your implementation must be distributed across more than one source code
file, in some sensible manner which reflects the logical purpose of the various
components of your design, to encourage problem decomposition and modular
design.

\item Include a \texttt{README} file in your submission which describes (i) the
language you used to develop your program and (ii) the name of the compiler or
interpreter  which you used to compile or interpret your program.  
\end{enumerate}

\subsection{Hints and Notes}

\begin{itemize}

\item You are advised to define a \textsc{pcb} (process control block)
structure and use it as a node in the various queues of your simulator.

\item When debugging your simulator, you are advised to add extra display
events to the sample input to trace the movement of jobs throughout the various
queues of the system.

\item You are advised to organize jobs on the \textsc{i/o} wait queue in the
order in which they are scheduled to come off (i.e., make the \textsc{i/o}
queue a priority queue) to obviate having to search the queue for the next job
to come off of it every time an \textsc{i/o} operation completes.

\item You are advised to organize jobs on each of the 5 semaphore wait queues in \textsc{fifo} order.

\item Since this program involves so many queues (there are five), you are
advised to use a programming language which has list operations built into the
language, such as Python, or one which provides a queue data structure in a
standard library, such as Java or C++. You are advised against re-inventing the
wheel. You are also advised against using this project as an opportunity to
learn a new language. Use tools with which you are already familiar and focus
on the operating systems aspects of the project.

\item If designed properly, the program required to solve this project should
occupy no more than 1,000 lines of code (or less if you use built-in data
structures or data structures from libraries).

\item Watch a YouTube video demo of the simulator at \url{https://youtu.be/eRU8h-5aMOs}.

\end{itemize}

\subsection{Test Data: Sample Input and Output Streams}

\begin{enumerate}

%\item (only events A \& D, \& E \& T) p2stdin\_a and p2stdout\_a [image]
\item (only events A \& D, \& E \& T) \texttt{stdin\_a} and \texttt{stdout\_a}

%\item (only events A, I, \& D, \& E, C, \& T) p2stdin\_b and p2stdout\_b [image]
\item (only events A, I, \& D, \& E, C, \& T) \texttt{stdin\_b} and \texttt{stdout\_b}

%\item (all events: A, I, W, S, & D, & E, C, & T) p2stdin\_c and p2stdout\_c [image]
\item (all events: A, I, W, S, \& D, \& E, C, \& T) \texttt{stdin\_c} and \texttt{stdout\_c}

\end{enumerate}

This test data is available in the shared repository at
\url{operating-systems-lab-manual-shared-files/module1mobile/scheduling/testdata}.
At first, simply try to get only one job through your system; use the input
file 
\url{operating-systems-lab-manual-shared-files/module1mobile/scheduling/testdata/one.txt}.
Once you are confident
that your system processes only one job properly, try to get two jobs through
the system; see the input file 
\url{operating-systems-lab-manual-shared-files/module1mobile/scheduling/testdata/two.txt}.

While developing your simulator, you are encouraged to get it to work on the
simple test input first (e.g., two to three jobs and a display event)
%(\texttt{d.dat})
and progressively enhance and refine
your system to the point where it works on the most complex test input.
%(\texttt{a.dat}).

Use the Linux \texttt{diff} utility to compare your output to the correct
output. For full credit, the output produced by your program must have zero
differences, as defined by \texttt{diff}, with the output posted here.

%There is also a reference executable of a solution for this project available
%at \url{operating-systems-lab-manual-shared-files/module1mobile/scheduling/OSsim}.

\subsection{Project Evaluation}

Ninety percent of your score will come from correctness and 10\% of your score
will come from following our programming style guide. Applicable submission
penalties will then be applied. 

In an effort to award partial credit to students who are unable to complete
certain parts of this project, students earn up to two different portions of
the 70 possible points for correctness:

\begin{itemize}

\item If your program produces this \texttt{stdout\_c} exactly when run on \texttt{stdin\_c}
(all events: A, I, W, S, \& D, \& T, E, \& C), you can earn up to 90 points.

\item If your program produces this \texttt{stdout\_b} exactly when run on
\texttt{stdin\_b} (only events A, I, \& D, \& T, E, \& C), you can only earn
up to 70 points.

\item If your program produces this \texttt{stdout\_a} exactly when run on
\texttt{stdin\_a} (only events A \& D, \& T \& E), you can only earn up to 45
points.  If you have this part of the homework complete, and working perfectly,
early, you will earn +20 points of extra credit.

\item If your program does not produce this \texttt{stdout\_a} exactly when
run on \texttt{stdin\_a}, you will not earn any points.

\item If your program does not compile or execute without errors or warnings,
you will not earn any points.

\end{itemize}

% make a footnote
\textit{Note}: Depending on how you order the jobs on your \textsc{i/o} wait
queue, your dump of the jobs waiting for \textsc{i/o} may not match our output
exactly, and for just that queue, that is acceptable. For instance, you might
organize your \textsc{i/o} wait queue as a priority queue where the job which
comes off first is at the head, or you might maintain jobs on the \textsc{i/o}
wait queue in the order in which they are put on and then search for the job to
take off when the \textsc{i/o} is complete.

\section{Implementation}

\subsection{Tuning Simulation Parameters}
%\subsection{Process Control Block (PCB)}

\subsection{Tuning Simulation Parameters and Process Control Block (PCB)}

\begin{table}
\centering
\begin{tabular}{ll}
\begin{tabular}{l}
\begin{lstlisting}[language=C,basicstyle=\tiny]
#define JOB_SCHEDULING_QUEUE 0
#define FIRST_LEVEL_READY_QUEUE 1 
#define SECOND_LEVEL_READY_QUEUE 2 
#define IO_WAIT_QUEUE 3  
#define FINISHED_LIST 4
#define NEW_JOB_ARRIVAL 'A'
#define IO_REQUEST 'I'
#define SEMAPHORE_WAIT 'W' 
#define SEMAPHORE_SIGNAL 'S'
#define DISPLAY_SIM_STATUS 'D'
#define IO_BURST_COMPLETION 'C'
#define JOB_TERMINATION 'T'
#define TIME_SLICE_EXPIRATION 'E'
#define FIRST_LEVEL_RQ_TIME_QUANTUM 100
#define SECOND_LEVEL_RQ_TIME_QUANTUM 300
#define MAIN_MEM_CAPACITY 512
\end{lstlisting}
\end{tabular} &

\begin{tabular}{l}
\begin{lstlisting}[language=C,basicstyle=\tiny]
typedef struct PCB_tag {
  int job_number,
      arrival_time,
      mem_req,
      time_off_job_scheduler,
      CPU_run_time,
      how_many_CPU_time_units_still_needed,
      starting_time_on_CPU,
      will_this_job_time_slice,
      when_this_job_will_time_slice,
      second_level_job,
      IO_burst_time,
      IO_start_time,
      IO_completion_time,
      completion_time;

  struct PCB_tag* next_process;
} PCB;
\end{lstlisting}
\end{tabular}
\end{tabular}
\caption{(left) Simulator constants and (right) process control block definition.}
\label{tab:code}
\end{table}

Students are encouraged to use sound principles of software engineering in
designing and implementing their simulator. For instance, naming the parameters
and events makes the source code readable (see Table~\ref{tab:code}---left).
The quantum of the first and second level ready queues, as well as the main
memory capacity, can be also passed as command-line arguments. 

\subsection{Main Event Loop}

The simulator involves eight events: five external (A, I, W, S, \& D) and three internal (T, E, \& C).

\begin{lstlisting}[language=C,basicstyle=\tiny]
while (!feof(stdin) || jobs_in_system > 0) {
   if (!feof(stdin))
      next_event = determine_next_event(input_file_event,
                   &next_time_an_input_file_event_will_occur);
    else
      next_event = determine_next_event_after_data_file_expired();

   fprintf(stderr,"Event: %c   Time: %d\n", next_event, current_time);

   switch (next_event) {

      case NEW_JOB_ARRIVAL:

         process_job_arrival(&jobs_in_system);

         scanf("%s%d", inputevent, 
                &next_time_an_input_file_event_will_occur);
         input_file_event = inputevent[0];
         break;
 
      case IO_REQUEST:

         process_io_request();
               
         scanf("%s%d", inputevent, 
                &next_time_an_input_file_event_will_occur);
         input_file_event = inputevent[0];
         break;

      case SEMAPHORE_WAIT:

         process_semaphore_wait();
            
         scanf("%s%d", inputevent, 
                &next_time_an_input_file_event_will_occur);
         input_file_event = inputevent[0];
         break;

      case SEMAPHORE_SIGNAL:

         process_semaphore_signal();
               
         scanf("%s%d", inputevent, 
                &next_time_an_input_file_event_will_occur);
         input_file_event = inputevent[0];
         break;

      case DISPLAY_SIM_STATUS:

         process_display_sim_status();

         scanf("%s%d", inputevent, 
                &next_time_an_input_file_event_will_occur);
         input_file_event = inputevent[0];
         break;

      case IO_BURST_COMPLETION:

         process_io_burst_completion();
         break; 
          
      case TIME_SLICE_EXPIRATION:
           
         process_time_slice_expiration();
         break;
 
      case JOB_TERMINATION:
 
         process_job_termination(&jobs_in_system);
         break;
   }
}
\end{lstlisting}
%
%printf("\nThe contents of the FINAL FINISHED LIST\n");
%printf("---------------------------------------\n\n");
%print_fl();
%printf("\n\n");
%
%printf("The Average Turnaround Time for the simulation was %.3f units.\n\n",
%      calc_avg_turnaround_time() );
%  
%printf("The Average Job Scheduling Wait Time for the simulation was");
%printf(" %.3f units.\n\n", calc_avg_job_scheduling_wait_time() );
%
%printf("There are %d blocks of main memory available in the system.\n\n", 
%      main_mem_avail); 
%
%exit(EXIT_SUCCESS);
%}

\subsection{Implementation Languages}

Students have used C++, Java, Python, Perl, and Scheme to develop a solution 
to this problem.

\section{Student Feedback}

% from Fall 2017
\begin{quote}
\scriptsize{
The mid-term project really nailed in the main concepts of operating systems in general.

The mid-term project was also an interactive and engaging experience that
demonstrated and explained concepts we were working on in class. 

I found that the project really helped me learn how an operating system scheduler worked.

Also I found the midterm project to be really fun, I actually enjoyed working on it.

\dots mostly the project that we did halfway through the semester was very
beneficial to my learning looking back at it.}
\end{quote}

\section{Conclusion}

Future work entails adding a memory management scheme (e.g., paging) to the
organization of the ready queues.

%%%%%% END MAIN CONTENT OF LAB%%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

\section{Thematic Takeaways}

\begin{itemize}

\item Identifying and implementing the actions required to handle/process even
only a handful of system events requires a copious
amount of critical thought and
design.

\item When system events collide (i.e., happen at the same time), policies
must exist to determine which events take priority over others.

\end{itemize}

%\section{Lab Summary}

\section{Key Terms}

%\section{Bibliographic Notes}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
