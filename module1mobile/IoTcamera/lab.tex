% uncomment following line when compiling entire book
\graphicspath{{"module1mobile/IoTcamera/figs/"}}
% uncomment following line when compiling individual chapter
%\graphicspath{{"figs/"}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpg,.png}

%\renewcommand{\chaptername}{Lab}
%\renewcommand{\thechapter}{11}
\chapter{Introduction to Internet of Things (Camera Sensors)}
\label{CHAP:IOTCAMERA}

\noindent
\large{Authors: Rusty O. Baldwin and Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\begin{quote} \textsc{Zuboff's Laws:} ``(1) Everything that can be
automated will be automated, (2) Everything that can be informated will be
informated, (3) Every digital application that can be used for surveillance and
control will be used for surveillance and control.''
\flushright --- \textit{Shoshana
Zuboff}, Harvard Business School, 1988.	\end{quote}

\section{Lab Learning Outcomes}

This chapter serves to familiarize the reader with the Internet of Things
(IoT), its scope, its implications on privacy and security, and a practical
example of installing and using an IoT device.

After reading and doing the exercises in this chapter, the reader will be able
to:

\begin{itemize}

\item Identify the principal characteristics of an IoT device.
\item Enumerate and provide examples of implications the IoT has on security and privacy.
\item Install, configure, and use an IoT camera and motion detector to monitor activities in a given area.

\end{itemize}

\section{Introduction}
\label{sec:IoTintroduction:cam}

The Internet of Things (IoT) can be thought of as network connectivity pushed
down to a lower level of devices.  Typically, network addressable devices were
`heavyweight' in the sense that they consisted of an individual computers or
servers, websites, network routers, or other devices with significant
computational power relative to the devices accessing them.  These
`heavyweight' devices provided communications, computation, or other services.
While they often controlled embedded devices or sensors, these embedded devices
could only be accessed indirectly by a network user (i.e., mediated via the
controlling device).  The IoT changed all that.

What the IoT paradigm enables is direct addressability and control of what once were considered embedded devices or sensors.  In addition to direct addressability, IoT devices often have \textit{less} computational power or resources than the device accessing them.  Furthermore, they usually perform a limited number of specialized functions and, therefore, can be manufactured in quantity at a much lower cost.

\subsection{Internet of Things Examples}
Below is a small sample of IoT devices that are readily available--many have been for some time.  Further reductions in cost, size, and increases in functionality will inevitably make IoT devices even more common.  This will result in a corresponding increase in threats to user privacy, as well as to security.

\begin{itemize}

\item Smart door locks
\item Smart thermostats
\item Smart appliances (refrigerators, dryers, washers)
\item Smart light bulbs and light switches
\item Video cameras and microphones
\item Temperature, humidity, motion, InfraRed (IR) sensors
\item Many others ...

\end{itemize}

The platform you are using for these lab exercises can be considered an IoT device.  The computing capability as well as the size of the original Raspberry Pi was an incredible innovation when introduced in 2012.  Similar (and even smaller) devices are routinely used to provide an vast amount of computational capability for IoT applications.

\subsection{Why Study This Stuff Anyway?}

IoT devices are already common and will only continue to become even more so.
The privacy implications of these devices are already being felt.  Amazon Echo,
Google, Windows Cortana, and Apple Siri routinely collect and store audio data
to both recognize their wakeup command (e.g., `OK Google'), improve their voice
recognition algorithms, and for other purposes.  That these devices are
constantly `listening' and recording conversations within range is no small
privacy concern.  Should such recordings be allowed in the first place?  If
conversations contain evidence of a potential or actual crime, should law
enforcement be able to compel their release?  Should recording such
conversations without express consent itself be considered a crime?  These and
a host of other privacy issues are now at the forefront of debate in legal
circles.

The security of these devices themselves are typically not a major element in
their design leaving them vulnerable to hackers.  The low price of these
devices certainly contributes to this lack of security as well as the desire to
make them as easy to use/configure as possible.  Security in IoT devices is in
such a lax state that there is a public subscription-based web service
dedicated to mapping vulnerable IoT devices world-wide (www.shodan.io), thus,
providing an `existence proof' of just how vulnerable such devices can be.
Moreover, the lack of economic incentive to provide security updates for a
device that costs but a few dollars and is likely to be deployed for years
prior to being replaced and you have a recipe for long-term and recurring
security disasters.

\subsection{Conceptual Exercises for Section~\ref{sec:IoTintroduction:cam}}

\begin{exlist}

\item
Give two examples of IoT systems.

\item Explain the difference between an IoT device and a typical computer server.

\item
What is the principal characteristic of an IoT device?

\item
Give examples of privacy and security threats associated with IoT devices?

\end{exlist}

\subsection{Hands-on Exercises for Section~\ref{sec:IoTintroduction:cam}}

\begin{exlist}

\item
Enabling and Installing the RPi Camera Hardware.\\ \\
This exercise involves enabling the RPi Camera interface and installing
the RPi Camera hardware (i.e., the IoT device) onto the RPi board.

\begin{enumerate}[1)]
	
	\item~The first step to installing the RPi Camera is to enable the
Camera interface in the RPi Operating System by going to \texttt{Preferences ->
RaspberryPiConfiguration -> Interfaces} as shown below in
Figure~\ref{fig:camera-interface:cam}.  You must do this before you install and run
\texttt{install.sh} that you get from cloning the GitHub repository.

%\begin{figure}[H]
\begin{figure}
	\centering
	\includegraphics[height=3in]{camera-interface.jpg}
	\caption{Enabling the RPi Camera Interface.}
	\label{fig:camera-interface:cam}
\end{figure}

After completing this step, reboot the RPi before continuing to Step 2.\\

	\item~To install the RPi Camera you must locate the camera connector.  It is shown circled in red in Figure~\ref{fig:iot-pi-board:cam}.  Once you have located the connector, proceed to the next step.
	
	%\begin{figure}[H]
	\begin{figure}
		\centering
		\includegraphics[height=3in]{RPi-board-layout.jpg}
		\caption{RPi board (RPicamera connector circled).}
		\label{fig:iot-pi-board:cam}
	\end{figure}
	
	\item~Figure~\ref{fig:iot-camera-slot:cam} shows a side view of the camera
connector.  Ensure the black portion is in the ``up'' position as shown in the
figure.
	
	%\begin{figure}[H]
	\begin{figure}
		\centering
		\includegraphics[height=3in]{RPi-camera-slot-up.jpg}
		\caption{Ensure black connector is up.}
		\label{fig:iot-camera-slot:cam}
	\end{figure}

	\item~The RPi camera is connected to the board via a ribbon cable.  The camera ribbon cable connector is circled in red in Figure~\ref{fig:iot-ribbon-cable:cam}, below.
	
	%\begin{figure}[H]
	\begin{figure}
		\centering
		\includegraphics[height=3in]{RPi-camera.jpg}
		\caption{RPi Camera with ribbon connector circled.}
		\label{fig:iot-ribbon-cable:cam}
	\end{figure}

	\item~Figure~\ref{fig:iot-ready:cam} shows the camera ribbon cable ready to be placed into RPi board camera connector.
	%\begin{figure}[H]
	\begin{figure}
		\centering
		\includegraphics[height=3in]{RPi-camera-uninstalled.jpg}
		\caption{RPi Camera ribbon cable positioned and ready for insertion.}
		\label{fig:iot-ready:cam}
	\end{figure}

	\item~Insert the ribbon cable connector into the RPi board connector and then secure the ribbon cable to the board by simultaneously pushing on both ends of the black portion of the board connector.  Figure~\ref{fig:iot-camera-connected:cam} shows how the board should look after completing this step.
	
%\begin{figure}[H]
\begin{figure}
	\centering
	\includegraphics[height=3in]{RPi-camera-installed.jpg}
	\caption{RPi Camera ribbon cable installed.  Note how the black connector is in the down position.}
	\label{fig:iot-camera-connected:cam}
\end{figure}

\end{enumerate}

\item 
Installing the IoT Camera Web Service \\

Next we install a web interface to the Raspberry Pi Camera.  The software and
installation instructions below were taken (and modified as needed) from the
RPi-Cam-Web-Interface at \url{https://elinux.org/RPi-Cam-Web-Interface}.  For
complete documentation and additional information, you are encouraged to
consult the RPi-Cam-Web-Interface site directly.  It is assumed that the
Raspbian Operating System (OS) is installed on your Raspberry Pi (RPi) and
further that the camera hardware is installed and enabled as described above.
You must have access to the Internet to download the web interface software.

\begin{enumerate}[1)]
	\item~First, open a Terminal Window.  The location of the Terminal Window icon is shown in Figure~\ref{fig:term-window:cam}.\\
%\begin{figure}[H]
\begin{figure}
	\centering
	\includegraphics[height=3in]{Open-Term-Window.jpg}
	\caption{Location of Terminal Window Icon.}
	\label{fig:term-window:cam}
\end{figure}
	
	\item~From this window, install the latest OS updates by entering the two commands below and shown in Figs.~\ref{fig:apt-update:cam}--\ref{fig:apt-upgrade:cam}.  These commands can take fifteen minutes or more to complete.\\
	\texttt{\$~sudo apt-get update \\
	\$~sudo apt-get dist-upgrade} \\

%\begin{figure}[H]
\begin{figure}
	\centering
	\includegraphics[height=3in]{apt-get-update.jpg}
	\caption{Updating the Operating System.}
	\label{fig:apt-update:cam}
\end{figure}

%\begin{figure}[H]
\begin{figure}
	\centering
	\includegraphics[height=3in]{apt-get-dist-upgrade.jpg}
	\caption{Upgrading the Operating System.}
	\label{fig:apt-upgrade:cam}
\end{figure}
	
	\item~Now download the RPi Web Interface Software from Github using the command below as shown in Figure~\ref{fig:getting-rpi-cam-web:cam}.\\ \\
	%\texttt{\$~git clone https://github.com/silvanmelchior/RPi\_Cam\_Web\_Interface.git} \\
	\url{\$~git clone https://github.com/silvanmelchior/RPi\_Cam\_Web\_Interface} \\
	
	%\begin{figure}[H]
	\begin{figure}
		\centering
		\includegraphics[height=3in]{Getting-RPi-Cam-Web.jpg}
		\caption{Downloading the RPi Camera Web Interface.}
		\label{fig:getting-rpi-cam-web:cam}
	\end{figure}
	
	\item~After downloading the software, install it using the commands below as shown in Figure~\ref{fig:installing-cam-web-software:cam}. \\ \\
	\texttt{\$~cd RPi\_Cam\_Web\_Interface \\ \$~./install.sh} \\
	
	%\begin{figure}[H]
	\begin{figure}
		\centering
		\includegraphics[height=3in]{Installing-Cam-Web-Software.jpg}
		\caption{Installing the RPi Camera Web Interface.}
		\label{fig:installing-cam-web-software:cam}
	\end{figure}
	
	\item~After successfully installing the software, it is ready to use!
Open up the web browser on your RPi and enter the following \textsc{url} to
access the camera web site. Your browser should look similar to
Figure~\ref{fig:camera-web-site:cam} with the picture illustrating the object at which
your camera is pointing!\\ \\ \url{http://127.0.0.1/html}
	
	%\begin{figure}[H]
	\begin{figure}
		\centering
		\includegraphics[height=3in]{Cam-Web.jpg}
		\caption{RPi Camera Website.}
		\label{fig:camera-web-site:cam}
	\end{figure}
	
\end{enumerate}

\item~Using RPi-Cam-Web.

At this point, the best way to learn the RPi-Cam-Web-Interface is to use it!
It's capabilities include capturing still shots and full-motion video, and
scheduling options for when the camera is active.  Your webcam even has motion
detection built-in!  The 10 buttons available for the operation and
configuration of the RPi Camera Web are circled in red in
Figure~\ref{fig:camera-web-interface:cam}, below.

The function of each button is self-explanatory.  For many of the functions, there are LOTS of configuration options.  You will probably find the defaults adequate for the purposes of this class.  However, a detailed explanation of all configuration options can be found at
\url{https://elinux.org/RPi-Cam-Web-Interface}.

Spend fifteen minutes or so familiarizing yourself with the operation of each
button and the function each controls.

\subsubsection*{Controlling the Camera Through the Terminal}

Now, let us explore how to control the camera through the terminal (i.e.,
without web browser).  Experiment controlling the camera with the command listed
at \url{https://elinux.org/RPi-Cam-Web-Interface#Pipe}.  Demonstrate that you
can control the camera through the terminal.


%\begin{figure}[H]
\begin{figure}
	\centering
	\includegraphics[height=4in]{Cam-Web-Interface.jpg}
	\caption{RPi Camera Web Interface.}
	\label{fig:camera-web-interface:cam}
\end{figure}

\item 
Hacking (and Securing!) the IoT Camera

\begin{enumerate}[1)]
	
	\item~A.
	
	\item~B.
	
	\item~C.
	
	\item~D.
	
\end{enumerate}

\end{exlist}

\section{Thematic Takeaways}

\begin{itemize}
	
	\item~IoT devices are cheap, prolific, and have their own IP addresses.
	
	\item~Securing IoT devices is problematic. 
	
	\item~The economics of the IoT, IoT device disposability, and the desire to make the devices easy to use create incentives \begin{em} not \end{em} to secure IoT devices.
	
	\item~IoT devices bring with them significant privacy issues.
	
\end{itemize}

\section{Chapter Summary} As with most new technology, the Internet of Things
(IoT) brings the promise of exciting new capabilities but also a corresponding
increase in `attack surfaces' that malicious actors can attack.  As you have
seen, these devices are cheap and quite easy to install.  As they become even
more widely deployed, audio and video recording devices will encroach more and
more on our privacy.\\

Based on this lab you:

\begin{itemize}
	
	\item Can identify the principal characteristics of an IoT device.
	\item Are able to enumerate and provide examples of implications the IoT has on security and privacy.
	\item Can install, configure, and use an IoT camera and motion detector to monitor activities in a given area.
	\item Learned how easy it is to compromise network-connected IoT devices.
	
\end{itemize}

\section{Key Terms}

IoT, Internet-of-Things, security, privacy.

%section{Bibliographic Notes}
%\renewcommand{\chaptername}{Chapter}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}
