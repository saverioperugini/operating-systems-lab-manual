/*
 * myioctl.h
 */
#include <linux/ioctl.h>
#include <linux/kdev_t.h>

#define DEVICE_NAME "myioctl_dev"
#define DEVICE_PATH "/dev/myioctl_dev"
static int major_no;

#define MAGIC_NO '4'
/* Define ioctl command; sets the message of the device driver */
#define MYIOCTL_CMD _IOR(MAGIC_NO, 0, char *)
