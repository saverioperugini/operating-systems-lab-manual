#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "myioctl.h"

int main()
{
	int fd;
	int ret;
	char *msg = "OPERATING-SYSTEMS-INTERNALS-AND-DESIGN";

	fd = open(DEVICE_PATH, O_RDWR);
	if (fd < 0) {
		printf("Sorry, cannot open device: %s\n", DEVICE_PATH);
		return -1;
	}

	ret = ioctl(fd, MYIOCTL_CMD, msg);
	if (ret < 0) {
		printf("Sorry, ioctl command failed\n");
		return -1;
	}

	close(fd);
	return 0;
}
