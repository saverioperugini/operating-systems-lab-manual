% uncomment following line when compiling entire book
\graphicspath{{"module3bigdata/lab3/figs/"}}
% uncomment following line when compiling individual chapter
%\graphicspath{{"figs/"}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpg,.png}

\chapter{Cloud Computing and Big Data Processing (Part III)}
\label{CHAP:LAB3}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Authors: Zhongmei Yao and Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

\begin{itemize}
\item Create a mapreduce project using Maven in Eclipse.
\item Run the \texttt{.jar} package on the Hadoop cluster at Amazon Clouds.
\end{itemize}

\section{Resources and Getting Help}

\noindent
\url{https://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html}

\section{Initial Setup and Preparation}

Before proceeding, make sure that Java 8 and Eclipse (the most recent
version) have been installed on your own computer. If you have not installed
Eclipse, go to \url{https://www.eclipse.org/downloads/packages/} to
download/install the latest version of Eclipse IDE for Java Developers.

\subsection{Create a Mapreduce Project using Maven in Eclipse}

We will create a mapreduce project that reads in data from \texttt{.txt}
files and counts word frequencies. The project can deal with a large number
of input \texttt{.txt} files (e.g., votes from all states in the U.S.). The
output is written as $<$word, its frequency$>$ pairs.

\begin{enumerate}

\item Launch Eclipse on your computer. Go to
    \texttt{File}/\texttt{New}/\texttt{Maven Project} and follow steps in
    Figures \ref{newMaven1}--\ref{newMaven3} to create a new Maven project.

\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{newMaven.jpg}
	\caption{New Maven Project --- Select project name and location.}
\label{newMaven1}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{newMaven2.jpg}
	\caption{New Maven Project --- Select an archetype.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{newMaven3.jpg}
	\caption{New Maven Project --- Name group Id and artifact Id.}
\label{newMaven3}
\end{figure}
\item Right click on \texttt{src/main/java} and then add \texttt{new
    Class}. Add \texttt{CountMapper.java},
\texttt{CountReducer.java}, and
    \texttt{CountApplication.java}, respectively, as in Figures
    \ref{class1}--\ref{class3}.

\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{mapper.jpg}
	\caption{Mapper class.}
\label{class1}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{reducer.jpg}
	\caption{Reducer class.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{driver.jpg}
	\caption{Driver class.}
\label{class3}
\end{figure}

\item Edit \texttt{target/pom.xml}. Add the following two dependencies into
    \texttt{pom.xml} file:
\begin{lstlisting}[language=bash, numbers=none]
<dependency>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
    <groupId>org.apache.hadoop</groupId>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
    <artifactId>hadoop-client</artifactId>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
    <version>2.8.4</version>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</dependency>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<dependency>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
   <groupId>org.apache.hadoop</groupId>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
   <artifactId>hadoop-common</artifactId>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
   <version>2.8.4</version>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</dependency>
\end{lstlisting}
Note that 2.8.4 is the version of the hadoop software that we have
installed on the cluster during the previous lab.
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{pom.jpg}
	\caption{Edit \texttt{pom.xml} file.}
\label{pom}
\end{figure}


Click \texttt{Save}. It may take a while to save the \texttt{pom.xml} file.

\item (This step is not required.) If one is interested in dependency
    search, go to \url{https://mvnrepository.com/} and search
    \texttt{hadoop client} and then the matching hadoop version:
%\begin{figure}[H]
%	\centering
%	\includegraphics[width=5.8in]{repository.jpg}
%	\caption{Search dependencies at \url{mvnrepository.com}.}
%\label{repository1}
%\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{hadoopclient.jpg}
	\caption{Search hadoop client dependency at \url{mvnrepository.com}.}
\label{repository}
\end{figure}

Similarly, search \texttt{hadoop common} at the website and then the
matching hadoop version:
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{hadoopcommon.jpg}
	\caption{Search hadoop common dependency at \url{mvnrepository.com}.}
\label{repository}
\end{figure}

\item Choose Java \texttt{jdk} 1.8. In Eclipse, go to \texttt{Window}/
    \texttt{Preferences}/ \texttt{Java}/\texttt{Installed JREs}. Select
    \texttt{jre1.8.0} and then click \texttt{Edit…}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{preferences.jpg}
	\caption{Preferences.}
\label{preferences}
\end{figure}
On the \texttt{Edit JRE} window, replace \texttt{jre} with \texttt{jdk} and
then click \texttt{Finish}. Then \texttt{Apply and Close}.
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{editjre.jpg}
	\caption{Edit JRE.}
\label{jdk}
\end{figure}

\item Choose Java compiler version \texttt{1.8}.
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{compiler.jpg}
	\caption{Compiler.}
\label{repository}
\end{figure}

\item To compile and run it, right click on \texttt{CountApplication} and
    click \texttt{run as Application}:
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{runas.jpg}
	\caption{Compiler.}
\label{repository}
\end{figure}
You should then see the output \texttt{Usage: [input] [output]}.

\item Finally, create our own \texttt{.jar} package! Right click on your
    project name and then choose \texttt{run as Maven Build \ldots} Choose
    goals \texttt{package}. The package \texttt{.jar} will be saved at the
    \texttt{target} folder in your project folder.

In case of errors, we can right click on the project name and then
    \texttt{run as Maven clean}. We can rebuild the \texttt{.jar} package.
\end{enumerate}

In the next subsection, we will upload our \texttt{.jar} file and data files
(in \texttt{.txt}) to \textsc{aws} and run our first mapreduce project in
clouds!

\subsection{Run the \texttt{.jar} Package at amazon clouds}

In the following, we first upload files in \texttt{.jar} and \texttt{.txt}
(mac users use \texttt{scp} command while windows users launch WinSCP
software for file transfer) from our local computer to \textsc{vm}s at
\url{aws.amazon.com}. We then place (using \texttt{put} command) files in
\textsc{hdfs} and run our project on the hadoop cluster.

\begin{enumerate}
\item Log onto \textsc{vm}s at amazon clouds and turn on all nodes.

\item For windows users, launch WinSCP to transfer files from your local
    computer to the name node at amazon clouds. Connect to the name node
    (i.e., master node) using its public IP address.

\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{winscp.jpg}
	\caption{Connect to the name node in WinSCP.}
\end{figure}

Drag and move your \texttt{.jar} file (found in the \texttt{target} folder
after you create \texttt{.jar} package) into folder \texttt{/home/hduser}
at the name node:
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{dragjar.jpg}
	\caption{Upload a mapreduce package in \texttt{.jar} to the name node.}
\end{figure}

Create many data files in \texttt{.txt} at your computer, where one word is
placed on each line:
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{votes.jpg}
	\caption{Sample text files.}
\end{figure}

Upload them to the folder \texttt{/home/hduser} at the name node:
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{dragtxt.jpg}
	\caption{Upload data in \texttt{.txt} to the name node.}
\end{figure}

\item Launch Putty to connect to \textsc{vm}s. Remove the \texttt{mydata}
    directory from each \textsc{vm} and then create a new directory for
    each new session (otherwise we cannot start data nodes from the name
    node):
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ rm -r mydata
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ sudo mkdir -p mydata/hdfs/namenode mydata/hdfs/datanode
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ sudo chown -R hduser:hadoop mydata
\end{lstlisting}
Do this for all \textsc{vm}s.

\item At the name node, start the hadoop cluster:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ hdfs namenode -format
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ start-dfs.sh
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ start-yarn.sh
\end{lstlisting}

Enter \texttt{jps} on each node to check if all nodes are running.

\item At the name node, create a directory (e.g., \texttt{votedata}) in
    \textsc{hdfs} using a \texttt{hadoop fs} (file system) command:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ hadoop fs -mkdir -p votedata
\end{lstlisting}
Use the \texttt{hadoop fs -put} command to load the input \texttt{.txt}
files in the \texttt{/home/hduser} folder to the \texttt{votedata}
directory in \textsc{hdfs}:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ hadoop fs -put /home/hduser/booth1.txt
/user/hduser/votedata
\end{lstlisting}
where \texttt{booth1.txt} can be replaced by \texttt{*.txt} if one wants to
copy multiple input \texttt{.txt} files to the \texttt{votedata} directory
in \textsc{hdfs}.
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{putInHDFS.jpg}
	\caption{Load data files in \texttt{.txt} to \textsc{hdfs} at the name node.}
\end{figure}

\item Run the \texttt{.jar} package:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ hadoop jar votecountMR.jar votecountMR.CountApplication
/user/hduser/votedata/booth1.txt /user/hduser/output
\end{lstlisting}
where \texttt{votecountMR} is the name of the \texttt{.jar} package (found
in the \texttt{target} folder if we use Maven to create the project),
\texttt{CountApplication} is the class that contains the \texttt{main}
method, and \texttt{booth1.txt} can be replaced by \texttt{*.txt} if you
have more than one input \texttt{.txt} file, and a new directory
\texttt{output} is created under the \textsc{hdfs} directory
\texttt{/user/hduser} where the output will be saved.
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{runMyJar.jpg}
	\caption{Run our own mapreduce project at the cluster.}
\end{figure}

Next, we should copy the \texttt{output} folder from the \textsc{hdfs}
directory \texttt{/user/hduser} to the folder \texttt{/home/hduser} at the
name node:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ hadoop fs -get /user/hduser/output /home/hduser
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{results.jpg}
	\caption{Transfer output from \textsc{hdfs} to \texttt{/home/hduser} at the name node.}
\label{tranfer}
\end{figure}
As shown in Figure \ref{tranfer}, we can use \texttt{ls -la} to see the
\texttt{output} folder at the name node.

Let's go to the \texttt{output} folder and open the \texttt{part-r-00000}
file to see results:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ cd output
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~/output$ vi part-r-00000
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{output.jpg}
	\caption{Go to \texttt{/home/hduser/output} at the name node.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{outputCounts.jpg}
	\caption{Open \texttt{part-r-00000} to see results at the name node.}
\end{figure}

If there are errors in the output, we can always debug our Java code on our
local computer and then transfer the \texttt{.jar} to the name node.
Whenever we run the project at the name node, use a \textbf{new output
folder} (e.g., output2) for saving results.

\item Stop all nodes at name node:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ stop-dfs.sh
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ stop-yarn.sh
\end{lstlisting}
Then stop all \textsc{vm}s at \textsc{aws} (see the last step in previous
Cloud Computing labs).
\end{enumerate}



%%%%%% END PREFIX %%%%%%%

%%%%%% END MAIN CONTENT OF LAB %%%%%%%

%\begin{lstlisting}[language=C]
%main() {
%    printf("hello\n");
%    exit(0);
%}
%\end{lstlisting}

%\lstinputlisting[language=C,label=lst:Cprogram]{src/program.c}


%%%%%% END MAIN CONTENT OF LAB%%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

\section{Thematic Takeaways}

\begin{itemize}
\item \textsc{hdfs} is a distributed file system that deals with large
    data.
\item \textsc{hdfs} is designed to reliably store very large files across
    machines in a cluster. Each file is divided into blocks. The blocks of
    a file are replicated for fault tolerance and stored at data nodes.
\item The name node makes decisions regarding replication of blocks.
\end{itemize}

\section{Lab Summary}

In this lab, we created our own mapreduce project using Maven in Eclipse and
ran it on our hadoop cluster at \textsc{aws}.

\section{Key Term}

Hadoop Distributed File Systems.

%\section{Bibliographic Notes}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
