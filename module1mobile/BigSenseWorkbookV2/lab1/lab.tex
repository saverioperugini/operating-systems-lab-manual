% uncomment following line when compiling entire book
\graphicspath{{"module1mobile/BigSenseWorkbookV2/lab1/figs/"}}
% uncomment following line when compiling individual lab
%\graphicspath{{"figs/"}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpg,.png}

%\renewcommand{\chaptername}{Lab}
%\renewcommand{\thechapter}{11}
%\chaptername{Lab 1}
\chapter{Setting up LtSense on the BeagleBone}
\label{CHAP:BigSenseLtSenseSetupBeagleBone}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Authors: Sumit Khanna, Andrew Rettig, and Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

This lab introduces the reader to working with setting up a single board
computer as an environmental monitoring device.  In this lab, you will use a
BeagleBone Black embedded Linux system to collect temperature sensor data and
send it to a web server. You will connect to your BeagleBone Black, install and
configure the LtSense client and then query the BigSense server to view the
results after the data transmission. 

After completing the activities in this lab, will be able to:

\begin{itemize}

\item Connect a computer to the BeagleBone Black. 

\item Install Ltsense on the BeagleBone and configure the software.

\item Connect the BeagleBone to a hotspot and transmit data to the BigSense
server (provided by the instructor).

\end{itemize}

\section{Resources and Getting Help}

This lab and the following two labs require you to independently look up
information, read documentation, and learn unfamiliar concepts and technologies.

The following are resources to help with connecting the DS18B20 temperature
sensor.  This is a little temperature sensor that we have used for various
applications.  You can also chain these sensors together and then each one can
be identified with a unique ID.  These sensors have been used throughout grain
silos with hundreds of sensors along a single drop.  Maxim Semiconductor has a
download for testing your sensors, the OneWire Viewer.  Maxim will also provide
you with the DS18B20 sensors for free for educational purposes.  Just wire the
free sensors to an RJ11 or RJ12 using the wiring diagram below and you are
ready to go. 

\url{https://www.maximintegrated.com/en/products/sensors/DS18B20.html}\\
\url{https://www.maximintegrated.com/en/products/ibutton-one-wire/one-wire/software-tools.html}\\

%\renewcommand{\thefigure}{A\arabic{figure}}

%\setcounter{figure}{0} 

\begin{figure}
   \centering
   \includegraphics[height=3in]{picds.png}
   \caption{DS18B20 Temperature Sensor.}
   \label{picds}
\end{figure}

\begin{figure}
   \centering
   \includegraphics[height=3in]{ds18b20.png}
   \caption{DS18B20 Block Diagram.}
   \label{fig:ds}
\end{figure}

\begin{figure}
   \centering
   \includegraphics[height=4in]{pinds.png}
   \caption{1-Wire Wiring Diagram (Notice the ``Female Connector'').}
   \label{fig:pinds}
\end{figure}

%%%%%% END PREFIX %%%%%%%

%%%%%% BEGIN MAIN CONTENT OF LAB %%%%%%%

In this lab, you will learn how to read data from sensors using LtSense running
on an embedded Linux computer known as a BeagleBone. You will also learn how to
collect that data in BigSense, and then retrieve that data for analysis.

These labs have both BigSense and LtSense running on
%The labs covered in this workbook have both BigSense and LtSense running on
Linux systems. Although it is possible to run both pieces of software on other
operating systems, the official installation instructions and packages are for
Linux operating systems.
%Don't worry!
Working through these labs, you will acquire an understanding of Linux and be
given the information and tools needed to configure and run simple services.

\section{Preliminaries and Stylistic Conventions}

Consistent notation and style are used throughout the BigSense Labs. Being
familiar with them will help when completing the lab assignments. 

\begin{itemize}

\item \textbf{Command blocks.}

Command blocks show commands that should be typed into a Linux console,
typically (but not always) on either the device being used to collect and
transmit sensor data (the Beagle Bone), or the system running BigSense which
collects the data. The following command shows how to retrieve details about
\textsc{unix} systems:

	\texttt{\color{red}{\$~uname -a}} 
%	\$~sudo apt-get dist-upgrade} \\
	
%\item Uniform Resource Locator (URL)\\

%All URLs start with either http or https and contain a full web address that
%can be accessed via a web browser:
%\url{https://bigsense.io}

\item File Samples\\

Blocks of text used for configuration files or containing programming code will
be in a mono spaced font, indented and can usually be cut and paste, adjusting
certain parameters as appropriate.  Definitions and explanations relating to
specific parts of URLs or file samples will be placed in italics. For example,
in the below file sample, sample-rate is a setting indicating the rate at which
data will be samples in seconds. \\

\begin{mylisting}
[Config]
some_setting = Test
sample_rate = 15.0
\end{mylisting} 

\noindent
Explanations for parts of commands will be in \textbf{bold}. 

\item \textbf{Parameters.}\\

In various examples, there may be less-than and greater-than signs with an
identifier. These should be replaced with a specific value. The instructions
following a figure with parameters will explain the parameters and what values
they should be replaced with.\\

\textbf{Value in a URL:}\\
\url{http://example.com?Parameter=<SpecificValue>}\\

\medskip

\textbf{Value in a Command:}\\
\texttt{\color{red}{ls /home/<Username>/data}}

\end{itemize}

\section{Materials}
\begin{itemize}
    \item Laptop
    \item smart phone (if using hotspot)
    \item Beagle Board Black Wireless
    \item Micro USB Cable
    \item 1-Wire Temperature Sensor (CHECK APPENDIX)
    \item 1-Wire USB Connector (DS9490r)
    \item USB Hub
    \item USB GPS
\end{itemize}

\section{Linux Commands}

You will use the following Linux commands in this lesson:

\begin{itemize}

\item
\textbf{ssh}: Secure Shell; a tool to remotely connect to the command prompt of a Linux system

\item
\texttt{cat}: used to print the contents of a file to the screen. The entire file is printed at once. If you want to display a file in pages, the commands more and less can be used

\item
\texttt{cd}: change directory

\item \texttt{nano}: an editor. If you are already familiar with Linux, you may
be more comfortable installing the more advanced vim or emacs editors. 

\item \texttt{tail}: command that displays the last several lines of a file. It
is often used for examining log files.

\item \texttt{sudo}: runs a command as the root user (system administrator).
This is similar to Windows User Access Control dialog or having to enter in a
password for Administration access on MacOS. 

\item
\texttt{apt-get}: package manager in Debian based distributions for installing and updating apps and services. 

\end{itemize}

\subsection{Other Linux Concepts}

\begin{itemize}

\item
\texttt{/etc} the \texttt{/etc}
directory in Linux is where almost all configuration files are stored. Almost all setting in Linux can be configured in plain text files.

\item
\texttt{root}: The root user on a Linux system is the administrator. 
\end{itemize}

\section{Procedure}

\begin{enumerate}

    \item \textbf{Connect the Beagle Board via USB port to your desktop or
laptop.} Power is provided over USB, so you do not need to connect the 5 volt
power adapter.  On Windows or Mac, occasionally you will need to install
additional drivers to connect to the Beagle Board. Refer to step \#2 on the
official installation guide for the additional download and installation:
\url{https://beagleboard.org/getting-started}  

%\begin{figure}[H]
\begin{figure}
   \centering
   \includegraphics[height=4in]{bbb.png}
   \caption{Beagle Bone Black connected to a laptop.}
   \label{fig:bbb}
\end{figure}

    \item \textbf{Connect to the Beagle Board console.}
    First open a web browser (Firefox or Chrome) and attempt to browse to
\url{http://192.168.7.2}. You should see a web page that is served from a web
server on your Beagle Board. 

If you are using Windows, you will need to download Putty, an open source
\textsc{ssh} client:
\url{https://www.chiark.greenend.org.uk/~sgtatham/putty/download.html}\\

You should then be able to connect to \url{192.168.7.2}, the default IP address
of a Beagle Board. The username will be debian and the password will be
temppwd.

%\begin{figure}[H]
\begin{figure}
   \centering
   \includegraphics[height=4in]{putty.png}
   \caption{Putty connection dialogue.}
   \label{fig:putty}
\end{figure}

If you are on Linux or MacOS, open a Terminal and run the following command:
\texttt{\color{red}ssh 192.168.7.2 -ldebian}\\\\
Troubleshooting: If you are having trouble connecting to your Beagle Board, please refer to the troubleshooting guide on the official Beagle Board quick start guide:\\
\url{https://beagleboard.org/getting-started#troubleshooting}\\
or the Adafruit guide\\
\url{https://learn.adafruit.com/ssh-to-beaglebone-black-over-usb/preparation}

\item \textbf{Connecting to the Internet.}

Your lab administrator should provide you with some basic resources and
suggestions for connecting to the Internet.  The preferred solution is to hot
spot your phone and create the connection using the built-in network manager
called ``connmanctl''.  A secondary solution is passing your connection from you
laptop to the Beagle Board.  Some suggestions for the phone solution are to
create a very simple password and to disable the default wifi tether mode.  You
can confirm the connection with a simple ping command.  Follow the example
commands given below for a local WiFi or the phone hotspot.

\texttt{\color{red}
sudo connmanctl\\
connmanctl > tether wifi disable\\
connmanctl > enable wifi\\
connmanctl > scan wifi\\
connmanctl > services\\
connmanctl > agent on\\
connmanctl > connect wifi\_506583d4697256616c6c6f5f42\_managed\_psk\\
Passphrase? ***********\\
connmanctl > quit\\}

\item \textbf{Ensure your Beagle Board is running the Debian Operating System.}
From the Beagle Board root command prompt, run the following:\\\\
\texttt{\color{red}cat /etc/debian\_version}\\

This should return a version number of 9.0 or higher. If you get an error
indicating this file does not exist, my may be running a Beagle Board with the
older Angstorm Linux Operating System. Please refer to the documentation for
updating the Beagle Board to the latest Debian image:
\url{https://beagleboard.org/latest-images}

\item \textbf{Be sure to update your system.} Use the \texttt{apt-get update}
and \texttt{upgrade} commands in root to update the system.  Researching
commands online is a good habit if you are having issues running them or you
are confused on their function.  If you skip updating the basic operating
system it will cause errors later in the lab.

\item \textbf{Set the correct date and time (you may be able to skip this step,
use the \texttt{date} command to test).}
The correct date and time is necessary for transmitting sensor data. This can be accomplished by installing the ntpdate application and running the ntpdate like so:\\\\
\texttt{\color{red}{apt-get install ntpdate\\
ntpdate pool.ntp.org}}\\

Also install ntpd to keep the device in sync\\
\texttt{\color{red}{apt-get install -y ntpd\\
systemctl status ntpd.service
}}\\

NTP is the Network Time Protocol. It’s used internally within many operating
systems including Windows, Mac, Android and desktop Linux to sync the machines
time with well-known clocks around the world over the Internet. Because we’re
running a minimal embedded Debian system, we need to install this component
manually using the apt-get command. The second command, ntpdate, syncs the
operating system immediately with pool.ntp.org. \\\\ You can verify the date is
set correctly by running {\color{red}date}. Keep in mind that the date returned
will be in the \textsc{utc} timezone.  You can set the time for the local
timezone depending on your distribution.

\item Add the BigSense.io Digital Signing Key\\\\
In the Linux world, packages are typically installed from package repositories. If you have used app stores for mobile devices such as Google Play's App store or Apple's App store, the concept is similar. However, in the Linux world, you can add 3rd party repositories that are not part of the official repository. \\
Unlike Google or Apple's closed software repositories where only software they approve can be managed automatically, in Debian you can add custom repositories maintained by others and manage packages in the same way as if they came from the official repository. \\
To start, we'll add the BigSense repository key by entering in the following command as the root user:\\\\
\texttt{\color{red}wget https://repo.bigsense.io/bigsense.io.key}\\
\texttt{\color{red}sudo apt-key add bigsense.io.key}\\\\
A public key is used to digitally sign all official BigSense packages to ensure they have been released by the BigSense team. This command uses curl to download the key from the BigSense website, and then pass that key into apt-key so that the system will trust it. The pipe (|) operator allows the output of curl to be sent as the input to apt-key. The sudo command is used to ensure apt-key runs as root (although if you are already root, this is redundant/unnecessary) 

\item Add the BigSense.io Repository\\\\
Next, will add the BigSense repository. The repository is on a secure connection, so we need to add the apt-transport-https package using the following command:\\\\
\texttt{\color{red}sudo apt-get install apt-transport-https -y --force-yes }\\\\
If running as root, the sudo command can be omitted. The -y will install the package without prompting you first (otherwise you’d have to enter ‘Y’ or ‘Yes’ at the prompt). The --force-yes is only necessary for the apt-transport-https package as an additional confirmation as this is a security related update. \\
Next we need to create a file that tells apt-get to load the BigSense repository. You will need to run this in root as well.  Create the following file with the nano editor like so:\\\\
\texttt{\color{red}nano /etc/apt/sources.list.d/repo\_bigsense\_io\_debs.list}\\\\

Place the following line in this file, save and close it (Control+O followed by Control+X).\\\\

deb [arch=all] https://repo.bigsense.io/debs systemd testing\\\\

We are using the systemd component to indicate we want packages that use the SystemD startup process used on the Beagle Board custom Debian image. 
\item Install LtSense\\\\
Next we will install LtSense using the package repository with the following commands:\\\\
\texttt{\color{red}sudo apt-get update\\
sudo apt-get install ltsense -y}\\\\
The first command will search all package repositories for updates. The second command installs LtSense, the client we will use for reading sensor data.

\item Configure LtSense with virtual sensors\\\\
LtSense uses a simply configuration file located at
\texttt{/etc/ltsense/ltsense.conf}.
You can use the nano editor to create this file like so:\\\\
\texttt{\color{red}nano /etc/ltsense/ltsense.conf}\\\\

\begin{mylisting}
[General]
sample_rate = 15

[Data]
  [[primary]]
    type = sense.xml
      [[[Identifier]]]
        type = name
        id = <YourUniqueName>
      [[[Location]]]
        type = virtual
        longitude = 34.5
        latitude = 12.2

[Transport]
  [[http]]
    type = http
    url = <Server IP/url>/Sensor.sense.xml
    pause_rate = 0.1
    timeout = 10.0
      [[[Queue]]]
        type = memory
      [[[Security]]]
        type = none
[Handlers]
  [[virtual]]
    type = virtual
    sensors = $temp1,$temp2

[Sensors]
  [[temp1]]
    type = virtual/temp
    id = VRTEMP01
    units = C
    rangeMin = 1
    rangeMax = 25
  [[temp2]]
    type = virtual/temp
    id = VRTEMP02
    units = C
    rangeMin = 1
    rangeMax = 25
\end{mylisting}

\noindent Save and exit (Note: commands are at the bottom of the nano editor.
$<$\textit{ctrl-o}$>$ to save and $<$\textit{ctrl-x}$>$ to exit). (LtSense must
be restarted when changes are made to the config file)

There are several parts to this configuration file. Let us break them down.

The sample rate is time in seconds between sensor readings. In this case, every
15 seconds, data will be read from the Handlers.

We only have one Handler listed, one that contains two virtual/temp sensors. A
virtual temperature sensor is used for testing and transmits random readings
between rangeMin and rangeMax.

The Data section determines how to format the sensor readings for transport. In
this case, we're using the BigSense format sense.xml with a unique id field to
distinguish each LtSense instance. We're not using a real GPS, but setting the
Location to a virtual location and manually entering a longitude and latitude.

The Data is then sent to all the available Transport sections. In this case, we
have one transport that sends our data over the web to a BigSense service. The
timeout is set to 10 seconds, meaning if the data cannot be transmitted to the
destination successfully, it will be placed in a Queue and be retried after the
given interval. Once a queued sensor reading has been successfully transmitted
to the destination, the rest of the Queue will empty at the rate specified by
pause rate; in this instance, at a 1/10th second intervals.

Finally, the Security set for our instance is disabled. In a later lab, we'll
see how we can digitally sign our data to ensure the BigSense instance once
receives data from authorized LtSense clients.  (also, it is possible to have a
server without security, remember to drop the `s' from http in that case).

Full instructions on configuring LtSense are available at:
\url{https://bigsense.io/display/overview/Configuration#Configuration-LtSense}.

\item ReStart LtSense\\\\
To restart LtSense, run the following command as root: (it attempts to start automatically after install)\\\\
\texttt{\color{red}systemctl restart ltsense.service}\\\\
Other basic commands often used:\\
\texttt{\color{red}systemctl start ltsense.service}\\
\texttt{\color{red}systemctl stop ltsense.service}\\
\texttt{\color{red}systemctl status ltsense.service}\\

\item Check the logs to ensure LtSense is transmitting data properly \\\\
After LtSense has started, check the log files with the following command:\\\\

\texttt{\color{red}tail -n 500 -f /var/log/ltsense/ltsense.log}

This command will give us the last 500 lines of logging. The -f option
indicates that tail will follow the log file and show new lines as they are
added. You should see LtSense attempting to transmit sensor data to BigSense.
Use $<$\textit{ctrl-c}$>$ to exit 
\texttt{tail}. If there was an error and LtSense failed to start, you
will also see the error message in the log. At this point, it will most likely
be a configuration error. You may need to stop the service by running the
following:

\texttt{\color{red}systemctl stop ltsense.service}

Then you should fix any errors in your configuration file, start the service
again and check the logs. Do this until the logs indicate the service is
successfully running and transmitting data.  Lastly, the following command is
also good for debugging by running in root.

\texttt{\color{red}systemctl status ltsense.service}\\\\

\item Query BigSense for your data\\

Once you have LtSense successfully transmitting data, you can query the data.
In a web browser, go to the following address:

\url{http://<Server IP/url>/Query/Latest/100.table.html}

You should see your data mixed with data from others in your lab.

\item Filter your query for your data.

To see only your data, you can filter based on the unique id for your relay.

\url{http://<Server IP/url>/Query/Latest/100.table.html?RelayID=<YourUniqueID>}

\item Stop LtSense

Now that we have ensured LtSense is installed and can communicate with
BigSense, the next step is to configure LtSense to work with real sensors and a
GPS. Before we change the configuration, let's stop LtSense by running the
following:

\texttt{\color{red}systemctl stop ltsense.service}\\

\item Install GPS.

Next we need to install gpsd, a Linux service that collects information from
GPS devices. To do so, we will use the the package manager again with the
following command:

\texttt{\color{red}apt-get install gpsd gpsd-clients -y}

\item Plug in GPS\\

Dbus, a Linux messaging service, automatically starts an instance of the gpsd
service whenever a GPS device is plugged in. Plug your GPS into your USB Hub
and then plug the hub into the Beagle Board (The hub is so we can also add the
1-Wire sensors in a later step).  Make sure the light is on for both the hub
and the GPS.  (Sometimes it is necessary to have these plugged in before
powering on the BBB.)

\item Run the GPS client.

Run the following command to see GPS information from your device:

\texttt{\color{red}cgps}

This will show the current gps tracking and lock status. LtSense will only
transmit GPS data if there is a 3D lock. If you do not have a 3D lock, move
your GPS to ensure it has a clear view of the sky.  Check the trouble shooting
guide if you are having issues.  (Note: the GPS can take a bit to get a lock on
the satellites the first time you connect it to your system.)

\item Install 1-Wire Drivers

To communicate with our 1-Wire Temperature sensors, LtSense uses the open
source One Wire File System (owfs). We can install the 1-Wire Python package
like so:

\texttt{\color{red}apt-get install python-ow -y}

\item Attach 1-Wire USB Dongle.

Attach your 1-Wire USB dongle and temperature sensor to the USB hub. 

\item Reconfigure LtSense for 1-Wire+GPS.

Next, we are going to reconfigure LtSense to use our GPS and 1-Wire temperature
sensor instead of the virtual sensors which just generate random data. Run the
following command to edit the LtSense configuration file.

\texttt{\color{red}nano /etc/ltsense/ltsense.conf}

Change the configuration to the following:
\begin{mylisting}
[General]
sample_rate = 15

[Data]
  [[primary]]
    type = sense.xml
      [[[Identifier]]]
        type = name
        id = <YourUniqueName>
      [[[Location]]]
        type = gps

[Transport]
  [[http]]
    type = http
    url = <Server IP/url>/Sensor.sense.xml
    pause_rate = 0.1
    timeout = 10.0
      [[[Queue]]]
        type = memory
      [[[Security]]]
        type = none
[Handlers]
  [[onewire]]
    type = 1wire
    device = u
\end{mylisting}

\item Start LtSense

\texttt{\color{red}systemctl start ltsense.service}

\item Check the logs to ensure LtSense is transmitting real data

\texttt{\color{red}tail -n 500 -f /var/log/ltsense/ltsense.log}

\item Query BigSense for your new data

\url{http://<Server IP/url>/Query/Latest/100.table.html?RelayID=<YourUniqueID>}

\end{enumerate}

%%%%%% END MAIN CONTENT OF LAB%%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

\section{Thematic Takeaways}

\begin{itemize}
\item Learning IoT can begin with the single-board computer as a client
sending data to a remote server.

\item Linux commands and \textsc{ssh}
are essential both for embedded devices and server setups. 

\item
Sensors are the connection to the physical world with essential software
and hardware precision.

\end{itemize}

\section{Lab Summary}

By the end of this lab, you should be able to transmit sensor data and gps
information from your Beagle Boards using the LtSense client to a BigSense
server and query that information. This is the basis for working with BigSense
as a geospatial sensor network tool. In the next few labs, you will learn more
about configuring LtSense and using BigSense to retrieve and analyse that data.

\section{Key Terms}

BigSense, configuration file, connman, digital signing key, drivers,
\textsc{gps}, \textsc{ip} address, Linux, logs, LtSense, nano, port,
repository, \textsc{ssh}, 1-Wire.

%\section{Bibliographic Notes}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
