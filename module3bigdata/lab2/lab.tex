% uncomment following line when compiling entire book
\graphicspath{{"module3bigdata/lab2/figs/"}}
% uncomment following line when compiling individual chapter
%\graphicspath{{"figs/"}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpg,.png}

\chapter{Cloud Computing and Big Data Processing (Part II)}
\label{CHAP:LAB2}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Authors: Zhongmei Yao and Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved} }

\section{Lab Learning Outcomes}

\begin{itemize}
\item Build passwordless connections for nodes in the Cluster
\item Install Java and hadoop software
\item Configure the hadoop cluster
\item Start the hadoop cluster and run existing programs
\end{itemize}

\section{Resources and Getting Help}

\noindent
\url{https://hadoop.apache.org/docs/r2.8.4/hadoop-project-dist/hadoop-common/ClusterSetup.html}

\subsection{Build Passwordless Connections for Nodes in the Clusster}

As a demo, the hadoop cluster in this lab consists of one (primary) name node
(i.e., Master node), one secondary name node (a backup for the primary name
node), and two data nodes. In real applications, a hadoop cluster may
consists of hundreds/thousands of data nodes. When a hadoop program runs, the
name node should send tasks to data nodes. To support this, in the following
we will show steps to create passwordless connections from the name node to
data nodes.

Before proceeding, make sure that all four \textsc{vm}s at \textsc{aws} are
running and you have successfully opened \textsc{ssh} sessions (as the user
\texttt{hduser}) with them. Note that a \textsc{vm}'s public \textsc{ip} address
remains the same within a session but changes in a new session whenever you
turn it on 
at \textsc{aws} and that \textbf{its private \textsc{ip} address remains the
same until it is deleted}.

\begin{enumerate}

\item At each \textbf{data node}, make a directory \texttt{.ssh}
    (\texttt{-p} below means enforcing to make a directory), give
    \texttt{hduser} its ownership, and then enter \texttt{ls -la} to see if
    the folder \texttt{.ssh} has been created:
\begin{lstlisting}[language=bash, numbers=none]
hduser@data-node:~$ sudo mkdir -p .ssh
\end{lstlisting}
\vspace{-15mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@data-node:~$ sudo chown -R hduser:hadoop .ssh
\end{lstlisting}
\vspace{-15mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@data-node:~$ ls -la
\end{lstlisting}

\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{dirSSH.jpg}
	\caption{Make a directory \texttt{.ssh} on each data node.}
\end{figure}

\item At the \textbf{name node}, generate an RSA public-private key pair:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ ssh-keygen -f ~/.ssh/id_rsa -t rsa -P ""
\end{lstlisting}
You should see the following:
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{pubpriKeyPair.jpg}
	\caption{Generate a public-private key pair at Master node.}
\end{figure}

Note from the above figure that the public key \texttt{id\underline{
}rsa.pub} is saved in the directory \texttt{/home/hduser/.ssh/}.

\item Modify permissions on this file (mode \texttt{400} means that this
    file can be read only):
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ chmod 400 .ssh/id_rsa.pub
\end{lstlisting}

\item Append (i.e., using \texttt{cat} command) the public key to the
    \textbf{name node}’s authorized keys file:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
\end{lstlisting}

\item Send (using \texttt{ssh} command) and append (using \texttt{cat}
    command) the public key \texttt{id\underline{ }rsa.pub} from the name
    node to a data node and save this key in the authorized keys file in
    the data node's the folder \texttt{.ssh}:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ cat ~/.ssh/id_rsa.pub | ssh hduser@34.208.175.247 'cat
> ~/.ssh/authorized_keys'
\end{lstlisting}
where \textsc{ip} address `\url{34.208.175.247}' is this data node's public 
\textsc{ip}
    address (you can also use its private \textsc{ip}
address; either one is fine
    here).
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{pubKeyToSlave.jpg}
	\caption{Send and append the public key to a data node from the name node. }
\end{figure}

In the above figure, it will proceed after you enter the data node's
password.

Once a data node stores the name node's public key, \textsc{ssh} allows the
name node to access the data node without having to type in the data node's
password. This powerful feature is widely used for file transfers and
configuration management. To verify that, we can log onto the data node
(whose public \textsc{ip} address is \url{34.208.175.247})
using \texttt{ssh} at the name
node:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ ssh hduser@34.208.175.247
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{sshToSlave.jpg}
	\caption{Build a passwordless connection from the name node to a data node}
\label{passless}
\end{figure}
As shown above, the data node did not request its password. The command
\texttt{exit} in Figure \ref{passless} returns connection (from your local
computer) to the name node.

\textbf{Do this for the other nodes in the cluster} so that the name node
can build passwordless connections to them too.

\item Finally, on each \textsc{vm} modify permissions on the following file
    (mode \texttt{600} means that only authenticated users can read/write
    this file):
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ chmod 600 .ssh/authorized_keys
\end{lstlisting}
\end{enumerate}

In case one enters wrong commands and could not fix issues, remove the
\texttt{.ssh} folder on each \textsc{vm} (i.e., using \texttt{rm} command):
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ cd
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ rm -r .ssh
\end{lstlisting}
Then start over tasks described in this subsection.


\textbf{Before proceeding, make sure that the name node can connect to
all other nodes without entering a password; otherwise we cannot run any
hadoop programs.}

\subsection{Install Java and Hadoop Software on Each VM}

In this subsection, we go through steps to install Java 8 and hadoop 2 on
each \textsc{vm} in the cloud.

\begin{enumerate}
\item Log onto your \textsc{vm}s at aws and install Java 8 on all
    \textsc{vm}s:
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ sudo apt-get update
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ sudo apt-get install openjdk-8-jdk
\end{lstlisting}

Go to the directory \texttt{/usr/lib/jvm}, create a symbolic link (using
\texttt{ln} command) pointing to \texttt{java-8-openjdk-amd64} for ease of
operation, and then enter ``\texttt{java -version}" to check if we install
Java 8 successfully:
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ cd /usr/lib/jvm
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:/usr/lib/jvm$ sudo ln -s java-8-openjdk-amd64/ jdk
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:/usr/lib/jvm$ java -version
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{java8.jpg}
	\caption{Create a new name \texttt{jdk} pointing to the directory \texttt{java-8-openjdk-amd64}.}
\end{figure}

\item Install \texttt{hadoop} 2.8.4 on all \textsc{vm}s.

First go to the home directory, download (using \texttt{wget} command) the
hadoop software, and then extract it to the '/usr/local' directory:
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ cd
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ wget
http://mirrors.gigenet.com/apache/hadoop/common/hadoop-2.8.4/hadoop-2.8.4.tar.gz
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ sudo tar vxzf hadoop-2.8.4.tar.gz -C /usr/local
\end{lstlisting}
Under the directory \texttt{/usr/local}, rename the folder
\texttt{hadoop-2.8.4} for ease of operation, and give \texttt{hduser} the
ownership of the folder:
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ cd /usr/local
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:/usr/local$ sudo mv hadoop-2.8.4 hadoop
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:/usr/local$ sudo chown -R hduser:hadoop hadoop
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{hadoop.jpg}
	\caption{The folder \texttt{hadoop-2.8.4} is renamed as \texttt{hadoop} for ease of operation.}
\end{figure}

\item Edit hadoop environment variables on all \textsc{vm}s.

Go back to the home directory and edit (using \texttt{sudo vi} command)
hadoop environment variables in the \texttt{.bashrc} file:
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:/usr/local$ cd
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ sudo vi .bashrc
\end{lstlisting}
Add the following at the end of the \texttt{.bashrc} file and save the
file:
\begin{lstlisting}[language=bash, numbers=none]
#Hadoop Variables
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
export JAVA_HOME=/usr/lib/jvm/jdk/
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
export HADOOP_INSTALL=/usr/local/hadoop
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
export PATH=$PATH:$HADOOP_INSTALL/bin
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
export PATH=$PATH:$HADOOP_INSTALL/sbin
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
export HADOOP_MAPRED_HOME=$HADOOP_INSTALL
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
export HADOOP_COMMON_HOME=$HADOOP_INSTALL
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
export HADOOP_HDFS_HOME=$HADOOP_INSTALL
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
export YARN_HOME=$HADOOP_INSTALL
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
#End
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{bashrc.jpg}
	\caption{Edit \texttt{.bashrc} file.}
\end{figure}
Load these hadoop environment variables by using \texttt{source} command:
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ source ~/.bashrc
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{source.jpg}
	\caption{Load environment variables.}
\end{figure}

\item Create directories for \textsc{hdfs} (Hadoop Distributed File System)
    on all \texttt{vm}s.

At the home directory, make the following directories and give
\texttt{hduser} the ownership. Temporary files generated by \textsc{hdfs}
will be saved in these directories.
\begin{lstlisting}[language=bash, numbers=none]
hduser@each-node:~$ sudo mkdir -p mydata/hdfs/namenode mydata/hdfs/datanode
hduser@each-node:~$ sudo chown -R hduser:hadoop mydata
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{hdfsfolders.jpg}
	\caption{Create folders for \textsc{HDFS}.}
\end{figure}
\end{enumerate}

\subsection{Configure the Hadoop Cluster and Run Existing Programs}

This subsection describes details to configure the hadoop cluster. We will
modify these five files at the name node and then send them to other nodes in
the cluster: \texttt{hadoop-env.sh}, \texttt{core-site.sh},
\texttt{yarn-site.sh}, \texttt{mapred-site.xml} and \texttt{hdfs-site.xml}.


\begin{enumerate}
\item Edit \texttt{hadoop-env.sh}, where we specify Java home directory.
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ cd $HADOOP_INSTALL/etc/hadoop
hduser@name-node:/usr/local/hadoop/etc/hadoop$ sudo vi hadoop-env.sh
\end{lstlisting}
As in Figure \ref{hadoopenv}, under \texttt{vi} input mode, search
`\texttt{export JAVA}' and then spescify Java home directory as follows:
\begin{lstlisting}[language=bash, numbers=none]
export JAVA_HOME=/usr/lib/jvm/jdk/
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{hadoopenv.jpg}
	\caption{Specify Java home directory in \texttt{hadoop-env.sh}.}
\label{hadoopenv}
\end{figure}

\item Edit \texttt{core-site.xml}, where we list the name node's private 
\textsc{ip}
    address and port number (e.g., \texttt{9000}) as the core site's
    address.
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:/usr/local/hadoop/etc/hadoop$ sudo vi core-site.xml
\end{lstlisting}
Add the following but replace \texttt{name-private-IP-address} with the
actual \textbf{private \textsc{ip} address} of the name node in
\texttt{core-site.xml} and save it:
\begin{lstlisting}[language=bash, numbers=none]
<configuration>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<name>fs.defaultFS </name>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<value>hdfs://name-private-IP-address:9000 </value>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</configuration>
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{coresite.jpg}
	\caption{Specify the core site's address in \texttt{core-site.xml}.}
\end{figure}

\item Edit \texttt{yarn-site.xml}, where we list the name node as the
    resource manager.
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:/usr/local/hadoop/etc/hadoop$ sudo vi yarn-site.xml
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{yarnsite.jpg}
	\caption{Specify the (primary) name node as the resource manager in \texttt{yarn-site.xml}.}
\label{yarnsite}
\end{figure}

As in Figure \ref{yarnsite}, add the following but replace
name-private-IP-address with the actual \textbf{private IP address} of the
name node in \texttt{yarn-site.xml}:
\begin{lstlisting}[language=bash, numbers=none]
<configuration>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<name>yarn.nodemanager.aux-services</name>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<value>mapreduce_shuffle</value>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<name>yarn.nodemanager.aux-services.mapreduce.shuffle.class </name>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<value>org.apache.hadoop.mapred.ShuffleHandler</value>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<name>yarn.resourcemanager.scheduler.address</name>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<value>name-private-IP-address:8030</value> </property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<name>yarn.resourcemanager.address</name>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<value>name-private-IP-address:8032</value>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<name>yarn.resourcemanager.webapp.address</name>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<value>name-private-IP-address:8088</value> </property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<name>yarn.resourcemanager.resource-tracker.address</name>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<value>name-private-IP-address:8031</value>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<name>yarn.resourcemanager.admin.address</name>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<value>name-private-IP-address:8033</value>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</configuration>
\end{lstlisting}

\item Next is \texttt{mapred-site.xml}, where we list \texttt{yarn} as
    resource manager for \texttt{mapreduce} programs. First rename
    \texttt{mapred-site.xml.template} as \texttt{mapred-site.xml}
(using \texttt{mv} command) and then edit it:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:/usr/local/hadoop/etc/hadoop$ sudo mv
mapred-site.xml.template mapred-site.xml
hduser@name-node:/usr/local/hadoop/etc/hadoop$ sudo vi mapred-site.xml
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{mapredsite.jpg}
	\caption{List \texttt{yarn} as resource manager for \texttt{mapreduce} jobs in \texttt{mapred-site.xml}.}
\label{mapredsite}
\end{figure}
As in Figure \ref{mapredsite}, place the following between the
configuration tags in \texttt{mapred-site.xml}:
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<name>mapreduce.framework.name</name>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<value>yarn</value>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</property>
\end{lstlisting}

\item The last file that we should edit is \texttt{hdfs-site.xml}, where we
    list \textsc{hdfs} folders (for storing temporary data), the primary
    name node (i.e., name node), and the secondary name node.
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:/usr/local/hadoop/etc/hadoop$ sudo vi hdfs-site.xml
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{hdfssite.jpg}
	\caption{Configure \texttt{hdfs-site.xml}.}
\label{hdfssite}
\end{figure}
As in Figure \ref{hdfssite}, place the following but replace
private-IP-address with the actual private \textsc{ip}
address of the node between
configuration tags:
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
 <name>dfs.replication</name>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
 <value>2</value>
 \end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
 <description>It is the number of replicated files across HDFS
 </description> </property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
 <name>dfs.namenode.name.dir</name>
 \end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
 <value>file:/home/hduser/mydata/hdfs/namenode</value>
 \end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
 <name>dfs.datanode.data.dir</name>
 \end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
 <value>file:/home/hduser/mydata/hdfs/datanode</value>
 \end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
 <name>dfs.http.address</name>
 \end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
 <value>name-private-IP-address:50070</value>
 \end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
<property>
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
 <name>dfs.secondary.http.address</name>
 \end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
 <value>second-name-node-private-IP-address:50090</value>
 \end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
</property>
\end{lstlisting}

\item Send the five configuration files to other nodes using \texttt{scp}
    command.
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:/usr/local/hadoop/etc/hadoop$ scp hadoop-env.sh
core-site.xml yarn-site.xml hdfs-site.xml mapred-site.xml
hduser@other-node-private-ip-address:/usr/local/hadoop/etc/hadoop
\end{lstlisting}
where \texttt{other-node-private-ip-address} should be replaced by the
actual private \textsc{ip} ddress of a \textsc{vm}.

As shown in Figure \ref{scp}, we send these configuration files
\texttt{hadoop-env.sh, core-site.xml, yarn-site.xml, hdfs-site.xml}, and
\texttt{mapred-site.xml} to the other three nodes in the cluster.
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{scp.jpg}
	\caption{Send configuration files to other nodes.}
\label{scp}
\end{figure}

\item Edit files \texttt{masters} and \texttt{slaves} at the name node.
    First list private \textsc{ip} addresses of name nodes in the \texttt{masters}
    file:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:/usr/local/hadoop/etc/hadoop$ sudo vi masters
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{masterfile.jpg}
	\caption{List the primary name node and the secondary name node in the \texttt{masters} file.}
\label{masters}
\end{figure}

Then list private \textsc{ip} addresses of data nodes in the \texttt{slaves} file:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:/usr/local/hadoop/etc/hadoop$ sudo vi slaves
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{slavefile.jpg}
	\caption{List all data nodes in the \texttt{slaves} file. Secondary name node can also be listed as a data node.}
\label{slaves}
\end{figure}

\end{enumerate}

We have finished configuring all nodes in the cluster! After building this
multi-node cluster, we will test our system in the next subsection.

\subsection{Start Hadoop Cluster and Run Existing Programs}

In what follows, we will start the hadoop cluster and then run an existing
mapreduce program to examine if our installation is successful.

\begin{enumerate}
\item At the name node, let us reformat our name node and start the
    \textsc{dfs} (Distributed File System) and then resource manager
    \texttt{yarn}).
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ hdfs namenode -format
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ start-dfs.sh
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ start-yarn.sh
\end{lstlisting}

Use \texttt{jps} command to view the status of each node:
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{jpsmaster.jpg}
	\caption{The status of the name node.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{jps2nd.jpg}
	\caption{The status of the secondary name node.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{jpsslave.jpg}
	\caption{The status of other data node.}
\end{figure}

\item At the name node, submit and run (using \texttt{hadoop jar}) an
    existing \texttt{mapreduce} program saved in the
    \texttt{hadoop-mapreduce-examples-2.8.4.jar} file:
\begin{lstlisting}[language=bash, numbers=none]
 hduser@name-node:~$ hadoop jar
    /usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.8.4.jar
    pi 10 1000
\end{lstlisting}
where \texttt{pi} is the name of a java class that computes $\pi$'s value,
$10$ is the number of map tasks, and samples per map is $1000$. One can
improve accuracy of the output by increasing samples per map. When it
finishes, we should see the following output:
\begin{figure}[H]
	\centering
	\includegraphics[width=5.8in]{runpi.jpg}
	\caption{Run a mapreduce job \texttt{pi} to compute $\pi=3.1408$.}
\end{figure}

\item Stop the whole cluster at the name node:
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ stop-dfs.sh
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
hduser@name-node:~$ stop-yarn.sh
\end{lstlisting}

\item Stop all \textsc{vm}s at aws.amazon.com.
\end{enumerate}

While in this lab we run an existing program on our hadoop cluster at
\textsc{aws}, in next lab we will write our own mapreduce program and run our
own \texttt{.jar} file.

%%%%%% END PREFIX %%%%%%%

%%%%%% END MAIN CONTENT OF LAB %%%%%%%

%\begin{lstlisting}[language=C]
%main() {
%    printf("hello\n");
%    exit(0);
%}
%\end{lstlisting}

%\lstinputlisting[language=C,label=lst:Cprogram]{src/program.c}


%%%%%% END MAIN CONTENT OF LAB%%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

\section{Thematic Takeaways}

\begin{itemize}
\item A multi-node hadoop cluster consists of the (primary) name node, the
    secondary name node, and multiple data nodes.
\item The name node manages all resources in the cluster and assign tasks
    to data nodes. The secondary name node often works as a data node and
    more importantly, it will become the new (primary) name node in case of
    a primary name node failure.
\end{itemize}

\section{Lab Summary}

In this lab, we set up passwordless connections for nodes in our hadoop
cluster, installed java and hadoop software on each node, configured the
cluster, and ran an existing mapreduce program on the cluster to see if our
system was built successfully.

\section{Key Term}

Multi-node Hadoop clusters.

%\section{Bibliographic Notes}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
