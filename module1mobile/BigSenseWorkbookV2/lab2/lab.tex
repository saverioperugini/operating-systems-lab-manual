%uncomment following line when compiling entire book
\graphicspath{{"module1mobile/BigSenseWorkbookV2/lab2/figs/"}}
% uncomment following line when compiling individual lab
%\graphicspath{{"figs/"}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpg,.png}

\chapter{Field Work Data}
\label{CHAP:BigSenseFieldWorkData}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Authors: Sumit Khanna, Andrew Rettig, and Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

\begin{itemize}

\item Creation of a small field study to collect interesting data to tell a
story.

\item Management of IoT devices to achieve the data goals of a real project.

\item Visualizazation IoT data to relate the findings to the desired audience.

\end{itemize}

\section{Resources and Getting Help}

%%%%%% END PREFIX %%%%%%%

\section{Introduction}

In this lab, you will use the Beagle Boards you setup in the prior lab and move
them to different locations. From these locations, you will transmit sensor
data. Then you will be able to query sensor information based on the distance
from a geospatial point (longitude and latitude), create some interesting
charts and even map the data. 

\section{Create a Study}

The purpose of this chapter is to take data collection a step further and have
some fun with it.  The more creative you get with the data collection the
better your lab will be.  Remember that you will be creating charts and maps to
display the data so try to even create a story.  At  the very minimum your will
run your Beagle Boards at least thirty minutes at two locations. As your
devices are running, you can ensure you are getting data with a GPS lock by
running the same query you used in the previous chapter.

\url{<Server IP/url>/Query/Latest/100.table.html?RelayID=<YourUniqueID>}

Check the fields marked Longitude and Latitude to ensure GPS data is being
transmitted. You should be able to go to this address and refresh the site from
a mobile web browser on a phone, tablet or laptop. Be sure to keep note of what
times you have the Beagle Board running and the location. You will need it
later.

For a more interesting lab I also suggest taking data while moving.  The BBB
will store the data while it is not connected.  When you reconnect the BBB to
the Internet it will then upload the data.  So once it comes back within wifi
range it will upload the data.  You can test this process with short trips away
from the wifi or Ethernet and then return with the data.  Continue to check the
web services for the data dumps.  Another option is to create a hotspot with
your phone and then use the wifi dongle to connect your BBB to your phone for
continuous connection.  Additionally, the site bigsense.io has a couple simple
commands to install sqlite on the BBB for storing data even while powered
off.

\section{Data Formats}

The following questions will have you create queries to retrieve your sensors’
data from BigSense. Be sure to include the query you used to retrieve each data
set in your lab write up. You do not need to include the data returned.

Note that in the following questions, we have changed the URL extensions from
\texttt{table.html} to \textsc{csv}. The extension indicates the format that
data is returned to us from BigSense. Up until now we have been used
\textsc{html} tables (\texttt{table.html}) because it displays easily in a web
browser. Comma separated value (\textsc{csv}) files are another support format
from BigSense.

The full documentation for queries and supported formats can be found at the
following address:

\url{https://bigsense.io/display/sense/Webservice+API}

\noindent You can then view these files in either a text editor or import them
into a spreadsheet application such as OpenOffice Calc or Microsoft Excel. In
most web browsers, using the \textsc{csv} extension will prompt you to download
the results. You may want to use table.html first so you can view the result in
the browser and then switch to \textsc{csv} to download them. Both formats
contain the same data, just using different formats (web-html verses coma
separated values). If your web browser opens the \textsc{csv} file within the
web browser, you should be able to save the result by going to File>Save As.

\section{Questions}

\begin{enumerate}

    \item Download a file containing the results of your Beagle Board for a
date range.
    
    You can query for a date range by using a URL like the following:
    
\url{<Server IP/url>/Query/DateRange/<YYYYMMDD>/<YYYYMMDD>.csv?RelayID=<YourUniqueID>}
    
Replace both of the YYYYMMDD with a date in the ISO 8601 date format
(Year-Month-Date). Find a date range that gives you data points from your
Beagle Board at one of your remote locations. Do not forget it is possible to
view your data in the browser:
    
    \url{<Server IP/url>/Query/DateRange/<YYYYMMDD>/<YYYYMMDD>.table.html?RelayID=<YourUniqueID>}
    
    \item Download a file containing averages for a given date range.
    
You can query BigSense for averages over a given time range. For example:
    
\url{<Server IP/url>/Aggregate/Average/DateRange/20150101/20150130/1440.csv?SensorType=Temperature&RelayID=Test01}
    
\noindent Here, we are asking BigSense for the Average (mean) of all the data
from January 1st, 2015 to January 30th, 2015, for the SensorType of Temperature
and a RelayID of Test01. We want the averages for each 1440 minute period (1
day). Changing the 1440 to 60 would give us average temperatures for each
hour.
    
For your sensors, select a date range of a single day and retrieve the average
readings of your sensors for each hour of that day.  Do you have any concerns
with the data that you returned?  Is there a better query to represent your
data?
    
\item Design a query that will return your sensors using the WithinMetersFrom
constraint.
    
BigSense can also query sensors based upon a location when using date or
timestamp ranges, but not when computing averages. 
    
\url{<Server IP/url>/Query/DateRange/20150101/20150102.csv?WithinMetersFrom=long35.4lat39.6r200&RelayID=Test01 }\\
    
\noindent
In the query shown above, we are requesting all the data between the 1st
and 2nd of January 2015, with a RelayID of Test01 and within a 200 meter radius
of longitude 35.4 and latitude 39.6. The constraint WithinMetersFrom takes a
value in the form of long'x'lat'y'r'n', where 'x' is the longitude in decimal
degrees, 'y' is the latitude in decimal degrees and 'n' is the radius from that
location in meters.
    
\noindent
Use one of the locations from your Beagle Board and create a query with the
WithinMetersFrom constraint that has a wide enough radius to show data from
your second location.
    
\textbf{Note:  We have seen some errors with the WithinMetersFrom constraint.
If you get odd results just be sure to document those results and why you
believe that they are correct or incorrect.}
    
    \item Creating a table.
    
Now that you have basic experience with querying your data, lets create some
visualizations for your data.  (For both 4 and 5, use creativity to create
visuals that are going to enhance your data while telling a story)  Use the
\texttt{.csv} format for downloading your selected/filtered data and displaying
that data on a chart.  You are welcome to create more than one chart if this
will make your data more interesting.  It is also important to write up a
description for your data so the audience can easily understand and analyze
your results.
    
Example: You could have an example chart with flat 75 degrees until the
temperature sensor was placed in direct sunlight and the temperature rose to 85
degrees.
    
Example:  Here is the temperature dropping as I drove faster in my car with the
temperature sensor out of the window.  The temperature would rise when I would
be stopped at a light.  Also remember that spatial location will include the
speed, so this also can be used to make your lab more interesting.  A chart can
include more than one variable. 
    
(Future work can include Data Science.  The charts and analysis included can be
automated with a data science platform like Jupiter notebooks or Datazar.
These platforms can even automatically pull live data from the \textsc{api}.)
    
\item Making a map.\\
    
Now we are putting everything together.  We are going to display the
temperature data with a spatial location.  If you were successful gathering
mobile data this will create the most interesting maps.  Please include a
screen shot of your maps and descriptions.  A good description will help the
audience understand your map and the resulting temperature readings.  It is
also possible to color code your data for even easier understanding of warmer
and colder temperatures represented on the map.  There are numerous simple
mapping applications to display the data including google maps, ArcGIS Online,
or mapping within a data science platform.  I suggest trying the free ArcGIS
Online approach first.
    
\end{enumerate}

%%%%%% END MAIN CONTENT OF LAB%%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

\section{Thematic Takeaways}

\begin{itemize}

\item Learning IoT must begin with applied testing and
simple experimentation to acquire an understanding of what is possible.

\item HTTP is an excellent protocol for IoT when power is not an issue with
basic commands and lightweight clients.

\item Story driven IoT is important as the data needs to tell a story and
closely service the project or client.   

\end{itemize}

\section{Lab Summary}

In this lab, you have used BigSense to query averages as well as individual
readings from your sensors by various means including date ranges and
geospatial locations. Query sets based on locations and relative locations are
known as Spatial Queries. These calculations can take into account the
curvature of the Earth depending on the database backend used. The resulting
visualizations are crucial for creating interest in your data.  Spending just a
little extra time to visualize your data will help to tell your story.

\section{Key Terms}

\textsc{csv}, geospatial, \textsc{http}, query, \textsc{url}.

%\section{Bibliographic Notes}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
