% uncomment following line when compiling entire book
\graphicspath{{"module0fundamentals/Linux/figs/"}}
% uncomment following line when compiling individual chapter
%\graphicspath{{"figs/"}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpg,.png}

%\renewcommand{\chaptername}{Lab}
%\renewcommand{\thechapter}{1}
\chapter{Linux Essentials Lab: Files and I/O}
\label{CHAP:LINUX1}

%\pagenumbering{arabic}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Author: Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

\begin{itemize}
\item Establish an understanding of the terminal and command line
\item Establish an understanding of files, and the Linux file system
\item Establish an understanding of file manipulation and management
\item Establish an understanding of standard I/O and file streams
\item Establish an understanding of standard error and file descriptors
\end{itemize}

\section{Lab Overview}
This lab introduces the following foundational Linux topics: 
\begin{itemize}
\item The terminal and command line
\item Files, and the Linux file system
\item File manipulation and management
\item Standard I/O and file streams
\item Standard error and file descriptors
\end{itemize}

\subsection{Setup} Included with this lab is a preconfigured VirtualBox image
running Debian 9.1 x86-64.  The home directory of the \texttt{student} user
contains a directory called \texttt{lab-linux-files-io} which contains all of
the files necessary to complete this lab. 

\section{Resources and Getting Help} 

\begin{itemize}
    \item Files \& Directories (manipulation \& management) Notes:
       \url{https://perugini.cps.udayton.edu/teaching/books/SPUC/www/lecture_notes/files1.html}

    \item System Libraries and I/O Notes:
    \url{https://perugini.cps.udayton.edu/teaching/books/SPUC/www/lecture_notes/librariesIO.html}

    \item Linux Manpages (e.g., \texttt{\$ man ls})

\item More information on any of the programs, commands, and functions
mentioned in this lab can be referenced in their corresponding \texttt{man}
pages.  Reading the \texttt{man} pages is encouraged before turning to other
resources. 

\end{itemize}

%%%%%% END PREFIX %%%%%%%

%%%%%% BEGIN MAIN CONTENT OF LAB %%%%%%%

%\section{Lab Tasks}
\section{Steps}

Complete each of the following tasks and include a screenshot (usually
of the most recently executed command line) and a brief 1--2 sentence
description. 

    \begin{enumerate}

        \item \textbf{The Command Line}

	Boot up the provided virtual machine, and log in as \texttt{student}.
Upon logging in, the screen displays a shell prompt.  This is where the user
enters commands to interact with the system.

        \item \textbf{File system navigation and manipulation}

        Everything in Linux is a file---including directories, programs, and devices.  
        To list the contents of a directory, enter the \texttt{ls} command.  
        When interacting through the command line it is helpful to know the present working directory. 
        The \texttt{pwd} commands prints the \texttt{p}resent \texttt{w}orking \texttt{d}irectory. 
        Try it now; upon logging in as \texttt{student}, the working directory should be \texttt{/home/student}. 

The file system can be viewed as a tree as shown in Figure \ref{fig:filesystree}. 
\texttt{/} is the root directory with common system folders as child directories. 
%\texttt{bin}, \texttt{sbin}, \texttt{home}, \texttt{src}, \texttt{lib}, \texttt{etc}, \texttt{usr}, \texttt{dev}. 

\begin{figure}
\center
\includegraphics[scale=0.64]{filesystree.png}
\caption{Linux file system can be visualized as an upside-down tree.}
\label{fig:filesystree}
\end{figure}

The \texttt{tree} command pretty-prints a visualization of a given directory. 
For instance, enter the command \texttt{tree -d} to print the directory structure for the current directory. 
The \texttt{-d} argument specifies \texttt{tree} to only print directories (instead of all files).

\textit{Task:} Provide a screenshot of the tree command and its output in the terminal. 

\textbf{Absolute versus relative paths:} Absolute paths always start with root, \texttt{/}. 
For example, the output of the previous \texttt{pwd} command was \texttt{/home/student}. 
This starts with the root directory, \texttt{/}, followed by subdirectory \texttt{home}, and then \texttt{student}---a subdirectory of \texttt{home}. 

The \texttt{cd} command is a frequently used command that changes the working directory to the one specified. 
To use, type \texttt{cd}, followed by the path of the directory. 

\textit{Task:} Use the \texttt{cd} command to change the working directory to \texttt{lab-linux-files-io} using an absolute path. 
Show proof of this with a screenshot of the \texttt{cd} command followed by \texttt{pwd}. 

Enter \texttt{ls -a}. 
This passes one argument to \texttt{ls}. The \texttt{-a} shows all files including `hidden' files which begin with a `\texttt{.}'. 
In the output of \texttt{ls -a}, notice two special entries: \texttt{.} and \texttt{..}. 
\texttt{.} is a link to the current working directory. 
Try issuing \texttt{cd .} followed by \texttt{pwd} and observe that the working directory does not change. 
On the other hand, \texttt{..} is a link to the \textit{parent} of the current working directory. 
To demonstrate, execute \texttt{cd ..} followed by \texttt{pwd}; the working directory should now be \texttt{/home/student}. 
Now, how to return to \texttt{lab-linux-files-io} without typing the absolute path? 
The answer is with relative paths. 

Relative paths implicitly start with the present working directory. 
For example, to return to the \texttt{lab\-linx-files-io} directory, simply execute: \texttt{cd lab-linux-files-io}. 
Do this now.

\textit{Task:} From the \texttt{lab-linux-files-io} directory, use \texttt{cd} to change directories to \texttt{/home/student/tmp} using a relative path. 
\textit{Hint:} Use \texttt{..} in the path. 
Show proof of this with a screenshot of the \texttt{cd} command followed by \texttt{pwd}. 

\textbf{Navigation shortcuts:} Following are a few useful shortcuts for navigating and using file paths. 
\begin{itemize}
\item \texttt{cd} --- changes to user's home directory
\item \texttt{cd \~ -} --- changes to previous working directory
\item \texttt{\~} is also an alias for the user's home directory---in this case \texttt{/home/student}. 
To demonstrate, return to the \texttt{lab-linux-files-io} directory by entering the following command: \texttt{cd \~/lab-linux-files-io}. 
\end{itemize}

\textbf{Man pages:} Short for \texttt{man}ual, the \texttt{man} pages are a
resource for looking up information on programs, utilities, and functions found
on a Linux system. 

\textit{Task:} Read the \texttt{man} page for \texttt{ls} by entering
\texttt{man ls}.  Find the argument that prints \texttt{ls} in the `long
listing format' and run it on the command line within the current working
directory (\texttt{lab-linux-files-io}).  Provide proof of this with a
screenshot of the output of the \texttt{ls} command with the appropriate
argument.  \textit{Hint:} General information about \texttt{man} pages can be
found by running \texttt{man man}.

%\paragraph{File manipulation:} [mv, cp, rm, mkdir]
% Introduce these commands throughout the other tasks

\item \textbf{Standard I/O}

An important aspect of a Linux system is the concepts of file
streams---especially important since files can represent anything from data to
programs to devices.  By default, \texttt{stdin}, \texttt{stdout}, and
\texttt{stderr} are automatically open.  One immediate example of a stream is
the interaction on the shell prompt---as depicted in Figure \ref{fig:ioflow}.
Commands receive input from \texttt{stdin} through a keyboard device, and send
their output through \texttt{stdout}---the console or terminal. 

\begin{figure}
	\center
	\includegraphics[scale = 0.64]{IOflow.png}
	\caption{Standard input and output between keyboard and terminal.}
    \label{fig:ioflow}
\end{figure}

Shells are designed in a way that they are unaware of which devices or files
are on either end of the stream.  This allows for \textit{redirection} of I/O.
To demonstrate, consider the program \texttt{cat}---short for
con\texttt{cat}enate.  \texttt{cat} takes as argument one or more files and
prints them on standard output.  Try running \texttt{cat test1 test2}. 

\textbf{Redirecting standard I/O:} Built-in syntax in the shell allows for
redirecting standard I/O to other devices, programs, or anything that Linux
considers a file. 

The following tasks illustrate each redirection symbol:

\begin{enumerate}
% stdout redirection, overwrite, >
\item \texttt{[}$\mathtt{<}$\texttt{\textit{command}}$\mathtt{>}$\texttt{] > [}$\mathtt{<}$\texttt{\textit{file}}$\mathtt{>}$\texttt{]}
redirects a command's output to the specified file instead of the terminal. 
If the specified file does not exist, then it is created. 
Otherwise, this overwrites whatever contents are in the file. 

\textit{Task:} Redirect the output of the \texttt{ls} command with `long
listing format' (found in the previous step) into a file named
\texttt{ls.long}.  Provide proof with a screenshot of the command followed by
\texttt{cat ls.long} to print the contents of the new file. 

% stdout redirection, append, >>

\item \texttt{[}$\mathtt{<}$\texttt{\textit{command}}$\mathtt{>}$\texttt{] >>
[}$\mathtt{<}$\texttt{\textit{file}}$\mathtt{>}$\texttt{]} also redirects
output to the specified file.  The difference is that this symbol appends
output to the file instead of overwriting it. 

\textit{Task:} First copy the \texttt{test1} file using the \texttt{cp}
command.  The syntax for \texttt{cp} (which is also found in its \texttt{man}
page, \texttt{man cp}) is: \texttt{cp}
$\mathtt{<}$\texttt{\textit{source}}$\mathtt{>}$
$\mathtt{<}$\texttt{\textit{destination}}$\mathtt{>}$.  Name the new file
\texttt{test1-copy}.  Now \textit{append} the output of \texttt{test1} (using
\texttt{cat}) to the new file \texttt{test1-copy}.  Provide proof with a
screenshot of the command followed by \texttt{cat test1-copy} to print the
contents of the appended file. 

% stdin redirection, <

\item \texttt{[}$\mathtt{<}$\texttt{\textit{command}}$\mathtt{>}$\texttt{] <
[}$\mathtt{<}$\texttt{\textit{file}}$\mathtt{>}$\texttt{]} sends command input
from the specified file instead of standard input (the keyboard).  To
demonstrate its use, consider the command \texttt{wc -l test2}.  This is a
counting utility that counts the number of bytes, characters, words, and lines
in a given file---in this case \texttt{test2}.  The \texttt{-l} argument prints
the line count.  The filename can also be omitted from the command and provided
through input redirection. 

\textit{Task:} Using the input redirection symbol, \texttt{<}, redirect
\texttt{test2} into the \texttt{wc -l} command.  Provide a screenshot of the
command and answer the following questions. 

\begin{itemize}

\item What is the difference between this command and \texttt{wc -l test2}? 

\item What happens if \texttt{wc -l} is executed without any file arguments? 

\textit{Hint:} Type $\mathtt{<}$\texttt{\textit{ctrl-d}}$\mathtt{>}$, the
`Control' key and `D' key simultaneously, to send the `End-Of-File' signal to
the program.  This tells the command to stop reading from standard input (i.e.,
the  keyboard). 

\end{itemize}

% command < input > output

\item \texttt{[}$\mathtt{<}$\texttt{\textit{command}}$\mathtt{>}$\texttt{] < [}
$\mathtt{<}$\texttt{\textit{input}}$\mathtt{>}$\texttt{] > [}
$\mathtt{<}$\texttt{\textit{output}}$\mathtt{>}$\texttt{]}.  I/O redirection
symbols can also be combined in the same command. 

\textit{Task:} Using the \texttt{wc -l} command from the previous task, write a
single command that redirects input from \texttt{test2} and redirect output to
new file \texttt{linecounts}.  Provide proof with a screenshot of the command
followed by \texttt{cat linecounts}. 

% stdin redirection to HERE file, <<
% Who really uses this? Pipes are probably more frequently used. 

% pipes | 

\item \texttt{[}$\mathtt{<}$\texttt{\textit{command1}}$\mathtt{>}$\texttt{] |
[}$\mathtt{<}$\texttt{\textit{command2}}$\mathtt{>}$\texttt{] | ...} .  The
pipe operator, \texttt{|}, passes the output of one command into the input of
the next.  Pipes are powerful mechanisms that glue together commands to
efficiently compute something in a single line. 

\textit{Task:} Write a single command that uses the pipe operator to count the
total number of lines in the files \texttt{test1}, \texttt{test2}, and
\texttt{test1-copy}.  The output of the command must be a single number.  Show
proof of this with a screenshot of the command and its output. 

\end{enumerate}

\item \textbf{Standard Error}

Linux uses a separate output stream for errors called standard error
(\texttt{stderr}).  Both \texttt{stdout} and \texttt{stderr} look the same when
printed to the terminal.  Redirection of standard output---through \texttt{>}
and \texttt{>>}---does not affect standard error.  Explicitly redirecting
standard error requires understanding file descriptors. 

\textit{File descriptors} are handles (non-negative integers) assigned by the
operating system to uniquely identify opened files on the system.  Since
everything is a file in Linux, \texttt{stdin}, \texttt{stdout}, and
\texttt{stderr} are no exception.  They are reserved file descriptors 0, 1, and
2. 

\begin{itemize}

\item 0 for \texttt{stdin}. 
This is implicit in \texttt{<} when redirecting standard input. 

\item 1 for \texttt{stdout}. 
This is implicit in \texttt{>} and \texttt{>>} when redirecting standard output. 

\item 2 for \texttt{stderr}.  This is required when redirecting standard error.
For example, `\texttt{wc -q 2> errs}' redirects error output to file
\texttt{errs}. 

\end{itemize}

\textit{Task:} For the command \texttt{ls test1 nosuchfile}, redirect
\texttt{stdout} to file \texttt{ls.out} and redirect \texttt{stderr} to file
\texttt{ls.err}.  Provide proof of this with a screenshot of the command
followed by \texttt{cat ls.out} and \texttt{cat ls.err}. 

\textbf{Redirecting both standard output and standard error:} To redirect both
standard output and standard error to the same file, use the special syntax
\texttt{[}$\mathtt{<}$\texttt{\textit{command}}$\mathtt{>}$\texttt{] >
[}$\mathtt{<}$\texttt{\textit{filename}}$\mathtt{>}$\texttt{] 2>\&1}.  This is
read as `send file descriptor 2 to the same place as file descriptor 1.'
\textit{Note:} Modern shells now have a shorter alias for this syntax:
\texttt{[}$\mathtt{<}$\texttt{\textit{command}}$\mathtt{>}$\texttt{] \&>
[}$\mathtt{<}$\texttt{\textit{filename}}$\mathtt{>}$\texttt{]}. 

\textit{Task:} What happens if the command from the previous task redirects
\texttt{stdout} and \texttt{stderr} to the same file?  Why does this happen?

\end{enumerate}

\section{Submission Guidelines}

Compile all of the tasks into a document with the required screenshots and
answers to any questions. 

%%%%%% END MAIN CONTENT OF LAB%%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

%\section{Thematic Takeaways}

%\begin{itemize}
%\item ~
%\item ~
%\end{itemize}

%\section{Lab Summary}

%\section{Key Terms}

%\section{Bibliographic Notes}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
