\contentsline {chapter}{Books Referenced in the Labs}{iii}{Doc-Start}%
\contentsline {chapter}{Preface}{iv}{chapter*.1}%
\contentsline {paragraph}{Using the Virtual Machine.}{v}{section*.7}%
\contentsline {paragraph}{Using the Raspberry Pi.}{v}{section*.8}%
\contentsline {paragraph}{Computer Security Labs 18--19 in Module I and Security Lab 29 in Module II}{vi}{section*.11}%
\contentsline {paragraph}{The Internet-of-Things Lab in Module I: \nameref {CHAP:IOTCAMERA}}{vi}{section*.12}%
\contentsline {paragraph}{The Internet-of-Things Labs 21--23 in Module I}{vi}{section*.13}%
\contentsline {paragraph}{Module III: Cloud Computing and Big Data Processing.}{vi}{section*.14}%
\contentsline {chapter}{Acknowledgments}{xi}{section*.28}%
\contentsline {chapter}{List of Figures}{xix}{chapter*.30}%
\contentsline {chapter}{List of Tables}{xxiii}{chapter*.31}%
\contentsline {chapter}{\numberline {1}Virtual Machine Set-up Lab}{2}{chapter.1}%
\contentsline {section}{\numberline {1.1}Lab Learning Outcomes}{2}{section.1.1}%
\contentsline {section}{\numberline {1.2}Resources and Getting Help}{2}{section.1.2}%
\contentsline {section}{\numberline {1.3}Steps}{2}{section.1.3}%
\contentsline {chapter}{\numberline {2}Raspberry Pi Set-up Lab}{14}{chapter.2}%
\contentsline {section}{\numberline {2.1}Lab Learning Outcomes}{14}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Setup}{14}{subsection.2.1.1}%
\contentsline {section}{\numberline {2.2}Steps}{15}{section.2.2}%
\contentsline {chapter}{\numberline {3}Secure Copy (\texttt {scp})}{37}{chapter.3}%
\contentsline {section}{\numberline {3.1}Lab Learning Outcomes}{37}{section.3.1}%
\contentsline {section}{\numberline {3.2}Resources and Getting Help}{37}{section.3.2}%
\contentsline {section}{\numberline {3.3}Steps}{37}{section.3.3}%
\contentsline {section}{\numberline {3.4}Thematic Takeaways}{39}{section.3.4}%
\contentsline {chapter}{\numberline {4}C Programming with Pointers Lab}{41}{chapter.4}%
\contentsline {section}{\numberline {4.1}Lab Learning Outcomes}{41}{section.4.1}%
\contentsline {section}{\numberline {4.2}Lab Overview}{41}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Setup}{41}{subsection.4.2.1}%
\contentsline {section}{\numberline {4.3}Resources and Getting Help}{42}{section.4.3}%
\contentsline {section}{\numberline {4.4}Steps}{42}{section.4.4}%
\contentsline {chapter}{\numberline {5}Learning Pointers}{46}{chapter.5}%
\contentsline {section}{\numberline {5.1}Lab Learning Outcomes}{46}{section.5.1}%
\contentsline {section}{\numberline {5.2}Resources and Getting Help}{46}{section.5.2}%
\contentsline {section}{\numberline {5.3}Steps}{46}{section.5.3}%
\contentsline {section}{\numberline {5.4}Thematic Takeaways}{51}{section.5.4}%
\contentsline {chapter}{\numberline {6}Linux Essentials Lab: Files and I/O}{52}{chapter.6}%
\contentsline {section}{\numberline {6.1}Lab Learning Outcomes}{52}{section.6.1}%
\contentsline {section}{\numberline {6.2}Lab Overview}{52}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Setup}{53}{subsection.6.2.1}%
\contentsline {section}{\numberline {6.3}Resources and Getting Help}{53}{section.6.3}%
\contentsline {section}{\numberline {6.4}Steps}{53}{section.6.4}%
\contentsline {section}{\numberline {6.5}Submission Guidelines}{59}{section.6.5}%
\contentsline {chapter}{\numberline {7}Command-Line Arguments}{60}{chapter.7}%
\contentsline {section}{\numberline {7.1}Lab Learning Outcomes}{60}{section.7.1}%
\contentsline {section}{\numberline {7.2}Resources and Getting Help}{60}{section.7.2}%
\contentsline {section}{\numberline {7.3}Steps}{60}{section.7.3}%
\contentsline {section}{\numberline {7.4}Thematic Takeaways}{62}{section.7.4}%
\contentsline {chapter}{\numberline {8}Exploring \texttt {cat}}{63}{chapter.8}%
\contentsline {section}{\numberline {8.1}Lab Learning Outcomes}{63}{section.8.1}%
\contentsline {section}{\numberline {8.2}Resources and Getting Help}{63}{section.8.2}%
\contentsline {section}{\numberline {8.3}Steps}{64}{section.8.3}%
\contentsline {section}{\numberline {8.4}Thematic Takeaways}{64}{section.8.4}%
\contentsline {section}{\numberline {8.5}Key Terms}{64}{section.8.5}%
\contentsline {chapter}{\numberline {9}Dynamic Memory Management (\texttt {malloc}, \texttt {calloc}, \texttt {realloc}, and \texttt {free})}{65}{chapter.9}%
\contentsline {section}{\numberline {9.1}Lab Learning Outcomes}{65}{section.9.1}%
\contentsline {section}{\numberline {9.2}Resources and Getting Help}{65}{section.9.2}%
\contentsline {section}{\numberline {9.3}Steps}{66}{section.9.3}%
\contentsline {section}{\numberline {9.4}Thematic Takeaways}{68}{section.9.4}%
\contentsline {chapter}{\numberline {10}Compiling C in Linux: Storage and Linkage Classes; and Threads and Thread-safe Functions}{69}{chapter.10}%
\contentsline {section}{\numberline {10.1}Lab Learning Outcomes}{69}{section.10.1}%
\contentsline {section}{\numberline {10.2}Resources and Getting Help}{69}{section.10.2}%
\contentsline {section}{\numberline {10.3}Steps}{70}{section.10.3}%
\contentsline {subsection}{\numberline {10.3.1}Collecting necessary files for the lab}{70}{subsection.10.3.1}%
\contentsline {subsection}{\numberline {10.3.2}Exploring separate compilation and linking}{70}{subsection.10.3.2}%
\contentsline {subsection}{\numberline {10.3.3}Storgage Class and Linkage Classes}{71}{subsection.10.3.3}%
\contentsline {section}{\numberline {10.4}Thematic Takeaways}{74}{section.10.4}%
\contentsline {section}{\numberline {10.5}Key Terms}{74}{section.10.5}%
\contentsline {chapter}{\numberline {11}Exploring Time-shared OSs and Threads}{76}{chapter.11}%
\contentsline {section}{\numberline {11.1}Lab Learning Outcomes}{76}{section.11.1}%
\contentsline {section}{\numberline {11.2}Resources and Getting Help}{76}{section.11.2}%
\contentsline {section}{\numberline {11.3}Steps}{77}{section.11.3}%
\contentsline {section}{\numberline {11.4}Thematic Takeaways}{80}{section.11.4}%
\contentsline {section}{\numberline {11.5}Key Terms}{80}{section.11.5}%
\contentsline {chapter}{\numberline {12}Exploring Job \& Process Scheduling Algorithms}{81}{chapter.12}%
\contentsline {section}{\numberline {12.1}Lab Learning Outcomes}{81}{section.12.1}%
\contentsline {section}{\numberline {12.2}Resources and Getting Help}{81}{section.12.2}%
\contentsline {section}{\numberline {12.3}Introduction}{82}{section.12.3}%
\contentsline {section}{\numberline {12.4}Steps}{82}{section.12.4}%
\contentsline {section}{\numberline {12.5}Thematic Takeaways}{83}{section.12.5}%
\contentsline {section}{\numberline {12.6}Key Terms}{83}{section.12.6}%
\contentsline {chapter}{\numberline {13}Job and Semaphore Processing Simulation Project}{85}{chapter.13}%
\contentsline {section}{\numberline {13.1}Lab Learning Outcomes}{85}{section.13.1}%
\contentsline {section}{\numberline {13.2}Resources and Getting Help}{85}{section.13.2}%
\contentsline {section}{\numberline {13.3}Introduction}{86}{section.13.3}%
\contentsline {section}{\numberline {13.4}Project Specification}{86}{section.13.4}%
\contentsline {subsection}{\numberline {13.4.1}Problem}{86}{subsection.13.4.1}%
\contentsline {subsection}{\numberline {13.4.2}Detailed Description and Requirements}{86}{subsection.13.4.2}%
\contentsline {subsection}{\numberline {13.4.3}Event Collisions}{90}{subsection.13.4.3}%
\contentsline {subsection}{\numberline {13.4.4}Additional Requirements}{90}{subsection.13.4.4}%
\contentsline {subsection}{\numberline {13.4.5}Hints and Notes}{90}{subsection.13.4.5}%
\contentsline {subsection}{\numberline {13.4.6}Test Data: Sample Input and Output Streams}{91}{subsection.13.4.6}%
\contentsline {subsection}{\numberline {13.4.7}Project Evaluation}{92}{subsection.13.4.7}%
\contentsline {section}{\numberline {13.5}Implementation}{93}{section.13.5}%
\contentsline {subsection}{\numberline {13.5.1}Tuning Simulation Parameters}{93}{subsection.13.5.1}%
\contentsline {subsection}{\numberline {13.5.2}Tuning Simulation Parameters and Process Control Block (PCB)}{93}{subsection.13.5.2}%
\contentsline {subsection}{\numberline {13.5.3}Main Event Loop}{93}{subsection.13.5.3}%
\contentsline {subsection}{\numberline {13.5.4}Implementation Languages}{95}{subsection.13.5.4}%
\contentsline {section}{\numberline {13.6}Student Feedback}{95}{section.13.6}%
\contentsline {section}{\numberline {13.7}Conclusion}{95}{section.13.7}%
\contentsline {section}{\numberline {13.8}Thematic Takeaways}{95}{section.13.8}%
\contentsline {section}{\numberline {13.9}Key Terms}{96}{section.13.9}%
\contentsline {chapter}{\numberline {14}Exploring Process Creation in Linux: The fork System Call}{97}{chapter.14}%
\contentsline {section}{\numberline {14.1}Lab Learning Outcomes}{97}{section.14.1}%
\contentsline {section}{\numberline {14.2}Resources and Getting Help}{97}{section.14.2}%
\contentsline {section}{\numberline {14.3}Steps}{97}{section.14.3}%
\contentsline {section}{\numberline {14.4}Thematic Takeaways}{98}{section.14.4}%
\contentsline {chapter}{\numberline {15}Token Ring of Processes}{99}{chapter.15}%
\contentsline {section}{\numberline {15.1}Lab Learning Outcomes}{99}{section.15.1}%
\contentsline {section}{\numberline {15.2}Resources and Getting Help}{99}{section.15.2}%
\contentsline {section}{\numberline {15.3}Steps}{100}{section.15.3}%
\contentsline {section}{\numberline {15.4}Thematic Takeaways}{101}{section.15.4}%
\contentsline {section}{\numberline {15.5}Key Terms}{102}{section.15.5}%
\contentsline {chapter}{\numberline {16}Linux Kernel Lab---System Calls}{103}{chapter.16}%
\contentsline {section}{\numberline {16.1}Lab Overview}{103}{section.16.1}%
\contentsline {subsection}{\numberline {16.1.1}Setup}{103}{subsection.16.1.1}%
\contentsline {section}{\numberline {16.2}Resources and Getting Help}{103}{section.16.2}%
\contentsline {section}{\numberline {16.3}Steps}{104}{section.16.3}%
\contentsline {section}{\numberline {16.4}Submission Guidelines}{110}{section.16.4}%
\contentsline {chapter}{\numberline {17}Adding a New System Call: Compiling a Custom Kernel}{111}{chapter.17}%
\contentsline {section}{\numberline {17.1}Lab Learning Outcomes}{111}{section.17.1}%
\contentsline {section}{\numberline {17.2}Resources and Getting Help}{111}{section.17.2}%
\contentsline {chapter}{\numberline {18}Buffer Overflow Attacks and Defenses}{117}{chapter.18}%
\contentsline {section}{\numberline {18.1}Lab Learning Outcomes}{117}{section.18.1}%
\contentsline {section}{\numberline {18.2}Preparation}{118}{section.18.2}%
\contentsline {section}{\numberline {18.3}Lab tasks}{120}{section.18.3}%
\contentsline {subsection}{\numberline {18.3.1}Task 1: Identify the buffer size and overwrite the return address}{120}{subsection.18.3.1}%
\contentsline {subsection}{\numberline {18.3.2}Task 2: Identify the buffer address}{123}{subsection.18.3.2}%
\contentsline {subsection}{\numberline {18.3.3}Task 3: Construct the payload}{125}{subsection.18.3.3}%
\contentsline {subsection}{\numberline {18.3.4}Task 4: Launching the attack}{127}{subsection.18.3.4}%
\contentsline {subsection}{\numberline {18.3.5}Task 5: A Buffer Overflow Attack Countermeasure}{129}{subsection.18.3.5}%
\contentsline {section}{\numberline {18.4}Thematic Takeaways}{129}{section.18.4}%
\contentsline {section}{\numberline {18.5}Lab Summary}{130}{section.18.5}%
\contentsline {section}{\numberline {18.6}Key Terms}{130}{section.18.6}%
\contentsline {section}{\numberline {18.7}Bibliographic Notes}{130}{section.18.7}%
\contentsline {chapter}{\numberline {19}Android Application Reverse Engineering}{131}{chapter.19}%
\contentsline {section}{\numberline {19.1}Lab Learning Outcomes}{131}{section.19.1}%
\contentsline {section}{\numberline {19.2}Preparation}{131}{section.19.2}%
\contentsline {paragraph}{Download the Android application.}{132}{section*.37}%
\contentsline {paragraph}{Download the Apktool tool.}{132}{section*.38}%
\contentsline {section}{\numberline {19.3}Lab Tasks}{133}{section.19.3}%
\contentsline {subsection}{\numberline {19.3.1}Task 1: Upload and Run an APK Android app in an Android Emulator}{133}{subsection.19.3.1}%
\contentsline {subsection}{\numberline {19.3.2}Task 2: Reverse Engineer and Modify a Hybrid Android app}{136}{subsection.19.3.2}%
\contentsline {subsection}{\numberline {19.3.3}Task 3: Rebuild a Modified Android app}{140}{subsection.19.3.3}%
\contentsline {section}{\numberline {19.4}Thematic Takeaways}{143}{section.19.4}%
\contentsline {section}{\numberline {19.5}Lab Summary}{143}{section.19.5}%
\contentsline {section}{\numberline {19.6}Key Terms}{144}{section.19.6}%
\contentsline {section}{\numberline {19.7}Bibliographic Notes}{144}{section.19.7}%
\contentsline {chapter}{\numberline {20}Introduction to Internet of Things (Camera Sensors)}{145}{chapter.20}%
\contentsline {section}{\numberline {20.1}Lab Learning Outcomes}{145}{section.20.1}%
\contentsline {section}{\numberline {20.2}Introduction}{146}{section.20.2}%
\contentsline {subsection}{\numberline {20.2.1}Internet of Things Examples}{146}{subsection.20.2.1}%
\contentsline {subsection}{\numberline {20.2.2}Why Study This Stuff Anyway?}{147}{subsection.20.2.2}%
\contentsline {subsection}{\numberline {20.2.3}Conceptual Exercises for Section\nobreakspace {}\ref {sec:IoTintroduction:cam}}{147}{subsection.20.2.3}%
\contentsline {subsection}{\numberline {20.2.4}Hands-on Exercises for Section\nobreakspace {}\ref {sec:IoTintroduction:cam}}{148}{subsection.20.2.4}%
\contentsline {section}{\numberline {20.3}Thematic Takeaways}{157}{section.20.3}%
\contentsline {section}{\numberline {20.4}Chapter Summary}{157}{section.20.4}%
\contentsline {section}{\numberline {20.5}Key Terms}{158}{section.20.5}%
\contentsline {chapter}{\numberline {21}Setting up LtSense on the BeagleBone}{159}{chapter.21}%
\contentsline {section}{\numberline {21.1}Lab Learning Outcomes}{159}{section.21.1}%
\contentsline {section}{\numberline {21.2}Resources and Getting Help}{159}{section.21.2}%
\contentsline {section}{\numberline {21.3}Preliminaries and Stylistic Conventions}{162}{section.21.3}%
\contentsline {section}{\numberline {21.4}Materials}{163}{section.21.4}%
\contentsline {section}{\numberline {21.5}Linux Commands}{163}{section.21.5}%
\contentsline {subsection}{\numberline {21.5.1}Other Linux Concepts}{164}{subsection.21.5.1}%
\contentsline {section}{\numberline {21.6}Procedure}{164}{section.21.6}%
\contentsline {section}{\numberline {21.7}Thematic Takeaways}{176}{section.21.7}%
\contentsline {section}{\numberline {21.8}Lab Summary}{177}{section.21.8}%
\contentsline {section}{\numberline {21.9}Key Terms}{177}{section.21.9}%
\contentsline {chapter}{\numberline {22}Field Work Data}{178}{chapter.22}%
\contentsline {section}{\numberline {22.1}Lab Learning Outcomes}{178}{section.22.1}%
\contentsline {section}{\numberline {22.2}Resources and Getting Help}{178}{section.22.2}%
\contentsline {section}{\numberline {22.3}Introduction}{178}{section.22.3}%
\contentsline {section}{\numberline {22.4}Create a Study}{178}{section.22.4}%
\contentsline {section}{\numberline {22.5}Data Formats}{179}{section.22.5}%
\contentsline {section}{\numberline {22.6}Questions}{180}{section.22.6}%
\contentsline {section}{\numberline {22.7}Thematic Takeaways}{182}{section.22.7}%
\contentsline {section}{\numberline {22.8}Lab Summary}{183}{section.22.8}%
\contentsline {section}{\numberline {22.9}Key Terms}{183}{section.22.9}%
\contentsline {chapter}{\numberline {23}BigSense}{184}{chapter.23}%
\contentsline {section}{\numberline {23.1}Lab Learning Outcomes}{184}{section.23.1}%
\contentsline {section}{\numberline {23.2}Introduction}{184}{section.23.2}%
\contentsline {section}{\numberline {23.3}Materials}{185}{section.23.3}%
\contentsline {section}{\numberline {23.4}Procedure}{185}{section.23.4}%
\contentsline {section}{\numberline {23.5}Summary}{191}{section.23.5}%
\contentsline {section}{\numberline {23.6}Thematic Takeaways}{192}{section.23.6}%
\contentsline {section}{\numberline {23.7}Lab Summary}{192}{section.23.7}%
\contentsline {section}{\numberline {23.8}Key Terms}{192}{section.23.8}%
\contentsline {section}{\numberline {23.9}Notes from the Authors}{192}{section.23.9}%
\contentsline {chapter}{\numberline {24}Critical Sections, Race Conditions, \& Deadlock}{195}{chapter.24}%
\contentsline {section}{\numberline {24.1}Lab Learning Outcomes}{195}{section.24.1}%
\contentsline {section}{\numberline {24.2}Resources and Getting Help}{195}{section.24.2}%
\contentsline {section}{\numberline {24.3}Activities}{195}{section.24.3}%
\contentsline {section}{\numberline {24.4}Thematic Takeaways}{199}{section.24.4}%
\contentsline {section}{\numberline {24.5}Key Terms}{199}{section.24.5}%
\contentsline {chapter}{\numberline {25}An Introduction to Go, Goroutines, and Channels}{200}{chapter.25}%
\contentsline {section}{\numberline {25.1}Lab Learning Outcomes}{200}{section.25.1}%
\contentsline {section}{\numberline {25.2}Resources and Getting Help}{200}{section.25.2}%
\contentsline {section}{\numberline {25.3}Activities}{200}{section.25.3}%
\contentsline {section}{\numberline {25.4}Thematic Takeaways}{201}{section.25.4}%
\contentsline {section}{\numberline {25.5}Key Terms}{201}{section.25.5}%
\contentsline {section}{\numberline {25.6}Bibliographic Notes}{201}{section.25.6}%
\contentsline {chapter}{\numberline {26}Learning Channels and Communicating Sequential Processes (in Go)}{202}{chapter.26}%
\contentsline {section}{\numberline {26.1}Lab Learning Outcomes}{202}{section.26.1}%
\contentsline {section}{\numberline {26.2}Resources and Getting Help}{202}{section.26.2}%
\contentsline {section}{\numberline {26.3}Activities}{202}{section.26.3}%
\contentsline {section}{\numberline {26.4}Thematic Takeaways}{203}{section.26.4}%
\contentsline {section}{\numberline {26.5}Bibliographic Notes}{203}{section.26.5}%
\contentsline {chapter}{\numberline {27}More Channels and Communicating Sequential Processes}{204}{chapter.27}%
\contentsline {section}{\numberline {27.1}Lab Learning Outcomes}{204}{section.27.1}%
\contentsline {section}{\numberline {27.2}Resources and Getting Help}{204}{section.27.2}%
\contentsline {section}{\numberline {27.3}Activities}{204}{section.27.3}%
\contentsline {section}{\numberline {27.4}Thematic Takeaways}{205}{section.27.4}%
\contentsline {section}{\numberline {27.5}Key Terms}{205}{section.27.5}%
\contentsline {section}{\numberline {27.6}Bibliographic Notes}{205}{section.27.6}%
\contentsline {chapter}{\numberline {28}Exploring the Actor Model of Concurrency in Elixir}{206}{chapter.28}%
\contentsline {section}{\numberline {28.1}Lab Learning Outcomes}{206}{section.28.1}%
\contentsline {section}{\numberline {28.2}Resources and Getting Help}{206}{section.28.2}%
\contentsline {section}{\numberline {28.3}Activities}{207}{section.28.3}%
\contentsline {section}{\numberline {28.4}Thematic Takeaways}{207}{section.28.4}%
\contentsline {section}{\numberline {28.5}Bibliographic Notes}{207}{section.28.5}%
\contentsline {chapter}{\numberline {29}Time-of-check to Time-of-use Race Conditions}{208}{chapter.29}%
\contentsline {section}{\numberline {29.1}Lab Learning Outcomes}{208}{section.29.1}%
\contentsline {section}{\numberline {29.2}Preparation}{208}{section.29.2}%
\contentsline {section}{\numberline {29.3}Lab Tasks}{211}{section.29.3}%
\contentsline {subsection}{\numberline {29.3.1}Task 1: Understanding the Application}{211}{subsection.29.3.1}%
\contentsline {subsection}{\numberline {29.3.2}Task 2: Exploiting Time-of-check to time-of-use (TOCTOU) vulnerability}{212}{subsection.29.3.2}%
\contentsline {section}{\numberline {29.4}Thematic Takeaways}{215}{section.29.4}%
\contentsline {section}{\numberline {29.5}Lab Summary}{215}{section.29.5}%
\contentsline {section}{\numberline {29.6}Key Terms}{215}{section.29.6}%
\contentsline {section}{\numberline {29.7}Bibliographic Notes}{215}{section.29.7}%
\contentsline {chapter}{\numberline {30}Cloud Computing and Big Data Processing (Part I)}{218}{chapter.30}%
\contentsline {section}{\numberline {30.1}Lab Learning Outcomes}{218}{section.30.1}%
\contentsline {section}{\numberline {30.2}Resources and Getting Help}{218}{section.30.2}%
\contentsline {section}{\numberline {30.3}Initial Setup and Preparation}{218}{section.30.3}%
\contentsline {section}{\numberline {30.4}Create A Cluster of Virtual Machines}{219}{section.30.4}%
\contentsline {subsection}{\numberline {30.4.1}Connecting to Virtual Machines from Local Computers}{226}{subsection.30.4.1}%
\contentsline {subsection}{\numberline {30.4.2}Create Group/User and Set Up Password Authentication}{230}{subsection.30.4.2}%
\contentsline {section}{\numberline {30.5}Thematic Takeaways}{237}{section.30.5}%
\contentsline {section}{\numberline {30.6}Lab Summary}{237}{section.30.6}%
\contentsline {section}{\numberline {30.7}Key Terms}{237}{section.30.7}%
\contentsline {chapter}{\numberline {31}Cloud Computing and Big Data Processing (Part II)}{239}{chapter.31}%
\contentsline {section}{\numberline {31.1}Lab Learning Outcomes}{239}{section.31.1}%
\contentsline {section}{\numberline {31.2}Resources and Getting Help}{239}{section.31.2}%
\contentsline {subsection}{\numberline {31.2.1}Build Passwordless Connections for Nodes in the Clusster}{239}{subsection.31.2.1}%
\contentsline {subsection}{\numberline {31.2.2}Install Java and Hadoop Software on Each VM}{243}{subsection.31.2.2}%
\contentsline {subsection}{\numberline {31.2.3}Configure the Hadoop Cluster and Run Existing Programs}{246}{subsection.31.2.3}%
\contentsline {subsection}{\numberline {31.2.4}Start Hadoop Cluster and Run Existing Programs}{254}{subsection.31.2.4}%
\contentsline {section}{\numberline {31.3}Thematic Takeaways}{256}{section.31.3}%
\contentsline {section}{\numberline {31.4}Lab Summary}{257}{section.31.4}%
\contentsline {section}{\numberline {31.5}Key Term}{257}{section.31.5}%
\contentsline {chapter}{\numberline {32}Cloud Computing and Big Data Processing (Part III)}{258}{chapter.32}%
\contentsline {section}{\numberline {32.1}Lab Learning Outcomes}{258}{section.32.1}%
\contentsline {section}{\numberline {32.2}Resources and Getting Help}{258}{section.32.2}%
\contentsline {section}{\numberline {32.3}Initial Setup and Preparation}{258}{section.32.3}%
\contentsline {subsection}{\numberline {32.3.1}Create a Mapreduce Project using Maven in Eclipse}{259}{subsection.32.3.1}%
\contentsline {subsection}{\numberline {32.3.2}Run the \texttt {.jar} Package at amazon clouds}{269}{subsection.32.3.2}%
\contentsline {section}{\numberline {32.4}Thematic Takeaways}{275}{section.32.4}%
\contentsline {section}{\numberline {32.5}Lab Summary}{276}{section.32.5}%
\contentsline {section}{\numberline {32.6}Key Term}{276}{section.32.6}%
