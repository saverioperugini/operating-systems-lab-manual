Use dash (-) or underscore (_) in function names, but do not inter-change.

No section should have only one subsection (even if for just exercises).

Use fprintf(stderr, ...) for all tracing/print statements.

Use "parameter" for formal parameter in reference to a function
signature/prototype.

Use "argument" for a function invocation.

Use consist wording in exercises asking for a function definition.

Use \texttt{funname} in text.

Use three spaces consistently for code block indentation.

Always include line numbers in lstlisting code listings.
