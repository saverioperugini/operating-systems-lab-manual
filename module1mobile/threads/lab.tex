%\renewcommand{\chaptername}{Lab}
%\renewcommand{\thechapter}{5}
\chapter{Exploring Time-shared OSs and Threads}
\label{CHAP:THREADS}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Author: Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

\begin{itemize}
\item Establish an understanding of \textit{time-shared} operating systems.
\item Establish an understanding of the use of \texttt{htop} to explore 
the current threads.
\item Establish an understanding of shell job control.
\item Establish an understanding of forwarding X11 applications.
\end{itemize}

\section{Resources and Getting Help} 

\begin{itemize}
   \item Threads \& Thread-safe Functions Notes:
      \url{https://perugini.cps.udayton.edu/teaching/courses/cps346/lecture_notes/threads.html}

   \item (Shell) Job Control Notes:
      \url{https://perugini.cps.udayton.edu/teaching/courses/cps346/lecture_notes/jobcontrol.html}

    \item Linux Manpages (e.g., \texttt{\$ man htop})
\end{itemize}

%%%%%% END PREFIX %%%%%%%

%%%%%% BEGIN MAIN CONTENT OF LAB %%%%%%%

\section{Steps}

\begin{enumerate}
\item Start \texttt{xming} on your laptop.

\item \texttt{ssh} into your Raspberry PI with the \texttt{-X} option on to
\texttt{ssh} in Putty (under the SSH X11 menu).

\begin{lstlisting}[language=bash,numbers=none]
$ ssh -X piusername@ipaddressofpi
\end{lstlisting}

\item Copy some files from the share repository to your Raspberry Pi account:

\begin{lstlisting}[language=bash,numbers=none]
$ scp operating-systems-lab-manual-shared-files/module1mobile/threads/counters.tar \
   piusername@ipaddressofpi:
\end{lstlisting}

\item Untar the files on your Raspberry Pi:

\begin{lstlisting}[language=bash,numbers=none]
$ tar xvf counters.tar
$ cd Counter0
\end{lstlisting}

\item Compile the \texttt{Counter0} program:

\begin{lstlisting}[language=bash,numbers=none]
$ javac *.java
\end{lstlisting}

\item Run the \texttt{Counter0} program:

\begin{lstlisting}[language=bash,numbers=none]
$ java CounterGo0 &
\end{lstlisting}

\item Create a new counter.  Can you create a second counter?
Can you exit the program via the \textsc{gui}?  Why not?  Explain
why using operating systems nomenclature.  Is the \texttt{Counter0}
program single-threaded or multi-threaded? Explain with reasons.

\item Run the \texttt{htop} command to investigate the running Java
processes and threads in the system.

\begin{lstlisting}[language=bash,numbers=none]
$ htop
\end{lstlisting}

Hit the {F-5} key and drill into the process tree on the right hand side to
locate the Java process.

\item Since you cannot exit the process normally, find the pid 
of the Java process using \texttt{ps} and then kill the process
with the \texttt{kill} command: e.g.,
\texttt{kill} $\mathtt{<}$\texttt{\textit{pid}}$\mathtt{>}$

\begin{lstlisting}[language=bash,numbers=none]
$ kill <pid>
\end{lstlisting}

\item Now compile and run the \texttt{Counter1} program using \texttt{javac}
and \texttt{java}, respectively, as above.  Create several counters.  What do
you notice?  Are they all running at the same time?  Do they appear to be all
running at the same time?  Is the Linux Raspbian \textsc{os} time-shared?  How would the
counters react if they were run on a non-time-shared system?

\item Run the \texttt{htop} command to investigate the running Java
process and threads in the system.  What happens in
\texttt{htop} every time you spawn the new counter thread?

\begin{lstlisting}[language=bash,numbers=none]
$ htop
\end{lstlisting}

Hit the {F-5} key and drill into the process tree on the right hand side to
locate the Java process.

\item How many counters can you spawn before the \textsc{cpu} grinds to a halt?
Try to bring the system down.

\item Run Firefox:

\begin{lstlisting}[language=bash]
$ firefox &
\end{lstlisting}

\item Run \texttt{htop}:

\begin{lstlisting}[language=bash,numbers=none]
$ htop
\end{lstlisting}

Hit the {F-5} key and drill into the process tree on the right hand side to
locate Firefox.

\item Run the \texttt{htop} command to investigate the running Firefox
process and threads in the system.  What happens in \texttt{htop}
every time you open a new tab in Firefox?

See \url{http://ask.xmodulo.com/view-threads-process-linux.html}
for viewing threads in \texttt{htop}

\item Exploring job control with \texttt{xclock} and \texttt{xeyes}.
Install \texttt{x11-apps}:

\begin{lstlisting}[language=bash,numbers=none]
$ sudo apt-get install x11-apps
\end{lstlisting}

\item Read
\url{https://perugini.cps.udayton.edu/teaching/courses/cps346/lecture_notes/jobcontrol.html}

\item Adding a \texttt{\&} to the end of a command line runs the process in the
`background' (i.e., detached from the shell; the shell is no longer
`\texttt{wait}ing' for it)

Without the \texttt{\&} the process is run in the `foreground.'

$\mathtt{<}$\texttt{\textit{ctrl-c}}$\mathtt{>}$
will kill a foreground process.

The combination of $\mathtt{<}$\texttt{\textit{ctrl-z}}$\mathtt{>}$ (suspend)
and \texttt{bg} will put a `foreground' process in the `background.'

Run \texttt{xclock} and \texttt{xeyes} and display the output on your laptop.

\item Experiment running \texttt{xclock} and \texttt{xeyes} in the foreground
and background.

\begin{lstlisting}[language=bash,numbers=none]
$ xeyes
crtl-z
bg
$ xclock -update 1 
crtl-z
bg
$ ps
...
...
$ kill <insert process id of xeyes here> \
       <give process id of xclock here>
$ xeyes &
$ xclock -update 1 &
\end{lstlisting}

\item Explore the figure at \url{https://perugini.cps.udayton.edu/teaching/courses/cps346/lecture_notes/images/jobs.png}.

Move \texttt{xclock}, \texttt{xeyes}, and \texttt{firefox} process around this
triangle throughout the various queues (foreground, background,
suspended/blocked)

\end{enumerate}
%%%%%% END MAIN CONTENT OF LAB%%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

\section{Thematic Takeaways}

\begin{itemize}

\item The shell supports commands that move jobs from one state
to another (e.g., running in the foreground to stopped in the background).

\item The multi-threaded \textsc{gui} Java counter application vividly
demonstrates time sharing as we are unable to distinguish which threaded is
currently running---all counters appear to be running at once!
\end{itemize}

%\section{Lab Summary}

\section{Key Terms}

%\section{Bibliographic Notes}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
