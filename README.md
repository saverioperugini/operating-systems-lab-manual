# Compiling the Laboratory Manual

## Compiling the Entire Composite Manual

The laboratory manual is written in LaTeX and BibTeX and can be compiled 
with the standard packages that ship with a LaTeX installation (e.g., the 
The [MacTeX-2021 Distribution](https://www.tug.org/mactex/) available at 
[https://www.tug.org/mactex/](https://www.tug.org/mactex/)).

There is a `Makefile` in the repository for building the entire manual.  A
manual can be built using the following commands in sequence: `make clean;
make`.

```
$ make clean
$ make
...
...
...
$ open OSlabManual.pdf
```

## Compiling Individual Labs Separately

Each lab in the manual can also be compiled/built individually.  Simply
navigate to the directory of the lab you desire to build and enter `pdflatex
main`.  This will produce the file `main.pdf`, which will contain only the
contains of the particular lab.  For instance:

```
$ pwd
operating-systems-lab-manual
$ cd module1mobile/IoTcamera/
$ pwd
operating-systems-lab-manual/module1mobile/IoTcamera
$ pdflatex main
...
...
...
$ open main.pdf
```

The last three labs of Module I, which constitute the *BigSenseWorkbook* (version
2), involve the use of moisture sensors on a BeagleBone.  Compiling these labs
requires the installation of some extra LaTeX packages. In particular, these
labs require the LaTeX `minted` package and the Python syntax highlighter
[Pygments](https://pygments.org/docs/cmdline/) available at
[https://pygments.org/docs/cmdline/](https://pygments.org/docs/cmdline/) as
well as the `pygmentize` script.  Compiling these labs also requires passing
the `-shell-escape` flag to `pdflatex`.

```
$ pwd 
operating-systems-lab-manual
$ cd module1mobile/BigSenseWorkbookV2
$ ls 
lab1/ lab2/ lab3/
$ cd lab1
$ pdflatex -shell-escape main
...
...
...
```

# Adding New Labs to the Manual

Users can add new labs (and/or modules) to the laboratory manual.
There is a template for creating a new lab in the directory `ADDING_NEW_LABS`.
The process of getting started adding a new lab involves the following
series of commands:

```
$ pwd
operating-systems-lab-manual
$ cp -r ADDING_NEW_LABS/lab_template new_lab_name
$ cd new_lab_name
$ ls
GUIDE      lab.tex    main.idx   main.out   pristine/  solutions/
README     main.aux   main.log   main.tex   rubric/    src/
$ vi lab.tex #  to develop the contents of the new lab
...
...
...
```

# Statement of Need

A course in operating systems (OS) plays a central role in the curriculum of
undergraduate degree programs in computer science.  Unfortunately, many OS
courses are out-of-date and in need of time-consuming revision since
innovations in course content and related pedagogy are typically focused,
understandably enough, on the introductory computer science sequence.  The OS
laboratory manual discussed here resolves issues of misalignment
found between existing OS courses and employee professional skills and
knowledge requirements.  Instructors can make use of the labs in the manual as
they see fit in their teaching activities.  The plug-and-play nature
of the labs within each module allows instructors the ability to craft a
tailor-made operating systems course.  The manual offers significant value
especially since developing lab and project plans is quite time
consuming in computer science education.  Thus, this laboratory manual fills a
much-needed void in the operating systems education community.
