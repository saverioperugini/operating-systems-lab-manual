%\chapter*{Preliminaries: Getting What You Need to Get Started}
\chapter*{How to Use this Laboratory Manual}

\section*{Introduction}

% see https://www.rust-class.org/0/pages/course-wrapup.html
\subsection{Approaches to Teaching Operating Systems}

\begin{enumerate}

\item Building a simple operating system (e.g., a kernel).
\item Modifying an existing operating system kernel (e.g., Linux).
\item Studying concepts of operating systems while programming with the system.
\end{enumerate}

Broadly, these three approaches can be categoried as:

\begin{enumerate}[I)]
\item Programming \textit{the} system (approaches 1 and 2)
\item Programming \textit{with} the system (approach 3)
\end{enumerate}

This laboratory manual is designed to be used to teach an undergraduate course
in operating systems from a hand-on, active-learning perspective.


\section*{How to Use This Lab Manual in Your OS Course}

This laboratory manual contains five progressive modules (system and
environment setup followed by):

\begin{itemize}
\item Setup Module
\item Module 0: Fundamentals (Linux and C)
\item Module I: Mobile OSs and Internet of Things
\item Module II: Concurrent Programming and Synchronization
\item Module III: Cloud computing and Big Data Processing
\end{itemize}

\noindent While the modules in which the labs in this manual are classified
should be pursued in order, not all labs in each module need be necessarily
completed in the order prescribed on the cover page. Some of the labs in a
module loosely build upon each other.  On the other hand, certain labs more
strongly build upon each other.  For instance, the labs in Module III build
upon each other and must be pursued in order.

There is also a thematic focus on security running throughout the modules of
the manual: see Labs 18, 19, and 29.

\subsubsection*{Plug-and-Play Active-learning Laboratories}

The following figure depicts the dependencies between the labs of this
manual.

\medskip

%\begin{center}
\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{figs/dependencies.eps}}
%\end{center}

\medskip

\noindent Thus, multiple paths through this manual can be taken.

\section*{Learning Environment: System Support for the Labs}

The labs in this manual require a variety of equipment and systems software
support.  The labs in the manual, except where noted below, can be completed
using a virtual machine we make available for download or on a Raspberry Pi
computer (running Raspbian 4.14.79).  Either approach use the same set of files
available in a Git repository (see below).

\paragraph{Using the Virtual Machine.} \noindent Setup instructions for the
Virtual Machine are in ``Lab \nameref{CHAP:VMSETUP}'' on page
\pageref{CHAP:VMSETUP} and also available at
\url{http://perugini.cps.udayton.edu/teaching/books/SPUC/www/RaspberryPi/index.html}.

\paragraph{Using the Raspberry Pi.} \noindent Setup instructions for the
Raspberry Pi are in ``Lab \nameref{CHAP:RPISETUP}'' on page
\pageref{CHAP:RPISETUP} and also available at
\url{http://perugini.cps.udayton.edu/teaching/books/SPUC/www/RaspberryPi/index.html}.

\subsection*{Access to the Files Necessary to Work on the Labs}

The (source code) files referenced and mentioned in the labs contained in this
laboratory manual can be downloaded from our ``Operating Systems Lab Manual
Shared Files'' Git repository available in BitBucket at\\

\medskip

\noindent
\url{https://bitbucket.org/sperugin/operating-systems-lab-manual-shared-files/},

\medskip

\noindent
or by cloning the repository:

\medskip

\noindent
\texttt{git} \texttt{clone}
\url{https://sperugin@bitbucket.org/sperugin/operating-systems-lab-manual-shared-files.git}

\medskip

\noindent
or

\medskip

\noindent
\texttt{git} \texttt{clone}
\url{git@bitbucket.org:sperugin/operating-systems-lab-manual-shared-files.git}.

\subsection*{Labs Requiring Special Equipment or Software}

\paragraph{Computer Security Labs 18--19 in Module I and Security Lab 29 in
Module II}  require the \textsc{seed} virtual machine (\textsc{vm}) with Ubuntu
16.04 available at \url{http://bit.ly/SEED-VM}.

\paragraph{The Internet-of-Things Lab in Module I: \nameref{CHAP:IOTCAMERA}}  
requires a Raspberry Pi and a camera.

\paragraph{The Internet-of-Things Labs 21--23 in Module I}  
require a BeagleBoard.

\paragraph{Module III: Cloud Computing and Big Data Processing.} The labs in Module III 
require an Amazon Web Services account (\url{https://aws.amazon.com/}).

\subsection*{Systems Software Availability}

\begin{center}
\begin{tabular}{rll}
Amazon Web Services & \url{https://aws.amazon.com} & Module III: Labs 30--32\\
SEED VM with Ubuntu 16.04 & \url{http://bit.ly/SEED-VM} & Lab 18--19 and Lab 29\\
Ubuntu & \url{https://ubuntu.com} & \\
VirtualBox & \url{https://www.virtualbox.org} &\\
\end{tabular}
\end{center}

\subsection*{Programming Language Availability}

\begin{center}
\begin{tabular}{rll}
C & \url{http://www.open-std.org/jtc1/sc22/wg14/} &\\
%C++ & \url{https://isocpp.org} &\\
Elixir & \url{https://elixir-lang.org} & Lab 28\\ %Lab 28: \nameref{CHAP:ACTOR} \\
Go & \url{https://golang.org} & Labs 25--27 \\
Java & \url{http://java.com} & Lab 24\\ % Lab 24: \nameref{CHAP:RACECONDITIONS}\\
%Julia & \url{https://julialang.org}\\
%Lua & \url{https://lua.org}\\
%Python & \url{https://python.org}\\
\end{tabular}
\end{center}

%C++
\noindent C and Java are available in our virtual machine and/or on the
Raspberry Pi and, thus, need not be installed independently.

\subsection*{Equipment Availability}

\begin{center}
\begin{tabular}{rll}
Raspberry Pi & \url{https://www.raspberrypi.org} & Lab 20 \\
BeagleBoard & \url{https://beagleboard.org} & Labs 21--23\\
\end{tabular}
\end{center}

\subsection*{Mapping Matrix to \textit{Operating Systems Concepts} Textbook~\cite{OSC10}}

The following is an approximate mapping from the chapters and topics in the widely used
\textit{Operating Systems Concepts} Textbook~\cite{OSC10} to the labs in this manual:

\begin{center}
\begin{tabular}{lll}
\hline
\textbf{\textit{Operating Systems Concepts} Textbook~\cite{OSC10}} & $\rightsquigarrow$ & 
\textbf{Lab Manual}\\
\hline
\cite{UPE} and \cite{CPL} & $\rightsquigarrow$ & Module 0 (Labs 4--10)\\
\cite{OSC10} Parts I and II (Chapters 1--5) & $\rightsquigarrow$ & Module I (Labs 11--23) \\
\cite{OSC10} Part III (Chapters 6--8) & $\rightsquigarrow$ & Module II (Labs 24--29) \\
\hline
\end{tabular}
\end{center}

\noindent The topics of Chapters 9 and 10---memory management and virtual
memory, respectively---of Part II of~\cite{OSC10} are not well represented in
this lab manual.  Those topics are covered somewhat in Module I.  Instead of
including labs on memory management and virtual memory, we decided to include
a set of progressive labs on cloud computing and big data processing---see
Module III (Labs 30--32).

\subsection*{Additional Notes about Internet-of-Things Labs 21--23}

Labs 21--23 were developed by Dr. Andrew Rettig and Sumit Khanna and constitute
what is called the \textit{BigSense Workbook} (version 2). Dr. Rettig provides
additional assistance for these labs to instructors on both GitHub and on a
Slack support channel.  Instructors using these labs are encouraged to e-mail
Dr. Rettig at \url{andrew.rettig@lildata.monster} or
\url{arettig1@udayton.edu} with a link to your current university position for
the instructor invites.

\subsection*{For More Information}

For more information on how this Laboratory Manual fits into our NSF-funded
IUSE Research Project on Teaching Undergraduate Operating Systems, we refer the reader to the
following abstracts.

%\cite{NSFIUSECCSCMW2018tutorial}

%\bibitem[PW18]{NSFIUSECCSCMW2018tutorial}
S.~Perugini and D.J. Wright.
\newblock Developing a contemporary operating systems course.
\newblock {\em Journal of Computing Sciences in Colleges}, \textbf{34}(1):155--158,
  2018.
\newblock Conference Tutorial [Extended Abstract].

\medskip

%\cite{Perugini:2019:DCI:3287324.3293734}

\noindent
%\bibitem[PW19]{Perugini:2019:DCI:3287324.3293734}
S.~Perugini and D.J. Wright.
\newblock Developing a contemporary and innovative operating systems course.
\newblock In {\em Proceedings of the 50$^{th}$ {ACM} Technical Symposium on
  Computer Science Education ({SIGCSE})}, page 1248, New York, NY, 2019. {ACM}
  Press.
\newblock Conference Birds-of-a-Feather; DOI:
  \url{http://doi.acm.org/10.1145/3287324.3293734}.

%\newpage
%This manual is intended to help instructors prepare their labs. If it
%falls into students' hands, it will affect the intended educational objectives
%of the labs, harming our many years of effort. We really appreciate your
%cooperating in keeping this instructor manual to yourself. Please never post
%this manual online; please do not give it to students. If other instructors ask
%you for a copy, please ask them to send a request to me via e-mail at
%\texttt{saverio@udayton.edu}.

%\medskip

%\noindent
%Many thanks.

%\medskip

%\noindent
%Saverio Perugini\\
%\today

\newpage
\chapter*{Contributors}

This Lab Manual is a result of a collaborative effort of multiple
faculty members and students.

\section*{University of Dayton}

\subsection*{Faculty}

\textit{(all in the Department of Computer Science except where noted)}

\begin{itemize}
\item Rusty O. Baldwin 
\item Saverio Perugini
\item Phu H. Phung
\item Andrew Rettig\footnote{Department of Electrical and Computer Engineering and Department of Geology and Environmental Geosciences}
\item Zhongmei Yao
\end{itemize}

\subsection*{Students} 

\textit{(all in the Department of Computer Science)}

\begin{itemize}
\item Kevin Brown
\item Daniel J. Illg
\item Patrick Marsee
\item Matthew Weiler
\end{itemize}

\section*{Other Contributors}

\begin{itemize}
\item Adam Bryant (formerly, Department of Computer Science and Engineering,
Wright State University)
\item Sumit Khanna
\item Paul Talaga (University of Indianapolis; external advisor)
\end{itemize}

\section*{NSF-Funded Project}

This laboratory manual is part of a a three-year NSF-funded IUSE (Improving
Undergraduate STEM Education) project titled ``Engaged Student Learning:
Re-conceptualizing and Evaluating a Core Computer Science Course for Active
Learning and STEM Student Success'' (2017--20) whose goal is to foster and
facilitate innovation, in both content and delivery, in teaching \textsc{os}
through the development of a contemporary model for an \textsc{os} course that
aims to resolve significant issues of misalignment between existing \textsc{os}
courses and employee professional skills and knowledge
requirements\cite{NSFIUSECCSCMW2018tutorial}.

While an \textsc{os} course is a nexus in a \textsc{cs} program (connecting the
introductory programming sequence to upper-level electives), the typical
pedagogical approach to it has become dated and stale---most of the recent
focus has been on improving the introductory programming sequence primarily for
retention---because it has not been responsive to the transformed landscape of
modern computing platforms (e.g., from desktop to mobile; from single- to
multi-core architectures), the job market, and the concomitant progress in
active, student-centric learning (e.g., from a traditional, content-centric,
purely lecture-based course to an active, learner-centric, hybrid
lecture/lab-based format). Addressing this issue is the rationale for our
NSF-funded project.  Thus, a broader objective of this laboratory manual is to promote
and facilitate use of our re-conceptualized course model for \textsc{os}
content and pedagogy.  Specifically, we aim to help faculty at other
institutions adopt this model in a systematic and simplified way.

The course model involves three progressive modules: 1) mobile OSs and Internet
of Things, 2) concurrent programming and synchronization, and 2) cloud
computing and big data processing. 

\section*{Community of Practice}

We are also establishing an \textsc{os} teaching community of practice to share
both materials and experiences in the teaching of \textsc{os}.  To support the
community in sharing expertise and perspective, we have developed an
open-access, Git repository of items related to teaching \textsc{os}. The items
collected in the repository are shared and accessed through our GitHub Pages
portal site:
\url{https://saverioperugini.github.io/Teaching-Operating-Systems-Community-of-Practice/}).
The community includes members who attended our 2018 CCSC:MW
tutorial~\cite{NSFIUSECCSCMW2018tutorial} and 2019 SIGCSE
birds-of-a-feather session~\cite{Perugini:2019:DCI:3287324.3293734}.
Another goal of this lab manual is to expand the participation of
faculty in the community of practice.  Members of this community serve as
external advisors and are invited to provide an external perspective on both the module
content and the laboratory plans.

\section*{Advisory Group}

We are also establishing an \textit{advisory group} of computer science faculty
members for this project for an external perspective on the model and its
adoption.  Another goal of this lab manual is to expand the participation of
regional faculty in the advisory group.  Members of this group serve as
advisors and are asked to provide an external perspective on both the module
content and the laboratory plans.

We hope that faculty teaching \textsc{os} find the ability to plug-and-play
with the active-learning, laboratory plans in this manual attractive and see
significant value in making use of them in their courses and teaching
activities, especially since developing real-world lab and project plans
requires substantial effort and time.
