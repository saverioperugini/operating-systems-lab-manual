%\renewcommand{\chaptername}{Lab}
%\renewcommand{\thechapter}{1}
\chapter{Learning Pointers}
\label{CHAP:POINTERS2}

%\pagenumbering{arabic}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Author: Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

\begin{itemize}
\item Establish an understanding of pointers (i.e., addresses) in C.
\item Establish an understanding of dynamic memory allocation in C.
\item Establish an understanding of string (i.e., an array of \texttt{char}acters)
manipulation in C.
\end{itemize}

\section{Resources and Getting Help} 

\begin{itemize}
    \item System Libraries and I/O Notes:
 \url{https://perugini.cps.udayton.edu/teaching/books/SPUC/www/lecture_notes/librariesIO.html}

    \item Linux Manpages (e.g., \texttt{\$ man scanf})
\end{itemize}

%%%%%% END PREFIX %%%%%%%

%%%%%% BEGIN MAIN CONTENT OF LAB %%%%%%%

\section{Steps}

\begin{enumerate}

\item
Assume the following declarations:

\noindent
\begin{lstlisting}[language=C,numbers=none]
int a = 5;
double b = 10.0;
char c = 'c';
\end{lstlisting}

\noindent
Write C code to declare pointer variables \texttt{aPtr}, \texttt{bPtr},
and \texttt{cPtr}, and make them point to \texttt{a}, \texttt{b},
and \texttt{c}, respectively.

\noindent
Suppose afterward, the following code excutes:

\noindent
\begin{lstlisting}[language=C,numbers=none]
a += 10;
b += 10;
c += 10;
\end{lstlisting}

\noindent
Complete the following table with the resulting values. Assume a 32-bit
architecture with \texttt{sizeof(int) == 4}, \texttt{sizeof(double) == 8},
and \texttt{sizeof(char) == 1}.  Assume all variables are (automatic) local variables
allocated contigously on the stack:

\begin{center}
\resizebox{\textwidth}{!}{
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
\textbf{Symbol} & \multicolumn{1}{c|}{\texttt{a}} &
\multicolumn{1}{c|}{\texttt{\&b}} & \multicolumn{1}{c|}{\texttt{c}} &
\multicolumn{1}{c|}{\texttt{aPtr}} & \multicolumn{1}{c|}{\texttt{*bPtr}} &
\multicolumn{1}{c|}{\texttt{cPtr}}\\
\hline
\hline
\textbf{Value} & & & & & & \\
\hline
\textbf{Address} & \texttt{0x402bfba4} & \texttt{0x402bfb70} &
\texttt{0x402bfb8b} & \texttt{0x402bfb7c} & \texttt{0x402bfb94} &
\texttt{0x402bfb64}\\
\hline
\textbf{Data type} & & & & & & \\
\hline
\textbf{Size (in bits)} & & & & & & \\
\hline
\end{tabular}}
\end{center}

\noindent You can write a C program to figure out the answers.
%Demo your code to instructor or TA before continuing to Step 2.

\item
\cite[Self-Review Exercise 10.3, p.~424]{DeitelC}
%(Also available as
%Pointer Exercise 1 at:
%\url{https://perugini.cps.udayton.edu/teaching/courses/cps346/lecture_notes/ptrEx.html}.)

Write a single statement or set of statements to accomplish each of the
following:

\begin{enumerate}[a)]

\item~Define a structure called \texttt{route} containing an \texttt{float}
variable \texttt{routeNumber}, and \texttt{char} array \texttt{routeName} whose
values may be as long as 25 characters.

\item~Define \texttt{Route} to be a synonym for the type \texttt{struct route}.

\item~Use \texttt{Route} to declare variable \texttt{a} to be of type
\texttt{struct route}, \texttt{array b[10]} to be of type \texttt{struct route},
and variable \texttt{ptr} to be of type pointer to \texttt{struct route}.

\item~Read a route number and a route name from the keyboard into the individual
members of variable \texttt{a}.

\item~Assign the member values of variable \texttt{a} to element 3 of array
\texttt{b}.

\item~Assign the address of array \texttt{b} to the pointer variable
\texttt{ptr}.

\item~Print the members values of element 3 of array \texttt{b} to the display
using the variable \texttt{ptr} and the structure pointer operator to refer to
the members.

\end{enumerate}

\item
\cite[Self-Review Exercise~7.4,~p.~300]{DeitelC}

%(Also available as
%Pointer Exercise 2 at:
%\url{https://perugini.cps.udayton.edu/teaching/courses/cps346/lecture_notes/ptrEx.html}.)

\noindent
Assume the following variables
have been declared as shown.

\vspace{0.5cm}

\begin{verbatim}
double number1 = 7.3, number2;
char* ptr = NULL;
char s1[100], s2[100];
\end{verbatim}

\vspace{0.5cm}

\begin{enumerate}[a)]

\item~Declare the variable \texttt{dPtr} to be a pointer to a variable
of type \texttt{double}.
\item~Assign the address of variable \texttt{number1} to pointer
variable \texttt{dPtr}.
\item~Print the value of the variable pointed to by \texttt{dPtr} to the 
display.
\item~Assign the value of the variable pointed to by \texttt{dPtr} to
variable \texttt{number2}.
\item~Print the value of \texttt{number2} to the display.
\item~Print the address of \texttt{number1} to the display.
\item~Print the address stored in \texttt{dPtr} to the display.
Is the value printed
the equal to the address of \texttt{number1}?
\item~Copy the string stored in character array \texttt{s1} into character
array \texttt{s2}.
\item~Compare the string stored in character array \texttt{s1} with
the string in character array \texttt{s2}, and print the result to
the display.
\item~Append the string in character array \texttt{s2} to the string
in character array \texttt{s1}. Will this cause a run-time error?
\item~Determine the length of the string stored in character array \texttt{s1},
and print the result to the display.

\end{enumerate}

\item Define the functions \texttt{swap\_cbv} and \texttt{swap\_cbr} in the following C program:

\begin{lstlisting}[language=C,numbers=none]
#include<stdio.h>

/* swap call-by-value */
void swap_cbv (int x, int y) {
 ...
 ...
 ...
}

/* swap (call-by-reference) */
void swap_cbr (int* x, int* y) {
 ...
 ...
 ...
}

main() {

 int a = 1;
 int b = 2;

 printf ("before call-by-value: a(%x) = %d, b(%x) = %d\n\n",
    &a, a, &b, b);
 swap_cbv (a, b);
 printf ("after call-by-value: a(%x) = %d, b(%x) = %d\n\n",
    &a, a, &b, b);

 printf ("before call-by-reference: a(%x) = %d, b(%x) = %d\n\n",
    &a, a, &b, b);
 swap_cbr (&a, &b);
 printf ("after call-by-reference: a(%x) = %d, b(%x) = %d\n\n", 
    &a, a, &b, b);
}
\end{lstlisting}

Output:

\begin{lstlisting}[language=bash,numbers=none]
$ ./a.out
before call-by-value: a(4fabf8ac) = 1, b(4fabf8a8) = 2

after call-by-value: a(4fabf8ac) = 1, b(4fabf8a8) = 2

before call-by-reference: a(4fabf8ac) = 1, b(4fabf8a8) = 2

after call-by-reference: a(4fabf8ac) = 2, b(4fabf8a8) = 1

\end{lstlisting}

\noindent
Draw the run-time call stack during the execution of each function.

\item Compile and run the program
\url{operating-systems-lab-manual-shared-files/module0fundamentals/pointers2/learn_pointers.c}.
Understand how it works.

\item Explore the following programs in the share repository:

\begin{itemize}
\item \url{operating-systems-lab-manual-shared-files/module0fundamentals/pointers2/strstr.c}
\item \url{operating-systems-lab-manual-shared-files/module0fundamentals/pointers2/traverse.c}
\item \url{operating-systems-lab-manual-shared-files/module0fundamentals/pointers2/getline.c}
\item \url{operating-systems-lab-manual-shared-files/module0fundamentals/pointers2/strcpy.c}
\end{itemize}

The functions \texttt{strchr}, \texttt{strstr}, \texttt{strcpy}, \texttt{getline}/\texttt{fgets}, \texttt{malloc}, and \texttt{free} used in these
programs are helpful functions in C.

Examine the \texttt{man} pages for these functions.

For example:

\texttt{\$ man strchr}

\texttt{\$ man strstr}

\texttt{\$ man strcpy}

\texttt{\$ man getline}

\texttt{\$ man fgets}

\texttt{\$ man malloc}

\texttt{\$ man free}

What does \texttt{strchr} do?

What does \texttt{strstr} do?

How is \texttt{getline} different from \texttt{scanf}?

\item Setup your Raspberry Pi using the step-by-step instructions at
\url{https://perugini.cps.udayton.edu/teaching/books/SPUC/www/RaspberryPi/index.html}

\end{enumerate}

%%%%%% END MAIN CONTENT OF LAB%%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

\section{Thematic Takeaways}

\begin{itemize}
\item C only uses pass-by-value.  We simulate pass-by-reference in C by passing the
argument's address \textit{by value}.
%\item ~
%\item ~
\end{itemize}

%\section{Lab Summary}

%\section{Key Terms}

%\section{Bibliographic Notes}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
