SRC = OSlabManual

LATEX = pdflatex
OPTS = -shell-escape

OTHER = acknowledgments.tex \
#Chap2/systemcalls.tex \
vita.tex

all: $(SRC)

$(SRC): $(SRC).pdf 

$(SRC).pdf: $(SRC).tex $(OTHER)
	$(LATEX) $(OPTS) $(SRC)
	bibtex $(SRC)
	$(LATEX) $(OPTS) $(SRC)
	$(LATEX) $(OPTS) $(SRC)

spell:
	detex $(SRC) $(OTHER) | spell | sort -u

clean:
	- touch *.tex
	- rm $(SRC).bbl $(SRC).blg $(SRC).out $(SRC).lot $(SRC).lof $(SRC).toc $(SRC).log $(SRC).aux $(SRC).pdf $(SRC).listing
