/*
 * myioctl.c
 */
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include "myioctl.h"

static int device_open(struct inode *inode, struct file *file)
{
	printk(KERN_INFO "myioctl: Device open\n");
	return 0;
}

static int device_release(struct inode *inode, struct file *file)
{
	printk(KERN_INFO "myioctl: Device release\n");
	return 0;
}

static long device_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	switch (cmd) {
	case MYIOCTL_CMD:
		printk(KERN_INFO "%s \n", (char *)arg);
		break;
	}

	return 0;
}

static struct file_operations fops = {
	.open = device_open, 
	.unlocked_ioctl = device_ioctl,
	.release = device_release
};

struct cdev *kernel_cdev; 

static int device_init(void) 
{
	int ret;
	dev_t dev_no, dev;

	kernel_cdev = cdev_alloc(); 
	kernel_cdev->ops = &fops;
	kernel_cdev->owner = THIS_MODULE;
	printk("myioctl: Initializing device...\n");
	ret = alloc_chrdev_region(&dev_no, 0, 1, DEVICE_NAME);
	if (ret < 0) {
		printk("Major number allocation failed\n");
		return ret; 
	}
	
	major_no = MAJOR(dev_no);
	dev = MKDEV(major_no, 0);
	printk("The major.minor number for your device is %d.%d\n", major_no, MINOR(dev_no));
	ret = cdev_add(kernel_cdev, dev, 1);
	if (ret < 0 ) {
		printk(KERN_INFO "Unable to allocate cdev\n");
		return ret;
	}
	return 0;
}

static void device_exit(void) {
	printk(KERN_INFO "myioctl: Removing device...\n");
	cdev_del(kernel_cdev);
	unregister_chrdev_region(major_no, 1);
	printk(KERN_INFO "myioctl: Device removed, goodbye!\n");
}

MODULE_LICENSE("GPL");
module_init(device_init);
module_exit(device_exit);
