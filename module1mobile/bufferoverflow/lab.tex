% uncomment following line when compiling entire book
\graphicspath{{"module1mobile/bufferoverflow/figs/"}}
% uncomment following line when compiling individual chapter
%\graphicspath{{"figs/"}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpg,.png}

\chapter{Buffer Overflow Attacks and Defenses}
\label{CHAP:BOF}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Authors: Phu H. Phung and Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}
The goal of this lab is to understand the memory layout of a program in execution and how unprotected memory in a program can be exploited to launch attacks. You will perform a real-world buffer overflow attack on a vulnerable program to execute shellcode to gain the root privilege of the system. You are provided with the vulnerable binary program (\textbf{\textcolor{red}{myecho}}) and shellcode (\textbf{\textcolor{red}{shellcode.txt}}). You need to complete setup to set the vulnerable program with the root privilege to perform the attack. In particular, the objectives of this lab are to:

\begin{itemize}
\item understand the stack layout in the memory of a program by executing the program in a debug environment;

\item identify the buffer size in a program and its memory address;

\item overwrite the return address to redirect the program counter to any
memory address;

\item construct binary code with a shellcode to perform the buffer overflow
attack to execute the shellcode under the root privilege; and

\item understand the memory protection mechanism against buffer overflow
attacks (i.e., address randomization)  by the operating systems.

\end{itemize}


\section{Preparation}

This lab is prepared on the \textsc{seed} virtual machine (\textsc{vm}) with
Ubuntu 16.04. Therefore, you need to have the \textsc{vm} ready. If you have
not installed this \textsc{vm}, follow the instructions at
\url{https://bit.ly/SEED-VM} to download and install it.

Before you start these tasks, you need to turn off the protection mechanism in Ubuntu (i.e., randomize the starting address of heap and stack to make guessing buffer address difficult). Also, we assume that the vulnerable program has the root privilege, so we simulate this by manually setting the root privilege for the program. (\textbf{In reality, this process cannot be done by attackers, but the vulnerable program already has the root permission.})

\noindent First, run the \textsc{vm} and download the program for this lab with the \texttt{wget} command and save it as \texttt{myecho} with the \texttt{-O} (i.e., uppercase O) option:
\begin{verbatim}
$ wget -O myecho https://bit.ly/bof-myecho 
\end{verbatim}
Ensure that the \texttt{myecho} file is successfully downloaded:  
\begin{verbatim}
$ ls -la myecho
\end{verbatim}

\noindent
\textbf{Note:} Manually type the above and later commands. Do NOT copy and paste them from this instruction as there may be formatting errors when pasting. The following screenshot demonstrates these steps:

\medskip

\begin{center}
\includegraphics[width=\textwidth]{wget.png}
\end{center}

\noindent Next, use the following commands to turn off the address
randomization and to set the root privilege for the program:

\begin{itemize}
    \item Login as root:\\
    \verb!$ su root!\\
    Enter root password: \textbf{seedubuntu}. Now you should see the $\#$ prompt.
    \item Turn off the address randomization:\\
    \verb!# sysctl -w kernel.randomize_va_space=0!
    \item Set the owner of the program to root:\\
    \verb!# chown root:root myecho!
    \item Make the program executable in the privilege of its owner:\\
    \verb!# chmod 4755 myecho!
    \item Exit the root session:\\
    \verb!# exit!
    \item To ensure you are not in the root session, enter:\\
    \verb!# whoami!\\
    You should see \texttt{seed}.
\end{itemize}

\noindent
The following screenshot demonstrates the above steps:

\medskip

\begin{center}
\includegraphics[width=\textwidth]{chmod.png}
\end{center}

\noindent Now, you should verify that the program has the root privilege
(\texttt{\$ ls -la myecho}) and execute the program with input (e.g., 
\texttt{\$ ./myecho CPS356}) to make sure the program runs properly:

\medskip

\begin{center}
\includegraphics[width=0.8\textwidth]{myecho-root.png}
\end{center}

\noindent
For your reference, the \texttt{myecho} file downloaded above is a binary executable compiled from the following C program, where \texttt{xxx} (in line 6) is a three-digit number as the buffer size: 

%\lstinputlisting[language=C,label=lst:Cprogram]{src/myecho.c}

\begin{lstlisting}[language=C,label=lst:Cprogram]
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int callee(char *str) {
   char buf[xxx];
   strcpy(buf,str);
   printf("%s\n", buf);
}
int main(int argc, char *argv[]){
   callee(argv[1]);
}
\end{lstlisting}

The above program is vulnerable to buffer overflow attacks because it uses the vulnerable function \texttt{strcpy} (line 7) to copy an argument to a buffer (\texttt{buf} declared in line 6). The program is given to help you understand the lab. However, attackers normally do not have access to the source code of a vulnerable program. 

In the next tasks, you will run and debug this program to identify the size and the address of the buffer. You will then construct a payload with shellcode and run the program with this payload to invoke the shellcode under the privilege of the program. 

\section{Lab tasks}

\subsection{Task 1: Identify the buffer size and overwrite the return address} 

You can run the \textbf{\textcolor{red}{myecho}} program with input data
generated by a Perl script (e.g., \verb!$(perl -e "print 'A'x?")!). You need to
replace \texttt{?} by a specific number (e.g., 100..999). For example:
\verb!$ ./myecho $(perl -e "print 'A'x500")!.
You need to change the number until you get to the boundary between the segmentation fault and successful of the execution. For example, if at $n$, the program has a segmentation fault message and at $n-1$, the program executes normally, then $n-1$ should be the buffer size. Hint: you can apply the half-interval search algorithm manually or write a script to do the above steps to find the number (i.e., the buffer size). The following screenshot illustrates a manual trial and error method:

\medskip

\begin{center}
\includegraphics[width=\textwidth]{buffersize.png}
\end{center}

\noindent \textbf{Note}: You need to remember the buffer size (i.e., $n-1$)
above to complete the remaining tasks.


After identifying the buffer size, you can try to overwrite the return address of the program to a dummy address. You can do this by running the program in a debug environment such as \texttt{gdb}: \verb!$ gdb myecho!. To ensure that you will overwrite the return address, you need to insert a dummy address (i.e., a 4-byte string that is different from \texttt{A}; e.g., \texttt{BCDE}) to see if the stack pointer points to this address. To run the program in \texttt{gdb} with inputs, use the command: \verb!$ run <input>!. You can use the input generated by the Perl command above and append the dummy address:\\
\verb!$ ./myecho $(perl -e "print 'A'x? . 'BCDE'")!.
(Replace \texttt{?} by the $n$ number identified previously. This number should be $n$, not $n-1$):
%Note for instructors: the number for this lab is 592.

\medskip

\begin{center}
\includegraphics[width=\textwidth]{gdb.png}
\end{center}

\noindent
With the above command, the program is executed and then stopped at the address of $\backslash$\texttt{x45444342}:

\medskip

\begin{center}
\includegraphics[width=\textwidth]{overflow.png}
\end{center}

\noindent
The hex value $\backslash$\texttt{x45444342} is the Little Endian Byte Order of $\backslash$\texttt{xBCDE}---the dummy address we provided. This confirms that the number $n$ we identified earlier is correct and indicates that you have successfully overwritten the return address of the program counter. If you do not see the same value of the address you inserted, \textit{you need to adjust the number until you see the same address value at the invalid program counter}. This number is referred as $n$ in the remainder of this lab. In the next task, you will identify the buffer address in the memory so that you can overwrite the return address with this buffer address. 

\subsection{Task 2: Identify the buffer address} 

To identify the buffer address, we need to debug the program and identify where a vulnerable function with a buffer is called (e.g., \texttt{strcpy}). You need to set a break point after this function call to view the stack to identify the buffer address:

\renewcommand{\theenumi}{\roman{enumi}}
\begin{enumerate}
\item In the \texttt{gdb} environment, disassemble the 
\texttt{main} function of the program: \texttt{\textcolor{red}{gdb-peda\$}} \verb!disas main!.
We can then trace to find the location of a vulnerable function where
a buffer overflow error can happen. In this program, \verb!strcpy! is a
vulnerable function:

    \medskip

\begin{center}
\resizebox{\textwidth}{!}{
    \includegraphics[width=\textwidth]{disas.png}}
\end{center}
    
    \item Set a break point after that function is called (at the pointer
\texttt{<+31>} and before it is returned (function \texttt{ret} in at the
pointer \texttt{<+54>} in the above screenshot). Select the pointer
\texttt{<+40>} and set a break at this pointer:
\texttt{\textcolor{red}{gdb-peda\$}} \verb!break *main+40!, and then run the
program again with the same input identified in Task 1:
\texttt{\textcolor{red}{gdb-peda\$}} \verb! run $(perl -e "print 'A'x? .  'BCDE'")!.  (Replace \texttt{?} by the $n$ number identified previously).

    \medskip

\begin{center}
\resizebox{\textwidth}{!}{
    \includegraphics[width=\textwidth]{break.png}}
\end{center}

\noindent As illustrated in the screenshot above, which presents some
information about memory registers and pointers, the program is now paused at
the break point.  To view the contents of the program's stack, we need to
display the content of the stack pointer (stored in ESP). The following command
displays the top 200 bytes of the stack: \texttt{\textcolor{red}{gdb-peda\$}}
\verb! x/200xb $esp!

    \medskip
\begin{center}
\resizebox{\textwidth}{!}{
\includegraphics[width=\textwidth]{esp-address.png}}
\end{center}

\noindent
    You will see the contents of the stack with the memory addresses and the data we provided. You should see the long list of \texttt{A} in hex form \texttt{0x41}. The buffer address should be where the first occurrence of the \texttt{0x41} (noted by the arrow in the above screenshot). For simplicity, we select the address in the next row (\texttt{0xbfffe790}), as illustrated in the above screenshot. We will use this address to replace the dummy address \texttt{BCDE} above. In the next task, you will construct a payload with shellcode, and the identified buffer address. This payload will permit the program to invoke the shellcode by overwriting the return address to the buffer address.

%\begin{center}
%\includegraphics[width=\textwidth]{address.png}
%\end{center}
    
\end{enumerate}

\subsection{Task 3: Construct the payload}

The payload in a buffer overflow attack, in general, is an input string that contains binary code to permit a vulnerable program to invoke this code. In this lab, we will use a pre-generated shellcode available at \url{https://bit.ly/shellcode-txt}. The shellcode is in hex form compiled from the system command \verb!$ exec /bin/dash!, which executes a dash shell. You can use \texttt{wget} to download it and save it (e.g., to  \texttt{attack-code.pl}):\\
\verb!$ wget -O attack-code.pl https://bit.ly/shellcode-txt!

\medskip 

\begin{center}
\includegraphics[width=\textwidth]{downloadshellcode.png}
\end{center}

\noindent
Since the payload length must be the same as we used in Task 2 and the shellcode length is typically smaller than the buffer size, we need to pad to the code. We can use the \texttt{NOP} command in assembly ($\backslash$\texttt{x90} in hex) that moves the program counter to the next instruction in memory. You need to construct the full payload in the format: $[\backslash$x$90^{n -shellcodelen}][shellcode][Address]$, where:
\begin{itemize}
    \item $n$ is the buffer size identified in Task 1, and
    \item $shellcodelen$ is the number of bytes of the shellcode.
\end{itemize}

\noindent To determine the size of the shellcode in number of bytes, you can
open it in a text editor such as Sublime to count the number of bytes by
searching for the string $\backslash$\texttt{x}:

\medskip

\begin{center}
\includegraphics[width=\textwidth]{shellcodelen.png}
\end{center}

\noindent
As you can see from the above screenshot, there are 46 matches of $\backslash$\texttt{x}. Thus, the shellcode length is 46. We can construct the payload in the above format in a Perl script as follows:\\
\verb!print "\x90" x (n - 46) . "[shellcode]" . "[address]"!
where:
\begin{itemize}
    \item \texttt{n} is the buffer size identified in Task 1;
    \item \texttt{[shellcode]} is the original content from the downloaded \texttt{attack-code.pl} file; and
    \item \texttt{[address]} is the buffer address identified in Task 2 in the Little Endian Byte Order. For example, the address \texttt{0xbfffe790} (identified in Task 2) must be written as $\backslash$\texttt{x90}$\backslash$\texttt{xe7}$\backslash$\texttt{xff}$\backslash$\texttt{xbf}. Note that you will get a \textbf{different address} when doing this lab in your own virtual machine.
\end{itemize}
You can open the file (e.g., \verb!$ subl attack-code.pl!) and edit it  in the above format and content:

\medskip

\begin{center}
\includegraphics[width=\textwidth]{attack-code.png}
\end{center}

\subsection{Task 4: Launching the attack}

Execute the program with the payload constructed in Task 3:
\verb!$ ./myecho $(perl attack-code.pl)!.
If successful, the attack should happen: you will attain a root
shell; you can verify this by entering \# \textbf{\textcolor{red}{whoami}}:

\medskip

\begin{center}
\includegraphics[width=\textwidth]{attack.png}
\end{center}

\noindent
\textbf{Trouble-shooting:} typically, the attack may not work at first because of invalid memory addresses. If this is the case, you can try to increase the buffer address closer to the shellcode. To determine an exact address, you can perform Task 2.ii again (ensure that you have set the break point). However, you need to run the program in the debug mode with the \texttt{attack-code.pl} as the input, instead of the string: 
\texttt{\textcolor{red}{gdb-peda\$}} \verb! run $(perl attack-code.pl)! 
and view a longer size of the stack. For example: 
\texttt{\textcolor{red}{gdb-peda\$}} \verb! x/999xb $esp!\\

\noindent You can view the shellcode (and its exact address) and the return
address on the stack:

\begin{center}
\includegraphics[width=\textwidth]{shellcodeinstack.png}
\end{center}

\subsection{Task 5: A Buffer Overflow Attack Countermeasure}

At the beginning of this lab (preparation), we turned off the default countermeasure in Linux. In this task, we will turn it on and launch the attack again to see if the attack still succeeds. To turn this on, you can simply restart the \textsc{vm}, or use the following command within the root shell:
\verb!# sysctl -w kernel.randomize_va_space=2!.
Repeat the attack as in Task 4. You should see that the attack is unsuccessful
(with a segmentation fault error):

\medskip

\begin{center}
\includegraphics[width=\textwidth]{protection.png}
\end{center}

\noindent
The attack is now unsuccessful because the system randomized the address each time the program runs. Therefore, the address you identified earlier is no longer the address of the buffer.

%%%%%% END PREFIX %%%%%%%

%%%%%% END MAIN CONTENT OF LAB %%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

\section{Thematic Takeaways}

\begin{itemize}
\item Any C/C++ program uses functions such as \texttt{strcpy} that do not have bound-check and does not validate the input are vulnerable to the buffer-overflow attacks.
\item With a buffer-overflow attack, attackers can inject and run malicious code in the vulnerable program under the privilege of the program.
\item Modern operating systems provide mechanisms such as address randomization to protect against buffer-overflow attacks, however, the buffer-overflow vulnerability still exists if the programmer do not consider the security issue in the code. 
\end{itemize}

\section{Lab Summary}
In this lab, you have learned what is a buffer-overflow attack and how to perform such an attack. Through the hands-on steps, you have learned how the memory layout of a program is organized and how to identify buffer memory address. You also know the way to overwrite a return address to execute code at an arbitrary address.  

\section{Key Terms}

Address randomization, buffer, memory address, overflow, overrun, vulnerability.

\section{Bibliographic Notes}

%Tsipenyuk, K., Chess, B., \& McGraw, G. (2005). Seven pernicious kingdoms:
%A taxonomy of software security errors. \textit{IEEE Security \& Privacy}, 3(6), 81--84.

%\cite{Tsipenyuk}

%\bibitem[TCM05]{Tsipenyuk}
K.~Tsipenyuk, B.~Chess, and G.~McGraw.
\newblock Seven pernicious kingdoms: A taxonomy of software security errors.
\newblock {\em IEEE Security and Privacy}, \textbf{3}(6):81--84, 2005.

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
