% uncomment following line when compiling entire book
\graphicspath{{"module1mobile/Android/figs/"}}
% uncomment following line when compiling individual lab
%\graphicspath{{"figs/"}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpg,.png}

\chapter{Android Application Reverse Engineering}
\label{CHAP:ANDROID}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Authors: Phu H. Phung and Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

This lab will provide you hands-on experience in manipulating an Android app by
decompiling, modifying, and rebuilding it.  In particular, the objectives are
to:

\begin{itemize}

\item understand the architecture of an Android application and a hybrid mobile
application;

\item decompile an Android application file (\texttt{.apk}) and modify the
decompiled application; and

\item rebuild a modified Android application and deploy it to an Android
emulator to verify its functionality.

\end{itemize}

\section{Preparation}

This lab is prepared on the \textsc{seed} virtual machine (\textsc{vm}) with
Ubuntu 16.04. Therefore, you need to have the \textsc{vm} ready. If you have
not installed this \textsc{vm}, follow the instructions at
\url{https://bit.ly/SEED-VM} to download and install it.

\paragraph{Download the Android application.} First, we need to download an
Android application (app) and store it in the \textsc{vm} do perform this lab.
An Android application appears in the form of \texttt{.apk} file. There are
several places (in this lab, we use  \url{https://apkpure.com/}) that we can
search and download \texttt{.apk} files. We will use a hybrid Android app in
this lab. Hybrid mobile apps are apps developed using Web technologies, i.e.,
HTML, CSS, and JavaScript, and exported into a specific platform such as
Android or iOS. \texttt{czkiam.myapp} is such a hybrid app sample using the
Cordova framework. To download this, you can open a browser and enter this URL:
\url{https://apkpure.com/demo-app-cordova-vue-hybrid-app/czkiam.myapp}. Then,
click on the button Download APK to download it into your \textsc{vm}.
Alternatively, you can visit the website \url{https://apkpure.com/} and search
for \texttt{czkiam.myapp} to download.

\paragraph{Download the Apktool tool.}  Apktool is a Java-based tool used for
decompiling and rebuilding Android apps. To download this tool, visit
\url{https://ibotpeaches.github.io/Apktool/}, scroll down to the News section
and click on Download for downloading a version. In this lab, we use version
2.4.1 released in November 2019. Alternatively, you can directly download the
tool from
\url{https://bitbucket.org/iBotPeaches/apktool/downloads/apktool_2.4.1.jar}.

Assume that you downloaded the above files and saved them to the
\texttt{Downloads} folder. You can open the folder and verify as in the
following screenshot:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=.25]{0-downloads.png}}

\medskip

\section{Lab Tasks}

\subsection{Task 1: Upload and Run an APK Android app in an Android Emulator}

In principle, an APK Android app can be loaded and run in any physical Android
device. In this lab, we will use an Android emulator, a program that simulates
a physical Android device. There are many different Android emulators, however,
each of which requires a different installation process. To avoid this
complication, we can use an online Android emulator service available at
\url{https://www.apkonline.net}. This is a cloud-based service that allows
users to upload and run any APK Android app. To upload an APK Android app, you
can visit this URL \url{https://www.apkonline.net/filemanager.php?username=xxx}
in the browser, replace \texttt{xxx} with an ID that you should remember to use
later. You can click on the upload button on the website, then click on Browse
and select the \textsc{apk} file you have downloaded previously. Next, click on the
Upload APK button to upload the APK file as demonstrated in the following
figure:  

\medskip

\resizebox{\textwidth}{!}
{\includegraphics[scale=1.0]{1-uploadapk.png}}

\medskip

\noindent Once the \textsc{apk} file is uploaded, you should see it on the
website. You can click on the link Run\_APK to run the app as in the following
figure:

\medskip

\resizebox{\textwidth}{!}
{\includegraphics[scale=1.0]{2-runapk.png}}

\medskip

\noindent You need to wait for a while when the cloud service is preparing to
load the app. When it is successfully loaded, you can click on the Start button
to run the emulator:
 
\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{3-startapk.png}}

\medskip

\noindent It will take a while to start the emulator and the \textsc{apk} app.
When it is ready, you can click to the Enter button:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{4-enter.png}}

\medskip

\noindent
It will open the emulator and launch the \textsc{apk} app:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{5-androidrun.png}}

\medskip

\noindent You can interact with the app using your laptop's mouse or touch
screen to experience the app's functionality. The following screenshot
demonstrates that the app can retrieve the current geolocation:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{6-geolocation.png}}

\medskip

\subsection{Task 2: Reverse Engineer and Modify a Hybrid Android app} 

In this task, you will reverse engineer the downloaded \textsc{apk} file using the
Apktool tool to see the content of the app and to modify it.  

To reverse engineer or decompile an \textsc{apk} Android app with the Apktool tool, use
the  command: \texttt{java -jar apktool\_2.4.1.jar d}
$\mathtt{<}$\texttt{\textit{APK-file}}$\mathtt{>}$
\texttt{-o}
$\mathtt{<}$\texttt{\textit{newFolder}}$\mathtt{>}$,
where:

\begin{itemize}

    \item $\mathtt{<}$\texttt{\textit{APK-file}}$\mathtt{>}$
is the path of the \textsc{apk} filename. In this lab, we
use the default downloaded name of \texttt{Demo\ App\ Cordova\ Vue\ Hybrid\
App\_v1.0.0\_apkpure.com.apk}. (Note the \verb!\! for space.)

    \item $\mathtt{<}$\texttt{\textit{newFolder}}$\mathtt{>}$
is the folder you want to store the decompiled
files and directories from the \textsc{apk} file. 

\end{itemize}

The following figure demonstrates the decompiling process and view the
decompiled folder \texttt{newApk}:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{7-apktoold.png}}

\medskip

\noindent As you can see in the above figure, the decompiled Android APK file
contains various files and folders. As mentioned previously, in this lab, we
used a hybrid mobile app, where the core code of the app is written in HTML,
CSS, and JavaScript. These web-based files are stored in the
\texttt{assets/www} folder. You can open the file system to explore the
structure of the app:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{8-www.png}}

\medskip

\noindent As you can see in the above figure, there is an \texttt{index.html}
file in the \texttt{assets/www} folder. This is the main content of the app,
which includes HTML, CSS, and JavaScript as in the following figure:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{8-index.png}}

\medskip

\noindent As highlighted in the above figure, the app is linked to a JavaScript
file named \texttt{app.js}. This is normally the core code of the app. In this
lab, we will make several minor changes to this file to see how our changes are
effected. The first modification is to insert your name into the app's title.
To do so, you need to open the \texttt{app.js} file, search for the content
\texttt{Demo App}, and then add your name after that content as illustrated in
the following figure:

\medskip

\begin{center}
%\resizebox{\textwidth}{!}{
\includegraphics[scale=0.65]{8-code1.png}
\end{center}

\medskip

\noindent Next, you can scroll down to the end of the file and write one simple
line of JavaScript code to display a message box with your name:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{8-code2.png}}

\medskip

\noindent In the next task, you will rebuild this modified Android app to
verify that the modified Android app will be executed usually, and your added
code will also be executed. 

\subsection{Task 3: Rebuild a Modified Android app}

To rebuild an Android app, you need to repack it into an \textsc{apk} file, and then
sign it with a digital signature. 

To repack the modified Android app, ensure that you are outside of the
modified Android app's folder:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{9-folder.png}}

\medskip

\noindent Next, you can use the Apktool tool, to repack the folder into an \textsc{apk}
file using the  command: \texttt{java -jar apktool\_2.4.1.jar b}
$\mathtt{<}$\texttt{\textit{APK-folder}}$\mathtt{>}$ \texttt{-o}
$\mathtt{<}$\texttt{\textit{new-APK-folder}}$\mathtt{>}$, where:

\begin{itemize}

    \item $\mathtt{<}$\texttt{\textit{APK-folder}}$\mathtt{>}$
the folder that contains the Android app.

    \item $\mathtt{<}$\texttt{\textit{new-APK-folder}}$\mathtt{>}$
is the new \textsc{apk} filename you want to create. 

\end{itemize}

\noindent The following figure demonstrates the repacking process to repack the
modified Android app from the \texttt{newApk} folder and generate a new
\textsc{apk} file named \texttt{newApk.apk}:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{10-apktoolb.png}}

\medskip

\noindent Before the repacked \textsc{apk} file can be installed in an Android device,
it needs to be digitally signed. We can create a self-signed key using the
\texttt{keytool} command: \texttt{keytool -alias newApk -genkey -v -keystore
newApk.keystore -keyalg RSA -keysize 2048 -validity 10000}, where
\texttt{newApk} is the key alias and \texttt{newApk.keystore} is the filename
to store the generated key to use later:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{11-keytool.png}}

\medskip

\noindent You need to enter a some pieces of information and a password (and
you need to remember this password for the next step), and then the key will be
generated:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{12-keystore.png}}

\medskip

\noindent As shown in the waring, we need to migrate the key to PKCS12 industry
standard using the command \texttt{keytool -importkeystore -srckeystore
newApk.keystore -destkeystore newApk.keystore -deststoretype pkcs12}. Then you
can use the \texttt{jarsigner} command \texttt{jarsigner -keystore
newApk.keystore -verbose newApk.apk newApk} to sign the \textsc{apk} file with the
generated key file (you need to provide the password that you have created
previously):

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{13-jarsigner.png}}

\medskip

\noindent Now the modified \textsc{apk} app is ready to be installed and tested in any
Android device. We can repeat Task 1 to upload this modified \textsc{apk} file to the
\url{https://www.apkonline.net} system, and run the app:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{14-newapk.png}}

\medskip

\noindent
You can observe that the code you wrote in Task 2 is executed:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{14-newapk1.png}}

\medskip

\noindent
And the new content in the app's title is displayed:

\medskip

\resizebox{\textwidth}{!}{
\includegraphics[scale=1.0]{14-newapp2.png}}

\medskip

\section{Thematic Takeaways}

\begin{itemize}

\item An Android application is packed into an \textsc{apk}
file and can be installed
and run in an Android device/emulator.

\item An \textsc{apk} file can be decompiled (reverse engineering), modified,
and then repacked as a new valid Android app.

\item Android users should take serious cautions when installing a popular
Android app through an \textsc{apk}
file from an untrusted source since it might contain
malicious code injected by attackers.

\end{itemize}

\section{Lab Summary}

In this lab, you have gained hands-on experience running an APK Android app in
an emulator, decompiling an APK Android app, modifying the app's content/code,
and rebuilding and relaunch the modified APK Android app. You have experienced
that your injected code can be executed automatically without the user's
consent. In software security, it is a common attack where attackers modify
popular mobile apps to add malicious code and upload to a real market place to
fool users to download and install it. Please NOTE that these attacks are not
legal and unethical; therefore, you MUST NOT perform it in any real-world
scenarios.

\section{Key Terms}

Android, APK, Apktool, decompile, digital signature, hybrid mobile app,
rebuild, reverse engineering.

\section{Bibliographic Notes}

%\noindent
%Phung, P.H., Reddy, R.S., Cap, S., Pierce, A., Mohanty, A., \& Sridhar, M. (2020).
%A multi-party, fine-grained permission and policy enforcement framework for hybrid mobile applications.
%\textit{Journal of Computer Security}, \textbf{28}(3), 375--404.

%\cite{Phu}

%\bibitem[PRC{\etalchar{+}}20]{Phu}
P.H. Phung, R.S. Reddy, S.~Cap, A.~Pierce, A.~Mohanty, and M.~Sridhar.
\newblock A multi-party, fine-grained permission and policy enforcement
  framework for hybrid mobile applications.
\newblock {\em Journal of Computer Security}, \textbf{28}(3):375--404, 2020.

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
