% uncomment following line when compiling entire book
\graphicspath{{"module1mobile/BigSenseWorkbookV2/lab3/figs/"}}
% uncomment following line when compiling individual lab
%\graphicspath{{"figs/"}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpg,.png}

\chapter{BigSense}
\label{CHAP:BigSense}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Authors: Sumit Khanna, Andrew Rettig, and Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

\begin{itemize}

\item An established familiarity with virtual server setup and configuration. 

\item Translation of protocol and security fundamentals into applied network
and application configurations.  

\item Creation of a complete end-to-end IoT infrastructure.

\end{itemize}

%\section{Resources and Getting Help}

%\begin{itemize}
%\item ~
%\item ~
%\item ~
%\end{itemize}

%%%%%% END PREFIX %%%%%%%

\section{Introduction}

Thus far, you have be using an existing instance of BigSense set up for you by the
lab administrator. In the following chapter, you will learn how to setup your
own BigSense instance running on a Linux server.  With your team you will be
responsible for installing BigSense on one of the supported distributions and
reconfiguring LtSense to send data to the new service instance.  BigSense is
compatible with Ubuntu, Debian, CentOS and openSUSE; for this lab we will be
using Desktop Ubuntu 18.xx LTR.

Your team will take on the role of Linux system administrators. As part of the
lab your team is responsible for selecting an Infrastructure as a Service
(IaaS) platform to purchase and spin up a Linux server.   With IaaS you will
have access at the \textsc{os} level with the ability to configure your
firewall as well.  The procedures outlined in this workbook are primarily
guidelines.  Although the goals will be clearly stated, your group may need to
search for documentation (both official and unofficial) to solve the problems
presented to you. 

\section{Materials}

\begin{itemize}

    \item Virtual Server on IaaS (Digital Ocean is suggested as an IaaS
provider – 5 dollars per month; Any IaaS provider may be used but they must
offer Ubuntu Desktop 18.xx virtual machines)

\item 1 gigabyte of RAM on the VM

\item Configured Beagle Board with LtSense from previous labs.

\end{itemize}

\section{Procedure}
\begin{enumerate}
    \item \textbf{Install the Linux Operating System.}
    
    You will be installing the Virtual Machine from the IaaS provider console
in a browser.  We will be using Ubuntu Server 18.xx. 
    
(Most of these Linux distributions offer both desktop and server
installations, either in the form of separate downloads or as an option during
the installation process. The primary difference usually involves the desktop
installation having a graphical user interface.)
    
For this lab, it is recommended you install the server version and work through
the command line. Be aware that in production environments, system
administrators often use the server release and control everything from a
command prompt. 
   
    \item \textbf{Installation Best Practices.}
    
During the installation process, you will be guided through the basics on
\textsc{vm} creation.  Be conscious of how the provider gives you login access.
It is also recommended to enable an \textsc{ssh} port so you can work on your
server via command line.  Also, it is best to always login using your regular
user account and never as root. Its suggest that after your server is up and
running you add addition logins and also change the root password.  When you
need to run a command as root, prefix the command with sudo to execute it as
the administrator. After your install is complete, be sure you can login to the
terminal and run commands.
    
    \item \textbf{Firewall setup.}
    
After setting up the \textsc{vm} you will want to setup the Network Firewall.
In the firewall you can enable port 22 for ssh and a port for your web service.
Remember the web service port because you will need to configure BigSense.
    
    \item Updates\\
    
Before you get started, it is good practice to update your release of Ubuntu
Server to the most recent version of 18.xx.  
    
    \item Install and Setup a local PostgreSQL Database \\

    BigSense supports three databases for storing sensor readings: MySQL,
PostgreSQL and Microsoft SQL. MySQL and PostgreSQL are free and open source
(FOSS) while Microsoft SQL is commercial. For this lab, we’re going to be using
the PostgreSQL implementation.

    (This is a good time to check out some help files from your instructor.)
    
    \begin{itemize}
        \item Debian: \url{https://wiki.debian.org/PostgreSql}
        \item Ubuntu: \url{https://help.ubuntu.com/community/PostgreSQL}
        \item CentOS: \url{https://wiki.postgresql.org/wiki/YUM_Installation}
    \end{itemize}

\noindent Installation instructions will vary between distributions and may
place your PostgreSQL installation into various states. The important goals you
need to achieve before moving onto the next section include the following:
    
    \begin{itemize}
        \item Have working PostgreSQL instance that is up and running
        \item Install the postgis extension
        \item Create an empty database
        \item Create a user for that database that has unlimited access

	\item Make sure you can connect to that database from local host. (It
is possible to change the permissions for PostgreSQL to simplify this process.
However, this does compromise the database security.  Please search online for
PostgreSQL security and the configuration file if this is necessary.)
        
    \end{itemize}
    
    \item Install and Configure BigSense. 
    
    Make sure you figure out the system and service manager software (Upstart,
SystemV or SystemD) for your operating system.  Installing the incorrect
version of BigSense will cause complicated errors.
    
    Follow the official instructions for installing BigSense on the following page:
    
    \url{https://bigsense.io/display/overview/Installation}
    
\noindent After installation, you will need to go through the official
configuration instructions. The examples given on the official site are for an
instance of PostgreSQL running on the same sever as BigSense (localhost). 
    
    \url{https://bigsense.io/display/overview/Configuration}
    
\noindent After configuring BigSense and the database, you will need to start
the service. The startup procedure will vary depending on your Linux
distribution.
    
Most distributions use SystemD and BigSense can be started using the following:
    
    \texttt{\color{red}sudo systemctl restart bigsense.service}
     
\noindent
Check your log file for any errors. (After corrections be sure you are not
reading an old error that has been corrected.)
 
    \texttt{\color{red}tail -n 500 \texttt{-}f /var/log/bigsense/bigsense.log}\\   
    
\noindent
If there are no errors, you should be able to access the BigSense service
from the local machine by using the curl command or by typing the URL in the
command into a web browser:
    
    \texttt{\color{red}curl http://localhost:8080/Query/Latest/100.txt}\\
      
\noindent
The above command should return field headings with no data. (We have not
sent any sensor data to this web service yet.) If you encounter a 500 error or
see any errors in the log files, stop the BigSense service. (Replace start with
stop in the above service commands.) Verify your database and BigSense
configuration. 
    
    \item Reconfigure your Beagle Board from the previous labs to communicate
to your new BigSense server.
    
LtSense can be configured to send data to two different BigSense instances.
Reconfigure the Beagle Boards of each group member to add an additional
transport to send data to your new BigSense instance. 

\begin{mylisting}
[General]
sample_rate = 15

[Data]
  [[primary]]
    type = sense.xml
      [[[Identifier]]]
        type = name
        id = <YourUniqueName>
      [[[Location]]]
        type = gps

[Transport]
  [[yourserver]]
    type = http
    url = http://<YourServerIP>:<YourPort>/Sensor.sense.xml
    pause_rate = 1.0
    timeout = 10.0
      [[[Queue]]]
        type = memory
      [[[Security]]]
        type = none
  [[serverlab1and2]]
    type = http
    url = <Server IP/url>/Sensor.sense.xml
    pause_rate = 1.0
    timeout = 10.0
      [[[Queue]]]
        type = memory
      [[[Security]]]
        type = none

[Handlers]
  [[virtual]]
    type = 1wire
    device = u
\end{mylisting}
    
\noindent As with the other examples, replace
$\mathtt{<}$\texttt{\textit{YourUniqueName}}$\mathtt{>}$,
$\mathtt{<}$\texttt{\textit{YourServerIP}}$\mathtt{>}$, and
$\mathtt{<}$\texttt{\textit{YourPort}}$\mathtt{>}$
with your unique relay name and BigSense server IP
respectively. You designated the Port during your firewall additions earlier in
the lab.
    
The above example also expects a real GPS and 1-wire sensors. You can switch
these out to virtual sensors instead if you’d like, using the examples in the
previous labs.
    

After making your configuration changes on your Beagle Board, be sure to
restart the service (systemctl restart ltsense.service) and check the log files
to ensure it starts up correctly. You should see both transports present like
so:
    
\begin{mylisting}
    2015-08-12 22:49:43,791: Generated XML
<?xml version="1.0" ?>
<sensedata>
   <package id="VirtualRelay01" timestamp="1439390982673">
      <gps>
         <accuracy altitude_error="46.0" climb_error="" latitude_error="15.452" longitude_error="11.167" speed_error="30.9" track_error=""/>
         <location altitude="234.7" latitude="50.924608333" longitude="11.573911667"/>
         <delta climb="0.0" speed="0.0" track="187.85"/>
      </gps>
      <sensors>
         <sensor id="FFF34E141400" type="Temperature" units="C">
            <data>31.625</data>
         </sensor>
      </sensors>
   </package>
</sensedata>

2015-08-12 22:49:43,793: Preparing payload for transport to http://192.168.1.50:8080/Sensor.sense.xml
2015-08-12 22:49:43,794: Preparing payload for transport to https://dev-ud.bigsense.io/Sensor.sense.xml
2015-08-12 22:49:43,841: Payload transported. (Queue Size: 0)
2015-08-12 22:49:44,542: Payload transported. (Queue Size: 0)
\end{mylisting}
     
\noindent
You may also want to run a curl command or run a GET request in a browser.
    
\texttt{\color{red}curl <Server IP/url>/Query/Latest/100.txt?RelayID=<YourRelayID>}\\   

Now that you have established your own server instance, you can also experiment
with stopping BigSense while monitoring the logs on LtSense. When you do this,
LtSense will continue to send data to the classroom instance of BigSense, but
data to your local instance will result in an error and LtSense will place your
data in queue. Once you start BigSense again, LtSense will transmit all that
queued data back to BigSense.
    
With our current configuration, the Queue for each Transport has a type of
memory. This is an in-memory queue, which means all the data in the queue will
be lost if your Beagle Bone loses power or is reset. An alternative is using an
sqlite queue which will save the data to the on-board disk or flash memory.
    
\begin{mylisting}
    [[[Queue]]]
        type = sqlite
        data = /var/lib/ltsense/queue.sqlite
\end{mylisting}
    
\end{enumerate}

\section{Summary}
   
In this lab, you have learned how to establish your own BigSense instance on a
Linux server and configure the LtSense on your Beagle Board to transmit sensor
data to it, in addition to your current classroom BigSense server. You have
learned the basics of running Linux services and simple database
administration.

It is important to note we have only covered the basic of system administration
in this lab. In a production environment, there are usually other security
measures that need to be considered. For example, when sending data to a
production server, it does not go to BigSense directly. First, it passes
through a service called HAProxy (a load balancer), running on a different
server, which handles the encryption (https) and passes the request to
BigSense. Also, a production BigSense instance is running on a different server
than its PostgreSQL database.

Some topics, including load balancing and security, while important, are beyond
the scope of this lab. If you plan on using BigSense in a production
environment in the future, it is important you research Linux security and work
with people experienced with the field of development and operations (also
known as ``dev-ops'').

%%%%%% END MAIN CONTENT OF LAB%%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

\section{Thematic Takeaways}

\begin{itemize}

\item Embedded \textsc{sbc} skills transfer to server management with
simple commands and communication protocols.

\item Virtual servers enable faster testing, development and deployment.

\item IoT expands traditional client-server development to
many more surface areas starting with the sensor and device before the
server and application.

\end{itemize}

\section{Lab Summary}

You have completed the Internet of Things workbook using BigSense and LtSense.
The BigSense project is free and open source: anyone is free to examine its
internals, modify it to suit their needs---so long as the result is released as
free and open source as well, and contribute to help improve it.

%BigSense is a work in progress. By completing the prior three labs, you have
%helped us get closer to making BigSense more available and accessible to other
%researchers, students, and professionals. We hope to expand both
%BigSense/LtSense to support more sensors as well as integration with other
%\textsc{gis} software.

If you would like to continue following our progress with BigSense, 
visit our website, sign up for the mailing list, or view the source code on
GitHub:

\begin{itemize}

\item
\url{https://bigsense.io}

\item
\url{https://groups.google.com/forum/#!forum/bigsense }

\item
\url{https://github.com/bigsense/}

\end{itemize}

\section{Key Terms}

Database, firewall, IaaS, service, virtual server.

\section{Notes from the Authors}

LtSense and BigSense are open source projects, meaning all the internals and
source code are available for anyone to examine, modify, improve and build upon
so long as the results are always released under the terms of the GNU Public
License v3.0. BigSense is a web service which can be used to store and retrieve
information from environmental sensor networks.  Ltsense is written in python
and designed to retrieve sensor data on single board computers for
environmental monitoring. 

BigSense was created some years ago for a specific monitoring site. Over the
years, we have worked with others to expand BigSense to be a general purpose
sensor monitoring tool.  Although this software has been used for previous
courses it is still fairly new.  Thus, bottlenecks and bugs may exist.
If you find bugs in either BigSense or LtSense, please report them on the
appropriate issue trackers:

\medskip

\noindent
\url{https://github.com/BigSense/LtSense/issues}\\
\url{https://github.com/BigSense/BigSense/issues}\\

\begin{flushright}
--- Sumit Khanna\\
\url{sumit@penguindreams.org}
\end{flushright}

I provide additional assistance to instructors on both GitHub and on a Slack
support channel.  Please e-mail with a link to your current university position
for the instructor invites.

\begin{flushright}
--- Andrew Rettig\\
\url{andrew.rettig@lildata.monster}\\
\url{arettig1@udayton.edu}\\
\url{https://www.lildata.monster/}
\end{flushright}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
