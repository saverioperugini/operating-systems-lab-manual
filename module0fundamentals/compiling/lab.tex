%\renewcommand{\chaptername}{Lab}
%\renewcommand{\thechapter}{4}
\chapter{Compiling C in Linux:
       Storage and Linkage Classes; and
       Threads and Thread-safe Functions}
\label{CHAP:COMPILINGLAB}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Author: Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

\begin{itemize}
\item An understanding of how to separately compile and link C modules.
\item An understanding of the overloaded use of the \texttt{static} modifier
in C.
\item Explore what is problematic about the use of global variables.
\item Introduce \textit{thread-safe} functions.
\end{itemize}

\section{Resources and Getting Help} 

\begin{itemize}
   \item Compiling C in Linux:
      \url{https://perugini.cps.udayton.edu/teaching/books/SPUC/www/lecture_notes/compiling.html}

   \item Threads \& Thread-safe Functions Notes:
      \url{https://perugini.cps.udayton.edu/teaching/courses/cps346/lecture_notes/threads.html}

    \item Linux Manpages (e.g., \texttt{\$ man gcc})
\end{itemize}

%%%%%% END PREFIX %%%%%%%

%%%%%% BEGIN MAIN CONTENT OF LAB %%%%%%%

\section{Steps}

\subsection{Collecting necessary files for the lab}

\begin{enumerate}

\item \texttt{ssh} into your Raspberry PI with the \texttt{-X} option on to
\texttt{ssh} in Putty (under the SSH X11 menu).

\lstset{language=bash}
\begin{lstlisting}
$ ssh -X pi@131....
\end{lstlisting}

\item Copy the following files from the share repository to your Raspberry Pi account:

\begin{lstlisting}[language=bash,numbers=none]
$ scp operating-systems-lab-manual-shared-files/module0fundamentals/compiling/compilationstages.tar \
   piusername@ipaddressofpi:
$ scp operating-systems-lab-manual-shared-files/module0fundamentals/compiling/static.tar \
   piusername@ipaddressofpi: 
$ scp operating-systems-lab-manual-shared-files/module0fundamentals/compiling/strtok.tar \
   piusername@ipaddressofpi: 
\end{lstlisting}

\item Untar the files on your Raspberry Pi:

\begin{lstlisting}[language=bash,numbers=none]
$ tar xvf compilationstages.tar
$ tar xvf static.tar
$ tar xvf strtok.tar
\end{lstlisting}
\end{enumerate}

\subsection{Exploring separate compilation and linking}

\begin{enumerate}
\item Explore the image 
\url{https://perugini.cps.udayton.edu/teaching/books/SPUC/www/lecture_notes/images/compilationstagesc.png}
and the following files:

\url{operating-systems-lab-manual-shared-files/module0fundamentals/compiling/f1.c}
\url{operating-systems-lab-manual-shared-files/module0fundamentals/compiling/f2.c}
\url{operating-systems-lab-manual-shared-files/module0fundamentals/compiling/main.c}.

\item Using that image and those files, run \texttt{gcc} on the files
to walk through each compilation step depicted in the figure.

Run the \texttt{file} command on the output of every stage.

For instance,

\begin{lstlisting}[language=bash,numbers=none]
$ file f2.s

$ file f1.o ...

$ file a.out
\end{lstlisting}

What do you notice?

Examine
\url{https://perugini.cps.udayton.edu/teaching/books/SPUC/www/lecture_notes/images/gccopts.png}
and review and study which option control at which stage the compilation
process stops.

\end{enumerate}

\subsection{Storgage Class and Linkage Classes}

\begin{enumerate}
\item Every process has three storage classes
\begin{itemize}
    \item local/stack/automatic storage
    \item heap/dynamic storage
    \item global/static storage
\end{itemize}

Any data in a process is stored in one of these three locations.

In the context of computer science,
\begin{itemize}
   \item \textit{static} means `fixed before run-time'
   \item \textit{dynamic} means `changeable during run-time'
\end{itemize}

\item Name two `times' before run-time.  Hint: they both start with
the letter 'l'.

Examine
\url{https://perugini.cps.udayton.edu/teaching/books/SPUC/www/lecture_notes/images/compilationstagesc.png}
for help.

\item There are two linkage classes:
\begin{itemize}
    \item internally-linked (accessible only from within \texttt{.c} module)
    \item externally-linked
(accessible both from within and from outside of \texttt{.c} module)

\end{itemize}

In Java, an externally linked function is \texttt{public} while an
internally-linked function is \texttt{private}.

C uses the single keyword \texttt{static} to modify both
storage and linkage class depending on the context:

\begin{itemize}
    \item before a global variable
    \item before a local variable
    \item before a function
\end{itemize}

\item Examine \texttt{x.c}.

What is the storage class of the \texttt{int x} variable?
What is the linkage class of the \texttt{int x} variable?

Compile and link the program using the steps/stages given above.

\item Now declare \texttt{x} as \texttt{static int x}.

Compile and link the program using the steps/stages given above.

Did the program compile?
Did the program link?
Did \texttt{static} modify the storage or linkage class of the variable \texttt{x}?

\item Experiment with the use of the \texttt{static} modifier before the
local \texttt{int y} variable in the \texttt{y} function in \texttt{y.c}.

Compile and link the program using the steps/stages given above.

What do you notice?

Did the program compile?

Did the program link?

Did \texttt{static} modify the storage or linkage class of the local variable \texttt{y}?

\item Now experiment with the use of the \texttt{static} modifier before the
function \texttt{y} function in \texttt{y.c}: \texttt{static void y()}.

Compile and link the program using the steps/stages given above.

What do you notice?

Did the program compile?

Did the program link?

Did \texttt{static} modify the storage or linkage class of the local variable
\texttt{y}?

\item Compile and link the program using the steps/stages given above. %note: what changed since last time we compiled? Why are we compiling again?

What do you notice?

Did the program compile?

Did the program link?

Did \texttt{static} modify the storage or linkage class of the function \texttt{y}?

\item Relate to \texttt{printf} with static buffer.

\item Relate to \texttt{strtok} with static index buffer.

\item Explore the difference between \texttt{strtok} and \texttt{strtok\_r}

\item Task: Compute the average number of words per line in a file.

See \texttt{wordaveragebad.c}

What is the approach: tokenize the file based on \texttt{\textbackslash{}n} and
then tokenize each line based on \texttt{\textbackslash{}t}.

Compile and run \texttt{wordaveragebad.c.}

\item Does it work as expected?

Why not? Read the bottom of the  \texttt{manpage} for \texttt{strtok} (titled
\textsc{bugs}) for help.

\item Explore \texttt{wordaverage.c}.

What is the only difference between \texttt{wordavergebad.c} and
\texttt{wordaverage.c}?

Compile and run \texttt{wordaverage.c}.
\end{enumerate}

%%%%%% END MAIN CONTENT OF LAB%%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

\section{Thematic Takeaways}

\begin{itemize}
\item The \texttt{static} modifier in C can affect storage class or linkage
class of a (local or global) variable or function.
\item When modifying the linkage class of a function the absence or presence
of the \texttt{static} modifier is the C analog of the
\texttt{public} or \texttt{private} modifiers in Java.
\item The integrity of a global variable can become corrupted even in a 
single-threaded program (e.g., \texttt{wordaveragebad.c}).
\end{itemize}

%\section{Lab Summary}

\section{Key Terms}

\texttt{static}, thread-safe.

%\section{Bibliographic Notes}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
