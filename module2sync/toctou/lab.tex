% uncomment following line when compiling entire book
\graphicspath{{"module2sync/toctou/figs/"}}
% uncomment following line when compiling individual lab
%\graphicspath{{"figs/"}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpg,.png}

\chapter{Time-of-check to Time-of-use Race Conditions}
\label{CHAP:TOCTOU}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Authors: Phu H. Phung and Saverio Perugini \\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

This lab is intended to help you understand the vulnerability of time-of-check
to time-of-use (\textsc{toctou}) race conditions. You are provided an
application with some buggy APIs. Your tasks include exploiting the
\textsc{toctou} bug to perform the attack. In particular, the objectives are
to:

\begin{itemize}
\item understand the issues of time-of-check to time-of-use (TOCTOU)---a race condition bug;

\item exploit a \textsc{toctou} buggy program to perform an attack; and

\item simulate a single-process program as a multiple-process program.

\end{itemize}

\section{Preparation}

This lab is prepared on the \textsc{seed} virtual machine (\textsc{vm}) with
Ubuntu 16.04. Therefore, you need to have the \textsc{vm} ready. If you have
not installed this \textsc{vm}, follow the instructions at
\url{https://bit.ly/SEED-VM} to download and install it.

\noindent First, run the \textsc{vm} and download the program for this lab with the \texttt{wget} command and save it as \texttt{ShoppingCart.tar.gz} with the \texttt{-O} (i.e., uppercase O) option:

\begin{lstlisting}[language=bash,numbers=none]
$ wget -O ShoppingCart.tar.gz
https://bit.ly/ShoppingCart-toctou
\end{lstlisting}

\noindent
Ensure that the \texttt{ShoppingCart.tar.gz} file is successfully downloaded:  

\begin{lstlisting}[language=bash,numbers=none]
$ ls -la ShoppingCart.tar.gz
\end{lstlisting}

\noindent \textbf{Note:} Manually enter the above and later commands. Do NOT
copy and paste them as there may be formatting errors when pasting. The
following screenshot demonstrates these steps:

\medskip

\begin{center}
\includegraphics[width=\textwidth]{download.png}
\end{center}

\medskip

\noindent
Next, uncompress the download file with the following command:
\verb!tar -xvf ShoppingCart.tar.gz!. You should see the \texttt{ShoppingCart} folder with three files:\\
\begin{itemize}
    \item \texttt{ShoppingCart.jar}: the main program compiled in Java bytecode;
    \item \texttt{pocket.txt}: the file that stores purchased items. Originally, the content of this file is empty; and
    \item \texttt{wallet.txt}: the file that stores the current amount of money. Originally, the content of this file is 30000, meaning that there is \$30,000 in the wallet.
\end{itemize}

\medskip

You can list these files, and view and verify the content of the
\texttt{pocket.txt} and \texttt{wallet.txt} files using the \texttt{cat}
command:

\begin{center}
\includegraphics[width=0.75\textwidth]{tar.png}
\end{center}

\noindent
The main program in the  \texttt{ShoppingCart.jar} file is a simple command-line Java application performing the following tasks:

\begin{enumerate}
    \item print the current balance of the user's wallet (by reading the content of the \texttt{wallet.txt} file);
    \item print the product list and their prices;
    \item ask the user to select a product to buy; and
    \item check if the user's wallet's current balance is sufficient (i.e., equal or greater than the product's price):
    \begin{itemize}

	\item if the condition is false (i.e., the current balance is less than
the product's price), notify the user and exit the program.

        \item otherwise, perform the following steps:

        \begin{itemize}

            \item withdraw the amount of the product's price from the wallet
(by updating the content \texttt{wallet.txt} file with a new balance 
(e.g., \texttt{new\_balance = current\_balance - product\_price});

	    \item add the product to the pocket (by appending the product's
name in the \texttt{pocket.txt} file); and

            \item print the new balance and exit the program.
        \end{itemize}
    \end{itemize}
    
\end{enumerate}

\section{Lab Tasks}

\subsection{Task 1: Understanding the Application} 

In this task, you will execute the given program to observe and understand its
semantics. You can run the program with the \texttt{java} command from a
terminal:  \texttt{java -jar ShoppingCart.jar}. You can select a product (e.g.,
book) to buy. Since there is enough money in the wallet at the beginning, you
can buy any product. You can verify the purchase process by viewing the
contents of the \texttt{wallet.txt} and \texttt{pocket.txt} files:

\medskip

\begin{center}
\includegraphics[width=0.75\textwidth]{normalexecution.png}
\end{center}

\noindent Next, execute the program again and try to buy a product whose price
is greater than the current balance in the wallet. For instance, from the
previous step, if your current balance is \texttt{29900}, and consider trying to
buy a car, which has the price of \texttt{30000}. You should see that it is
not possible to buy a product in that scenario (i.e., the product's price is
greater than the current balance). Ensure that you check the contents of the
\texttt{wallet.txt}, and \texttt{pocket.txt} files before and after the
execution---the contents of these files should be unchanged:

\medskip

\begin{center}
\includegraphics[width=0.75\textwidth]{insufficentbalance.png}
\end{center}

\medskip

\noindent As you can see from the previous experiments, the program only allows
the user to purchase a product if there is sufficient balance. However, there
is a race condition in this program that an attacker can exploit to buy
products that cost more than the balance. For instance,  in such exploitation,
an attacker can buy both a book (whose price is \texttt{100}) and a car (whose
price is \texttt{30000}) with a balance of  \texttt{30000}, which is not
possible in a normal execution as demonstrated in the previous steps. In the
next task, you will perform the exploitation to understand the vulnerability.

\subsection{Task 2: Exploiting Time-of-check to time-of-use (TOCTOU) vulnerability} 

To exploit the vulnerability, you first need to reset the application to the
original state. To do this, uncompress the \texttt{ShoppingCart.tar.gz} file
(downloaded earlier) again and verify that the text files have their original
values (i.e., the content of \texttt{pocket.txt} file is empty and the content
of the \texttt{wallet.txt} file is 30000):

\medskip

\begin{center}
\includegraphics[width=0.75\textwidth]{retar.png}
\end{center}

\noindent Since a \textsc{toctou} race condition issue only happens when both
concurrent threads or processes access a shared resource simultaneously, and at
least one thread or process modifies that resource. The given
\texttt{ShoppingCart} program is a single-process program; however, we can
simulate it as a multi-process program by executing it in two different
terminals concurrently. Right-click on the current terminal (Terminator) of the
\textsc{vm}, select Split Vertically or Split Horizontally to open two terminal
tabs:

\medskip

\begin{center}
\includegraphics[width=0.75\textwidth]{split.png}
\end{center}

\medskip

\noindent Next, in one terminal (left in the following figure), execute the
program. You should see the original balance of \texttt{30000}. Do NOT type
anything to buy at this time. In the other terminal (right), verify that the
text files have their original values:

\medskip

\begin{center}
\includegraphics[width=\textwidth]{twoterminals.png}
\end{center}

\medskip

\noindent Next, execute the program in the other terminal (right), and enter a
product to buy (e.g., book). You can buy the product properly and verify it by
viewing the text files' contents:

\medskip

\begin{center}
\includegraphics[width=\textwidth]{firstbuy.png}
\end{center}

\medskip

\noindent
Now return to the first terminal (left) and enter a car to buy:

\medskip

\begin{center}
\includegraphics[width=\textwidth]{secondbuy.png}
\end{center}

\medskip

\noindent As demonstrated in the above screenshot, by executing the programs
concurrently, we can now buy products that cost more than the balance (i.e.,
can buy both a book whose price is \texttt{100} and a car whose price is
\texttt{30000} with a balance of  \texttt{30000}). This is due to the
\textsc{toctou} issue: in the left terminal, when the program runs, it loads
the \texttt{wallet.txt} file that has the original balance (time-of-check).
Before that balance is used, the content of the \texttt{wallet.txt} file has
been updated by another process (in the right terminal). However, it is not
updated by the program in the left terminal. Thus, when the user buys a
product, it uses the balance loaded earlier (time-of-use), which is out-of-date
and is not synchronized properly. 

%%%%%% END PREFIX %%%%%%%

%%%%%% END MAIN CONTENT OF LAB %%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

\section{Thematic Takeaways}

\begin{itemize}

\item Any shared resource that is neither unprotected nor synchronized among
concurrent processes or threads will lead to a Time-of-Check to Time-of-Use
(TOCTOU) race condition.

\item Attackers or malicious users can exploit a \textsc{toctou} race condition
in a buggy program to perform an attack that makes data inconsistent. 

\item TOCTOU race conditions are a bug in programs and can be avoided by
synchronizing program access to shared resources.

\end{itemize}

\section{Lab Summary}
In this lab, students have learned about a Time-of-check to Time-of-use race condition and how to exploit it in a buggy program to perform an integrity attack.

\section{Key Terms}
Concurrent, data race, multi-thread, multi-process, race condition,
time-of-check to time-of-use, TOCTOU.

\section{Bibliographic Notes}

%\cite{BuildingSecureSoftware}

%\noindent
%Viega, J. and McGraw, G. ``Building Secure Software.'' Addison-Wesley. 2002.

%\cite{BuildingSecureSoftware}

%\bibitem[VM02]{BuildingSecureSoftware}
J.~Viega and G.~McGraw.
\newblock {\em Building Secure Software}.
\newblock Addison-Wesley, Upper Saddle River, NJ, 2002.

\medskip

\noindent CAPEC-29: Leveraging Time-of-Check and Time-of-Use (TOCTOU) Race
Conditions. Available: \url{https://capec.mitre.org/data/definitions/29.html}
[Last accessed: 27 July 2020].

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
