%\renewcommand{\chaptername}{Lab}
%\renewcommand{\thechapter}{1}
\chapter{C Programming with Pointers Lab}
\label{CHAP:POINTERS1}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Author: Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

\begin{itemize}
\item Establish an understanding of pointers in C
\item Establish an understanding of Compilation with \texttt{gcc}
\item Establish an understanding of \texttt{printf} and format strings
\end{itemize}

\section{Lab Overview}
This lab introduces the following topics about the C programming language: 
\begin{itemize}
\item Pointers in C 
\item Compilation with \texttt{gcc}
\item \texttt{printf} and format strings
\end{itemize}

\subsection{Setup}

Included with this lab is a preconfigured VirtualBox image running Debian 9.1
x86-64.  The home directory of the \texttt{student} user contains a directory
called \texttt{lab-c-pointers} which contains all of the files necessary to
complete this lab. 

\section{Resources and Getting Help} 

\begin{itemize}
    \item System Libraries and I/O Notes:
       \url{https://perugini.cps.udayton.edu/teaching/books/SPUC/www/lecture_notes/librariesIO.html}

    \item Linux Manpages (e.g., \texttt{\$ man printf})
\end{itemize}

%%%%%% END PREFIX %%%%%%%

%%%%%% BEGIN MAIN CONTENT OF LAB %%%%%%%

%\section{Lab Tasks}
\section{Steps}

Complete each of the following tasks and include a screenshot (if requested)
and a detailed answer of any questions.  First, boot up the provided virtual
machine and log in as \texttt{student}. 

\begin{enumerate}

\item \textbf{Pointers in C}

To understand pointers, it is critical to understand that every variable in C
has three components: (1) a data type; (2) a value; and (3) an address in
memory.  Consider the following variable declaration in C: 

\noindent
\begin{lstlisting}[language=C,numbers=none]
int x = 2;
\end{lstlisting}

This variable named \texttt{x} is of data type \texttt{int} and is initialized to value $2$. 
Implicit and hidden in this declaration is the fact that the variable occupies some space in memory---in this case, because it is an \texttt{int} it takes four bytes of memory. 
An address identifies this space in memory. 

\textbf{Definition of a pointer:} A pointer is a memory address. 

This is the most concise definition of a pointer. 
To expand on it, consider the next variable declaration:

\noindent
\begin{lstlisting}[language=C,numbers=none]
int* x_ptr = &x;
\end{lstlisting}

This variable declaration is conceptually the same as the previous one. 
The variable named \mbox{\texttt{x\_ptr}} is of data type \texttt{int*}, has value \texttt{\&x}, and has a location in memory.  
\texttt{int*} is a data type that means `a pointer to an \texttt{int}.'
The \texttt{\&} operator is the `address-of' operator. 
\texttt{\&x} returns the address of the variable \texttt{x}. 
This satisfies the concise definition of a pointer mentioned above---that a pointer is a memory address. 
After this line of code, the variable of pointer type \mbox{\texttt{x\_ptr}} points to an \texttt{int} variable \texttt{x}. 

\textbf{Dereferencing:} A variable of a pointer type is `dereferenced' to access the value of the variable to which it points. 

\noindent
\begin{lstlisting}[language=C,numbers=none]
int y = *x_ptr;
\end{lstlisting}

\noindent This code introduces the dereference operator \texttt{*} (not to
be confused with the multiplication operator or the pointer data type
\texttt{*}).  It looks up the value that is stored at a memory address.  In
this case, \mbox{\texttt{x\_ptr}} points to \texttt{x} which has value 2. Thus
\mbox{\texttt{*x\_ptr}} returns the value 2.  This syntax \texttt{y = *p;}
with the dereference operator on the right-hand side of the assignment is
called a \textit{load} operation. 
The dereference operator can also be placed on the left-hand side. 
\noindent
\begin{lstlisting}[language=C,numbers=none]
*x_ptr = 3;
\end{lstlisting}

\noindent
This code sets \texttt{x} to 3. 
This is referred to as a \textit{store} operation. 

\textit{Question:} What happens in the following line of code? 
Why is this incorrect? 

\noindent
\begin{lstlisting}[language=C,numbers=none]
x_ptr = 17;
\end{lstlisting}

\textit{Task:} Explain the effect of the following lines of code. 
Explain the left-hand side and right-hand side of each assignment. 
Describe the data-types and values of the three variables \texttt{x}, \texttt{p}, and \texttt{q}. 

\noindent
\begin{lstlisting}[language=C,numbers=none]
int x = 2;
int* p = &x;
int** q = &p;
\end{lstlisting}

\item \textbf{Compiling with \texttt{gcc}}

The rest of this lab deals with the source code file \texttt{pointer-ex.c} found in the \texttt{lab-c-pointers} directory, and below: 
%Listing \ref{lst:code} shows the source code. 

\lstset{language=C}
\noindent
\begin{lstlisting}[label={lst:code},caption={Source code of file \texttt{pointer-ex.c}.},captionpos=b]
#include <stdio.h>

int main(int argc, char** argv) {
  int    a = 5;
  char   b = 'b';
  double c = 2.718282;
  printf("a is: %d \n", a);
  printf("b is: %c \n", b);
  printf("c is: %f \n\n", c);
  int*     ptr_a = &a;
  char*    ptr_b = &b;  
  double*  ptr_c = &c;
  printf("ptr_a points to (0x%x) with dereferenced value %d\n",
     a);
  printf("ptr_b points to (0x%x) with dereferenced value %c\n",
     b);
  printf("ptr_c points to (0x%x) with dereferenced value %f\n",
     c);
  return 0;
}
\end{lstlisting}

\noindent
\textit{Task:} As a quick refresher, C source files can be compiled from the command line using \texttt{gcc}. 
Compile \texttt{pointer-ex.c} using the following command:

\noindent
\begin{lstlisting}[language=bash,numbers=none]
$ gcc -o pointer-prog pointer-ex.c
\end{lstlisting}

\noindent
The \texttt{-o <NAME>} option specifies a program name for the executable.
The result of this compilation command is a program called \texttt{pointer-prog}.
Execute it from the shell prompt with:

\noindent
\begin{lstlisting}[language=bash,numbers=none]
$ ./pointer-prog
\end{lstlisting}

\noindent
(Capture a screenshot of the output.) 

\item \textbf{Format Strings}

Consider the code in Listing~\ref{lst:code} again.  The program uses the
\texttt{printf} function to print output to the console.  Parameters for
\texttt{printf} are a format string and arguments for that format string.  (See
%\texttt{man printf} and \texttt{man -s 3 printf} for more details.) For
%instance,
\texttt{man printf} and \texttt{man 3 printf} for more details.) For instance,
the first three \texttt{printf} statements each use different format string
modifiers to print the values of \texttt{a}, \texttt{b}, and \texttt{c}.
\texttt{\%d} prints a \texttt{float}/\texttt{double} value, \texttt{\%c} prints
a \texttt{char}acter type, and \texttt{\%f} prints a float/double type.  The
\texttt{\%} modifier should match the type of the argument given; the page for
%\texttt{man -s 3 printf} lists the available modifiers. 
\texttt{man 3 printf} lists the available modifiers. 

\textit{Task:} The code in Listing \ref{lst:code} is incorrect.  The goal of
this task is to fix the last three \texttt{printf} statements to provide the
correct arguments and print the address and dereferenced value of each pointer
variable.  Only the variables \texttt{ptr\_a}, \texttt{ptr\_b}, and
\texttt{ptr\_c} may be used in the last three \texttt{printf}
statements---\textit{not} \texttt{a}, \texttt{b}, or \texttt{c}.  Provide the
corrected source code in the lab write-up and a screenshot of the output of the
corrected \texttt{pointer-ex.c} program. 

\item \textbf{Submission Guidelines} Compile all of the tasks into a document
with the required answers and screenshots to any questions. 

\end{enumerate}

%%%%%% END MAIN CONTENT OF LAB%%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

%\section{Thematic Takeaways}

%\begin{itemize}
%\item ~
%\item ~
%\end{itemize}

%\section{Lab Summary}

%\section{Key Terms}

%\section{Bibliographic Notes}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
