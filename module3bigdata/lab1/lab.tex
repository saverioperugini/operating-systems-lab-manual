% uncomment following line when compiling entire book
\graphicspath{{"module3bigdata/lab1/figs/"}}
% uncomment following line when compiling individual chapter
%\graphicspath{{"figs/"}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpg,.png}

\chapter{Cloud Computing and Big Data Processing (Part I)}
\label{CHAP:AWSCLUSTER}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Authors: Zhongmei Yao and Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

\begin{itemize}
\item Create a cluster of virtual machines (\textsc{vm}s) at amazon clouds
    (\url{https://aws.amazon.com})
\item Connect to \textsc{vm}'s from your own computer
\item Set up Group/User and Password Authentication
\end{itemize}

\section{Resources and Getting Help}

Amazon EC2 documentation is available at:
\url{https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html}.
%
%AWS Documentation » Amazon EC2 » User Guide for Linux Instances » Getting
%Started with Amazon EC2 Linux Instances Getting Started with Amazon EC2 Linux
%Instances

\section{Initial Setup and Preparation}

Students may receive up to $\$100$ of \textsc{aws} (Amazon Web Services)
credit by applying for \textsc{aws} Educate at
\url{https://aws.amazon.com/education/awseducate/apply/}. Click `Apply for
\textsc{aws} educate for Students' and then follow the steps given at the
website. The \textsc{aws} credit may be used to pay the balance of your
\textsc{aws} account.


\section{Create A Cluster of Virtual Machines}

The following shows steps to create a cluster of four (at least three)
virtual machines at \url{https://aws.amazon.com}.

\begin{enumerate}

\item Go to \url{https://aws.amazon.com}. Click `Create an AWS
    Account' and follow the steps given at the website to create your
    account. You should have your cell phone number and credit card ready
    while doing so. The credit card that you enter will NOT be charged if
    your use amazon free resource credits (offered to new new account
    holders). After free hours, you may use \textsc{aws} credit (you need
    to apply for it) to pay the balance if your account has used all free
    hours offered by \textsc{aws}.

\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-log.jpg}
   \caption{Amazon cloud computing services.}
   \label{log}
\end{figure}

\item After successfully create an account and log in \textsc{aws}, click
    `Services' and then `EC2':

\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-EC2.jpg}
   \caption{Amazon EC2 (Elastic Compute Cloud) resources.}
   \label{EC2}
\end{figure}

EC2 (Elastic Compute Cloud) allows users to rent virtual computers on which
they can install and run their own computer applications.

Before proceeding, pay attention to the region where instances will be
    launched, which should be close to your location:
\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-region.jpg}
   \caption{Check your region.}
   \label{region}
\end{figure}

\item Now, click `Launch instance'.
\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-launch.jpg}
   \caption{Start to install \textsc{os} on virtual machines.}
   \label{launch}
\end{figure}

\item Check `\textbf{Free tier only},' and select Ubuntu Server 16.04 (note
    that version 16.04 is free tier eligible in 2019 and versions 14.04 and
    18.04 should be fine as wells):
\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-freetier.jpg}
   \caption{Select Ubuntu Server.}
   \label{freetier}
\end{figure}

\item Choose an Instance Type `t2.micro' (free tier eligible), and then
    click `Next: Configure Instance Details':
\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-type.jpg}
   \caption{Choose instance type.}
   \label{type}
\end{figure}

\item Enter `4' for Number of instances (i.e., one namenode/master-node,
    one secondary namenode, two slave nodes), and click `Next: Add
    Storage':

\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-fourIns.jpg}
   \caption{Input the number of instances.}
\end{figure}

\item Accepting the default setting, click `Next: Add Tags':

\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-storage.jpg}
   \caption{Select default storage setting.}
\end{figure}

\item Choose a name for Tag (e.g., \texttt{HadoopCluster}). Click `Next:
    Configure Security Group':

\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-nametag.jpg}
   \caption{Name the cluster.}
\end{figure}

\item Create a \emph{new} security group and click `\textbf{Add Rule}' to
    add these four rules (see Figure \ref{addRule} for details).
    \textbf{Note: This step is extremely important}. In the past, students
    did not add these four rules correctly and could not connect to the
    machines at \textsc{aws} from their own computers. For our first lab at
    Amazon clouds, you may choose `\textbf{Anywhere}' as Source for testing
    purpose. In future, use the \textsc{i[} address of your own computer (e.g.,
    \url{198.0.113.1/32}
or a range of \textsc{ip} addresses \url{198.0.113.0/24}) as Source, to
    prevent malicious users from connecting to your instances at
    \textsc{aws}. We can change/add rules at any time and the changes are
    automatically applied to the instances at \textsc{aws}. Now click
    `Review and Launch':

\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-group.jpg}
   \caption{Create a new security group for the cluster.}
    \label{addRule}
\end{figure}

\item After launch, choose `Create a new key pair.' Name it (e.g.,
    \texttt{EC2hadoopkey}) and click `Download KeyPair':
\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-keypair.jpg}
   \caption{Download a new key pair.}
    \label{keypair}
\end{figure}

Save the \texttt{.pem} file (e.g., \texttt{EC2hadoopkey.pem}) on your
computer. The \texttt{.pem} file will be used as a key to allow us to
connect to these machines at \textsc{aws}. Make sure that you remember
where it is saved. Now click 'Launch Instances' on the same webpage (shown
on Figure \ref{keypair}).
\end{enumerate}

You have created four \textsc{vm} instances in amazon clouds! Click `View
Instances.' As shown in Figure \ref{VMs}, the instance state may be `pending'.
Once it becomes `running' we can use PuTTY (in Windows) to connect to your
\textsc{vm}s in cloud, which will be our next task. Note from Figure~\ref{VMs}
that each \textsc{vm} has its \textbf{public \textsc{ip} address} and
\textbf{private \textsc{ip} address}.

\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-VMs.jpg}
   \caption{\textsc{vm} instance information at \textsc{aws}.}
    \label{VMs}
\end{figure}


\subsection{Connecting to Virtual Machines from Local Computers}
\label{subsect:putty}

For \textbf{Mac users}, you do not need to follow steps in this subsection.
Instead, Mac users can open an \textsc{ssh} session in terminal (\textsc{osx})
and use
\texttt{.pem} (downloaded from \textsc{aws}) to directly connect to
\textsc{vm}s at \textsc{aws}:

\noindent
{\small
\texttt{\$ chmod 400 /}$\mathtt{<}$\texttt{\textit{path}}$\mathtt{>}$\texttt{/EC2hadoopkey.pem}\\
\texttt{\$ ssh -i /}$\mathtt{<}$\texttt{\textit{path}}$\mathtt{>}$\texttt{/EC2hadoopkey.pem ubuntu@public-ip-address-of-vm}}

\noindent
where \texttt{chmod 400} sets the permission of the \texttt{.pem} file to
protect this file against overwriting,
$\mathtt{<}$\texttt{\textit{path}}$\mathtt{>}$ should be replaced by
the actual path on your computer where the \texttt{.pem} file is saved, and
\texttt{public-ip-address-of-vm} is the public \textsc{ip} address of a
\textsc{vm} created at \textsc{aws} (see Figure \ref{VMs}).

The following is for Windows users. You should install software
\textbf{PuttyGen} and \textbf{Putty} on your computer before continue.

\begin{enumerate}
\item Launch \textbf{PuttyGen} to obtain the \texttt{.ppk} private key
    given the \texttt{.pem} file. Click `Conversions' and then `Import
    key':

\begin{figure}[H]
   \centering
   \includegraphics[width=2.5in]{1-putty.jpg}
   \caption{Launch PuttyGen to convert \texttt{.pem} to a private key.}
\label{convertPrivate}
\end{figure}

\textbf{Do not click} `Generate' in Figure \ref{convertPrivate} (if click
    `Generate,' it will generate a new private/public key pair, which is
    not the private key for connecting to VMs in cloud).

\item After click 'Import key,' PuttyGen will ask you to open/load the
    \texttt{.pem} file that was downloaded from \textsc{aws}. Then, click
    `Save private key'.
\begin{figure}[H]
   \centering
   \includegraphics[width=2.5in]{1-keygenerator.jpg}
   \caption{Save the private key.}
\end{figure}

\item Click `Yes' when see the following window and then save the
    \texttt{.ppk} private key on your computer.
\begin{figure}[H]
   \centering
   \includegraphics[width=3in]{1-savekey.jpg}
   \caption{Save the key without a passphrase.}
\end{figure}

\item Get the IPv4 \textbf{public} \textsc{ip} (e.g.,
    \texttt{52.37.103.209}) from your \textsc{vm} instance at \textsc{aws}.
    Launch PuTTY. As shown in Figure \ref{puttyIP}, enter host name and
    port number $22$, choose Connection type `\textsc{ssh}' and then click
    `Connection'/'\textsc{ssh}':
\begin{figure}[H]
   \centering
   \includegraphics[width=3in]{1-puttyIP.jpg}
   \caption{Use public \textsc{ip} addresses to connect to \textsc{vm}s at \textsc{aws}.}
\label{puttyIP}
\end{figure}


\item Click `Auth,' locate the \texttt{.ppk} private key, and then click
    `Open' to connect:
\begin{figure}[H]
   \centering
   \includegraphics[width=3in]{1-puttyAuth.jpg}
   \caption{Authentication using the private key.}
\end{figure}

You should then see the following:
\begin{figure}[H]
   \centering
   \includegraphics[width=3in]{1-ubuntu.jpg}
   \caption{An \textsc{ssh} session with a \textsc{vm} instance at \textsc{aws}. The \textsc{ip} address \texttt{172-31-31-173} shown on the window is the private \textsc{ip} address of this instance.}
\end{figure}

Similarly, connect to the other three \textsc{vm} instances (using
different \textbf{public} \textsc{ip} addresses but the same \texttt{.ppk}
private key):
\begin{figure}[H]
   \centering
   \includegraphics[width=6.5in]{1-4VMs.jpg}
   \caption{Left: \textsc{ssh} sessions with \textsc{vm} instances at \textsc{aws}. Right: \textsc{vm} states at \textsc{aws}.}
    \label{VMsAtAWS}
\end{figure}
\end{enumerate}

We have created four ubuntu servers, which are \textbf{running} at
\textsc{aws} (shown in Figure \ref{VMsAtAWS}).

In the next subsection, we will create groups and users on ubuntu servers at
\textsc{aws} and set up an alternative way for authentication (so we do not
need to upload the \texttt{.ppk} private key for authentication every time we
log onto servers at \textsc{aws})

\subsection{Create Group/User and Set Up Password Authentication}
\label{subsectionGroupUser}

\begin{enumerate}
\item On each \textsc{vm}, create a group (e.g., named \texttt{hadoop}) and
    add the user (e.g., with name \texttt{hduser}) in the group:
\begin{lstlisting}[language=bash, numbers=none]
ubuntu@vm-ip-address:~$ sudo addgroup hadoop
\end{lstlisting}
\vspace{-18mm}
\begin{lstlisting}[language=bash, numbers=none]
ubuntu@vm-ip-address:~$ sudo adduser --ingroup hadoop hduser
\end{lstlisting}
As shown in Figure \ref{createGroup}, it will prompt you to create the
password for \texttt{hduser}. Proceed with creating the password that you
wish. For convenience, use the same password for all instances for this
lab. Press \textsc{enter} for other questions.

\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-groupuser.jpg}
   \caption{Create a group and a user in this group on each \textsc{vm}.}
\label{createGroup}
\end{figure}

In case you did not set password successfully, use the command to reset
password:
\begin{lstlisting}[language=bash, numbers=none]
ubuntu@vm-ip-address:~$ sudo passwd hduser
\end{lstlisting}
\item Add the \texttt{hduser} user into group 
\texttt{sudo} to allow root access:
\begin{lstlisting}[language=bash, numbers=none]
ubuntu@vm-ip-address:~$ sudo adduser hduser sudo
\end{lstlisting}
\begin{figure}[H]
   \centering
   \includegraphics[width=5.8in]{1-sudouser.jpg}
   \caption{Allow root access.}
\end{figure}

\item Edit the \textsc{ssh} configuration file (in the \texttt{/etc/ssh}
folder) to allow logging in by using the password:

\begin{lstlisting}[language=bash, numbers=none]
ubuntu@vm-ip-address:~$ sudo vi /etc/ssh/sshd_config
\end{lstlisting}
You will see the following:
\begin{figure}[H]
   \centering
   \includegraphics[width=5.0in]{1-sshConf.jpg}
   \caption{Open the ssh configuration file for editing.}
\end{figure}

\item Recalling \texttt{vi} commands, hit the uppercase letter \texttt{G}
    (on your keyboard) to find the last line and then lowercase \texttt{o}
    to place the cursor below the last line:
\begin{figure}[H]
   \centering
   \includegraphics[width=5.0in]{1-editConf.jpg}
   \caption{Use \texttt{vi} commands to edit the file.}
\end{figure}

After enter the lowercase \texttt{o}, we are in the \textsc{insert}
mode. In the
following, we will insert text into this file.

\item Hit Arrows on the keyboard to move the cursor to find the wanted line
    \texttt{PasswordAuthentication \textbf{no}} and modify it to be
    \texttt{PasswordAuthentication \textbf{yes}}. Hit 
the <\textit{esc}> key to exit \textsc{insert} mode.
    Now \texttt{vi} is in the \textsc{command}
mode. Enter \texttt{:w} to save text.
    Enter. You should see the file has been updated:
\begin{figure}[H]
   \centering
   \includegraphics[width=5.0in]{1-psdYes.jpg}
   \caption{Set Password Authentication Yes.}
\end{figure}

\item To leave \texttt{vi}, enter \texttt{:q}:
\begin{figure}[H]
   \centering
   \includegraphics[width=4.0in]{1-leaveVi.jpg}
   \caption{Close the configuration file.}
\end{figure}

You have successfully modified the file using \texttt{vi}. For more commands in
\texttt{vi}, please see help/support docs such as the following website:\\
\url{https://www.cs.colostate.edu/helpdocs/vi.html}

\item Now restart the \textsc{ssh} service for the update to take action
and switch to the \texttt{hduser} account:

\begin{lstlisting}[language=bash,numbers=none]
ubuntu@vm-ip-address:~$ sudo service ssh restart
ubuntu@vm-ip-address:~$ sudo su - hduser
\end{lstlisting}

\begin{figure}[H]
   \centering
   \includegraphics[width=4.0in]{1-restartSSH.jpg}
   \caption{Close the configuration file.}
\end{figure}

\item To test if we can log in the \textsc{vm} as \texttt{hduser},
    \textbf{open a new \textsc{ssh} session}. Enter host name and port
    number as shown in the following and then click `Open':
\begin{figure}[H]
   \centering
   \includegraphics[width=4.0in]{1-puttyhduser.jpg}
   \caption{Open a new \textsc{ssh} session in PuTTY.}
\end{figure}

\item Next, enter the password (the one you created):
\begin{figure}[H]
   \centering
   \includegraphics[width=4.0in]{1-enterPass.jpg}
   \caption{Enter password in the \textsc{ssh} session.}
\end{figure}
You should then see:
\begin{figure}[H]
   \centering
   \includegraphics[width=4.0in]{1-passLog.jpg}
   \caption{Successfully connected to a \textsc{vm} in \textsc{aws} using user name and password.}
\end{figure}
\end{enumerate}

Repeat steps in Section \ref{subsectionGroupUser} on all other \textsc{vm}s:
\begin{lstlisting}[language=bash, numbers=none]
ubuntu@vm-ip-address:~$ sudo addgroup hadoop
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
ubuntu@vm-ip-address:~$ sudo adduser --ingroup hadoop hduser
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
ubuntu@vm-ip-address:~$ sudo adduser hduser sudo
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
ubuntu@vm-ip-address:~$ sudo vi /etc/ssh/sshd_config
    PasswordAuthentication yes
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
ubuntu@vm-ip-address:~$ sudo service ssh restart
\end{lstlisting}
\vspace{-10mm}
\begin{lstlisting}[language=bash, numbers=none]
ubuntu@vm-ip-address:~$ sudo su - hduser
\end{lstlisting}

At the end of each lab, we should stop \textsc{vm}'s. To do so, choose
`Actions,' `Instance State,' and then click `Stop':
\begin{figure}[H]
   \centering
   \includegraphics[width=5.0in]{1-stopVMs.jpg}
   \caption{Stop \textsc{vm}'s in \textsc{aws}.}
\end{figure}

Make sure to set Instance State \textbf{stopped} before log out your
\textsc{aws} account; otherwise your account may quickly run out of free
hours.
\begin{figure}[H]
   \centering
   \includegraphics[width=5.0in]{1-stopped.jpg}
   \caption{Instance state is now stopped in \textsc{aws}.}
\end{figure}

%%%%%% END PREFIX %%%%%%%

%%%%%% END MAIN CONTENT OF LAB %%%%%%%

%\lstinputlisting[language=C,label=lst:Cprogram]{src/program.c}


%%%%%% END MAIN CONTENT OF LAB%%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

\section{Thematic Takeaways}

\begin{itemize}
\item An EC2 instance is a virtual server that we can rent to run our
    applications in \textsc{aws}.
\item When setting up an EC2 instance, we can custom-configure CPU,
    storage, memory, and networking resources and rules.
\end{itemize}

\section{Lab Summary}

In this lab, we created a cluster of four \textsc{vm}'s in the cloud,
connected to them through \textsc{ssh} sessions using the \texttt{.ppk}
private key, and then set up an alternative approach (i.e., user and
password) to connect to them for convenience.

Using \textsc{vm} instances in the cloud is very convenient. If fatal errors
happen (e.g., we cannot connect to them from our own computer), we can always
delete those servers and create new \textsc{vm} instances to do experiments.

\section{Key Terms}

EC2 instances, a security group.

%\section{Bibliographic Notes}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
