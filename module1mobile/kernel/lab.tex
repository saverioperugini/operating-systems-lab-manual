%\renewcommand{\chaptername}{Lab}
%\renewcommand{\thechapter}{1}
\chapter{Linux Kernel Lab---System Calls}
\label{CHAP:KERNEL}

%\pagenumbering{arabic}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Author: Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

%\section{Lab Learning Outcomes}

%\begin{itemize}
%\item~
%\item~
%\end{itemize}

\section{Lab Overview}

This lab introduces the following topics about the Linux Kernel: 
\begin{itemize}
\item System Calls---execution and tracing
\item Communication between kernel space and user space
\item Loadable kernel modules
\end{itemize}

\subsection{Setup}

Included with this lab is a preconfigured VirtualBox image running Debian 9.1
x86-64.  The home directory of the \texttt{student} user contains a directory
called \texttt{lab-syscalls} which contains all of the files necessary to
complete this lab. 

\section{Resources and Getting Help} 

\begin{itemize}
    \item System Libraries and I/O Notes:
       \url{https://perugini.cps.udayton.edu/teaching/books/SPUC/www/lecture_notes/librariesIO.html}

    \item Linux Manpage (e.g., \texttt{\$ man ioctl})
\end{itemize}

%%%%%% END PREFIX %%%%%%%

%%%%%% BEGIN MAIN CONTENT OF LAB %%%%%%%

%\section{Lab Tasks}
\section{Steps}

Complete each of the following tasks and include a screenshot (if requested)
and a concise answer to any questions.  First, boot up the provided virtual
machine, and log in as \texttt{student}.  Then, navigate to the
\texttt{lab-syscalls} directory. 

\begin{enumerate}

\item \textbf{System Calls}
\label{sec:syscalls}

System calls are functions provided by the kernel.  A system call provides an
interface between a program and operating system services.  They are not
executed directly by user-level programs but are invoked through functions in
program libraries.  A request for a system call signals a \textit{software
interrupt} where the active process context switches to kernel mode.  Then, the
kernel executes the system call---part of a set of restricted operations that
control services such as input/output, file and device management, network,
memory management, process creation, and others---and returns the process to
`non-privileged' user mode.  For more information on system calls specific to
Linux, read \texttt{man syscalls}.

Consider the C program in file \texttt{hello.c} shown in Listing
\ref{lst:codehello}.  This program calls the \texttt{printf} function from the C
standard library.  To print output to the console, this library function
invokes a system call.  However, the details of this interface are hidden from
the user. 

\begin{lstlisting}[language=C,label={lst:codehello},caption={Source code of file \texttt{hello.c}.},captionpos=b]
#include <stdio.h>
int main(){
   printf("Hello world!\n");
   return 0;
}
\end{lstlisting}

\textbf{Introducing \texttt{strace}:} \texttt{strace} is a system utility that
traces system calls and provides detailed diagnostic and debugging information.
To illustrate, first compile \texttt{hello.c} with the following command:

\begin{lstlisting}[language=bash,numbers=none]
$ gcc -o hello hello.c 
\end{lstlisting}
Running the program, \texttt{./hello}, prints, \texttt{Hello World!} to the console. 

\textit{Task}: Run \texttt{hello} with \texttt{strace} and take a screenshot of
the output.  Starting from the beginning, much of the output deals with program
initialization and loading libraries.  Each line of the output lists the
invoked system call, the arguments passed to it, and the return value of the
system call.  Find the system call associated with the \texttt{printf}
statement in Listing \ref{lst:codehello}. 

\textit{Questions}: What is the name of the system call associated with the
\texttt{printf} function?  What is the return value of this system call and
what does it mean?

\textit{Hint:} Individual system calls can also be looked up in the
\texttt{man} pages---just be sure to use the correct \textit{section} number
for system calls (e.g., \texttt{man -s 2 write}).

\textit{Bonus Hint:} The different \textit{section} numbers for the
\texttt{man} pages can be looked up with \texttt{man man}.

\textbf{More with \texttt{strace}:} \texttt{strace} also has command-line
options to output different information. 

\begin{itemize}

\item \texttt{-o <FILENAME>} saves the output of the \texttt{strace} command to
the file \texttt{<FILENAME>}.  This is helpful if the output is longer than the
console screen or if the trace should be saved for future analysis. 

\item \texttt{-e <SYSCALL>} filters the output to only include system calls
that are of type \texttt{<SYSCALL>}. 

\item \texttt{-c} produces a summary of all of the system calls, errors, and
time spent for a program upon exit. 

\end{itemize}

\textit{Task}: Choose one Linux utility program---such as \texttt{cat},
\texttt{ls}, \texttt{tail}, or \texttt{ping} among others. 

Explore the trace for the chosen program, and experiment with the new
command-line options mentioned above.  Choose three system calls invoked by the
program and describe:

\begin{itemize}

\item the arguments used, 

\item how they relate to the program's functionality, and

\item the return value of the system call and its meaning. 

\end{itemize}

If it is difficult to pick out three system calls, consider inducing an error
in the program and note any differences between the error trace and an
error-free trace. 

\item \textbf{Loadable Kernel Modules}

This task introduces how to compile, load, and interact with kernel modules in
the Linux kernel.  The kernel module in question is a driver for a basic
character device.  The driver defines its own special command called an
\texttt{ioctl} (for `Input Output Control').  An \texttt{ioctl} is actually a
system call that devices use to implement custom commands that are not
available through the standard set of system calls---thus allowing the commands
to be invoked from user space. 

The code in the Listing \ref{lst:codeioctl} defines the character device driver
called \texttt{myioctl}.  The first three functions, \texttt{device\_open()},
\texttt{device\_release()}, and \texttt{device\_ioctl}, define how the device
behaves during certain events.  The function \texttt{device\_ioctl()} describes
how the device handles incoming \texttt{ioctl} commands.  The custom-defined
\texttt{ioctl} command for this task is called \texttt{MYIOCTL\_COMMAND}.  The
\texttt{switch/case} statement defines what happens when a
\texttt{MYIOCTL\_COMMAND} is passed in.  

\begin{lstlisting}[label={lst:codeioctl},caption={Source code of file \texttt{myioctl.c}.},captionpos=b]
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include "myioctl.h"
static int device_open(struct inode *inode, struct file *file){
   printk(KERN_INFO "myioctl: Device open\n");
   return 0;
}

static int device_release(struct inode *inode,
                          struct file *file){
   printk(KERN_INFO "myioctl: Device release\n");
   return 0;
}

static long device_ioctl(struct file *file, unsigned int cmd,
                         unsigned long arg){
   switch (cmd) {
   case MYIOCTL_CMD:
      printk(KERN_INFO "%s \n", (char *)arg);
      break;
   }
   return 0;
}

static struct file_operations fops = {
   .open = device_open, 
   .unlocked_ioctl = device_ioctl,
   .release = device_release
};

struct cdev *kernel_cdev;
 
static int device_init(void) {
   int ret;
   dev_t dev_no, dev;
   kernel_cdev = cdev_alloc(); 
   kernel_cdev->ops = &fops;
   kernel_cdev->owner = THIS_MODULE;
   printk("myioctl: Initializing device...\n");
   ret = alloc_chrdev_region(&dev_no, 0, 1, DEVICE_NAME);
   if (ret < 0) {
      printk("Major number allocation failed\n");
      return ret; 
   }
   major_no = MAJOR(dev_no);
   dev = MKDEV(major_no, 0);
   printk("The major.minor number for your device is %d.%d\n",
           major_no, MINOR(dev_no));
   ret = cdev_add(kernel_cdev, dev, 1);
   if (ret < 0 ) {
      printk(KERN_INFO "Unable to allocate cdev\n");
      return ret;
   }
   return 0;
}

static void device_exit(void) {
   printk(KERN_INFO "myioctl: Removing device...\n");
   cdev_del(kernel_cdev);
   unregister_chrdev_region(major_no, 1);
   printk(KERN_INFO "myioctl: Device removed, goodbye!\n");
}

MODULE_LICENSE("GPL");
module_init(device_init);
module_exit(device_exit);
\end{lstlisting}

\texttt{device\_init()} and \texttt{device\_exit()} are called when this module
is loaded and removed, respectively.  The function \texttt{device\_init()}
initializes a character device (\texttt{cdev}) with the operations defined
above for open, release, and \texttt{ioctl}.  It also allocates space for the
character device and provides the \textit{major}-\textit{minor} number for the
device---a unique numerical identifier.  The function \texttt{device\_exit()}
deletes and unregisters the device from the kernel.  The header file,
\texttt{myioctl.h}, defines macros such as \texttt{DEVICE\_NAME} and
\texttt{MYIOCTL\_COMMAND}. 

\textit{Tasks}: Run the following command:

\begin{lstlisting}[language=bash,numbers=none]
$ sudo apt-get install raspberrypi-kernel-headers
\end{lstlisting}

\noindent
The \texttt{raspberry-kernel-headers} are necessary to compile
kernel modules according to \url{raspberry.org}.

Within the \texttt{lab-syscalls} directory, compile the
\texttt{myioctl} kernel module with \texttt{make}.  This produces a
\emph{kernel object} called \texttt{myioctl.ko}.  To load the kernel module,
run:

\begin{lstlisting}[language=bash,numbers=none]
$ sudo insmod myioctl.ko
\end{lstlisting}

Check the logs to make sure no errors occurred:

\begin{lstlisting}[language=bash,numbers=none]
$ sudo tail /var/log/messages
\end{lstlisting}

(Take a screenshot of this output.)

Note the major and minor number, then run the following command to create a
file for the character device:

\begin{lstlisting}[language=bash,numbers=none]
$ sudo mknod /dev/myioctl_dev c X Y
\end{lstlisting}

where \texttt{X} is the major number and \texttt{Y} is the minor number. 

Now it is time to interface with the character device from the user space.
Shown in Listing \ref{lst:codeuser}, the file \texttt{userprog.c} describes a
program to do this.  This program opens the file \texttt{/dev/myioctl\_dev} and
invokes the \texttt{ioctl()} function passing the device, the command
(\texttt{MYIOCTL\_COMMAND}), and a message (\texttt{msg}) as arguments. 

\begin{lstlisting}[language=C,label={lst:codeuser},caption={Source code of file \texttt{hello.c}.},captionpos=b]
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "myioctl.h"
int main(){
   int fd;
   int ret;
   char *msg = "OPERATING-SYSTEMS-INTERNALS-AND-DESIGN";
   fd = open(DEVICE_PATH, O_RDWR);
   if (fd < 0) {
      printf("Sorry, cannot open device: %s\n", DEVICE_PATH);
      return -1;
   }
   ret = ioctl(fd, MYIOCTL_CMD, msg);
   if (ret < 0) {
      printf("Sorry, ioctl command failed\n");
      return -1;
   }
   close(fd);
   return 0;
}
\end{lstlisting}

\textit{Task:} Compile \texttt{userprog.c} with the following command:

\begin{lstlisting}[language=bash,numbers=none]
$ gcc -o userprog userprog.c
\end{lstlisting}

With the kernel module still loaded, run the program with:

\begin{lstlisting}[language=bash,numbers=none]
$ sudo ./userprog
\end{lstlisting}

Then, \texttt{tail} the logs as before and take a screenshot of the output,
showing the message passed to the device.  Now, run \texttt{userprog} again
with \texttt{strace} (note: it may be necessary to run this with
\texttt{sudo}).  Describe the system call trace as before in
$\S$~\ref{sec:syscalls}---especially the \texttt{ioctl} call.  Use the
different command-line options to make the analysis easier.  When finished,
remove the kernel module with:

\begin{lstlisting}[language=bash,numbers=none]
$ sudo rmmod myioctl
\end{lstlisting}
\texttt{tail} the logs again, and take a screenshot showing the module was unloaded without any errors. 

\end{enumerate}

\section{Submission Guidelines}

Compile all of the tasks into a document with the required answers and
screenshots to any questions and tasks. 

%%%%%% BEGIN SUFFIX %%%%%%%

%\section{Thematic Takeaways}

%\begin{itemize}
%\item ~
%\item ~
%\end{itemize}

%\section{Lab Summary}

%\section{Key Terms}

%\section{Bibliographic Notes}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
