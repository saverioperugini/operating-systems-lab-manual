%\renewcommand{\chaptername}{Lab}
%\renewcommand{\thechapter}{10}
\chapter{Critical Sections, Race Conditions, \& Deadlock}
\label{CHAP:RACECONDITIONS}

%%%%%% BEGIN PREFIX %%%%%%%

\noindent
\large{Author: Saverio Perugini\\
Copyright $\copyright$ \the\year\ by Project Team NSF Award No (FAIN): 1712406. \\
\textsc{all rights reserved}}

\section{Lab Learning Outcomes}

\begin{itemize}

\item Establish an understanding of \textit{critical sections}.

\item Establish an understanding of \textit{race conditions} or \textit{data
races}.

\end{itemize}

\section{Resources and Getting Help} 

\begin{itemize}
   \item Synchronization Notes:
      \url{https://perugini.cps.udayton.edu/teaching/courses/cps346/lecture_notes/synchronization.html}

   \item
%      [OSCJ8] Chapter 6, $\S\S$6.1--6.4 (pp. 241--249)
%      or
%      [OSC8] Chapter 6, $\S\S$~6.1--6.4 (pp. 225--234)
      %[OSC10] Chapter 6, $\S\S$~6.1--6.5 (pp. 257--272)
      \cite{OSC10}~Chapter 6, $\S\S$~6.1--6.5 (pp. 257--272)
\end{itemize}

%%%%%% END PREFIX %%%%%%%

%%%%%% BEGIN MAIN CONTENT OF LAB %%%%%%%

\section{Activities}

\begin{enumerate}

\item Consider the following two processes:

\begin{verbatim}
process producer {

   while (true) {
      while (count == BUFFER_SIZE);
      ++count;
      buffer[in] = item;
      in = (in + 1) % BUFFER_SIZE;
   }
}

process consumer {

    while (true) {
       while (count == 0);
       --count;
       item = buffer[out];
       out = (out - 1) % BUFFER_SIZE;
    }
}
\end{verbatim}

\noindent Assume \texttt{count = 5} and both the \texttt{producer} and
\texttt{consumer} processes execute the statements \texttt{++count} and
\texttt{--count}.

Results? \texttt{count} could be set to 4, 5, or 6 (but only 5 is correct).

\begin{verbatim}
reg_1 = count
reg_1 = reg_1 + 1
count = reg_1

reg_2 = count
reg_2 = reg_2 - 1
count = reg_2
\end{verbatim}

Each assembly language instruction is \textit{atomic}
(i.e., executes in one \textsc{cpu} cycle).
The instruction cannot be divided.
The process executing the instruction
cannot time slice while it is executing the instruction.
In other words, you cannot execute half an instruction.

{\small
$t_0$: \texttt{producer} process executes \texttt{reg_1 = count}~~~~~[\texttt{reg_1 = 5}]\\
$t_1$: \texttt{producer} process executes \texttt{reg_1 = reg_1 + 1}~[\texttt{reg_1 = 6}]\\
$t_2$: \texttt{consumer} process executes \texttt{reg_2 = count}~~~~~[\texttt{reg_2 = 5}]\\
$t_3$: \texttt{consumer} process executes \texttt{reg_2 = reg_2 - 1}~[\texttt{reg_2 = 4}]\\
$t_4$: \texttt{producer} process executes \texttt{count = reg_1}~~~~~[\texttt{count = 6}]\\
$t_5$: \texttt{consumer} process executes \texttt{count = reg_2}~~~~~[\texttt{count = 4}]}\\

This is called a \textit{race condition} or \textit{data race}: a situation
where multiple processes access and manipulate the same data concurrently and
the outcome of the execution depends on the order in which the instructions
execute.

This is called the \textit{critical section problem}.  The \textit{critical
section} ($<\!\!\mathit{cs}\!\!>$) is the set of instructions which access
shared resource.  We must establish \textit{mutual exclusion} in the critical
section.  No two processes can be in their $<\!\!\mathit{cs}\!\!>$ at the same
time.

\item Interleave the assembly language instructions of the high-level
language statements \texttt{count++} and \texttt{count--}, under the section titled
``The critical section problem'' and demonstrate that we can get one
of three possible values for \texttt{count}: 4, 5, or 6.
Find an interleaving of instructions which result in \texttt{count = 6},
since the above sequence results in \texttt{count = 4}.

\item Of all $6!$ permutations of instructions, how many are valid
interleavings?  And of the those that are valid, how many result in
an incorrect value for \texttt{count}?  More than 50\%?

\item Investigate the definition of the hardware instructions
\texttt{test-and-set} and \texttt{swap} in the section titled ``Hardware
instructions'' and try to demonstrate a race condition in each program.

\item Investigate the definition of \textit{Peterson's Solution} in the section
titled ``Peterson's Solution'' and try to demonstrate a race condition in the
program.

\newpage
\item
Consider the following Java program containing two concurrent threads.

\begin{lstlisting}[language=Java]
class Test {
   // global shared data among threads
   int turn = 1;
   boolean t1WantsToEnter = false;
   boolean t2WantsToEnter = false;

   public static void main(String args[]) {
      (new Thread (new T1()).start();
      (new Thread (new T2()).start();
   }
}
class T1 implements Runnable {
   public void run() {
      while (true) {
         t1WantsToEnter = true;
         while (turn != 1) {
            while (t2WantsToEnter);
            turn = 1;
         }
         // <critical section>
         t1WantsToEnter = false;
      }
   }
}
class T2 implements Runnable {
   void run() {
      while (true) {
         t2WantsToEnter = true;
         while (turn != 2) {
            while (t1WantsToEnter);
            turn = 2;
         }
         // <critical section>
         t2WantsToEnter = false;
      }
   }
}
\end{lstlisting}

%\noindent Does this program enforce mutual exclusion in the critical section?
%In other words, does this program have a \textit{race condition}?  If not,
%state why not.  If so, identify the race condition by showing an interleaving
%of the threads, using the line numbers, in which mutual exclusion is not
%preserved.

\noindent This program has a \textit{race condition}.  Identify the race
condition by showing an interleaving of the threads, using the line numbers, in
which mutual exclusion is not preserved. In other words, give an execution
order of the statements, using line numbers, which leads to each thread into
the critical section at the same time.

%\item Install golang on your Raspberry Pi
%
%\begin{lstlisting}[language=bash]
%$ sudo apt-get update
%$ sudo apt-get upgrade
%$ sudo apt-get install golang
%\end{lstlisting}
%
%\item Install Elixir on your Raspberry Pi
%
%\begin{lstlisting}[language=bash]
%$ wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb &&
%    sudo dpkg -i erlang-solutions_1.0_all.deb &&
%    rm erlang-solutions_1.0_all.deb
%$
%$ sudo apt-get update && sudo apt-get install erlang erlang-dev elixir
%\end{lstlisting}

\end{enumerate}

%%%%%% END MAIN CONTENT OF LAB%%%%%%%

%%%%%% BEGIN SUFFIX %%%%%%%

\section{Thematic Takeaways}

\begin{itemize}
\item The integrity of data shared among threads must be protected so that 
it does not become corrupted.
\item Primitive protection mechanisms include disabling interrupts,
hardware instructions, and Peterson's solution.
\end{itemize}

%\section{Lab Summary}

\section{Key Terms}
Critical section,
data races,
disabling interrupts,
hardware instructions,
mutual exclusion,
\textit{Peterson's Solution},
race condition,
\texttt{swap},
\texttt{test-and-set}.

%\section{Bibliographic Notes}

\begin{center}
\begin{tabular}{ccc}
\Pisymbol{dingbat}{69} & & \Pisymbol{dingbat}{70} \\
& $\westcross$ & \\
\Pisymbol{dingbat}{72} & & \Pisymbol{dingbat}{71}
\end{tabular}
\end{center}

%%%%%% END SUFFIX %%%%%%%
